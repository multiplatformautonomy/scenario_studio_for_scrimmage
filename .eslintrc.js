module.exports = {
    'env': {
        'browser': true,
        'es6': true,
        'node': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:react/recommended'
    ],
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly'
    },
    'parserOptions': {
        'ecmaFeatures': {
            'jsx': true
        },
        'ecmaVersion': 2018,
        'sourceType': 'module'
    },
    'plugins': [
        'react'
    ],
    'settings': {
        'react': {
            'version': 'detect'
        }
    },
    'rules': {
        'indent': [
            2,
            4
        ],
        'eqeqeq': [
            'error',
            'always'
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'semi': [
            'error',
            'always'
        ],
        'camelcase': [
            'error',
            {
                'properties': 'always',
                'ignoreImports': true,
                'allow': [
                    'child_process'
                ]
            }
        ],
        'curly': [
            'error',
            'all'
        ],
        'no-multi-str': [
            'error'
        ],
        'no-return-assign': [
            'error'
        ],
        'yoda': [
            'error'
        ],
        'default-case': [
            'error'
        ],
        'no-undef-init': [
            'error'
        ],
        'eol-last': [
            'error',
            'always'
        ],
        'comma-spacing': [
            'error',
            {
                'before': false,
                'after': true
            }
        ],
        'brace-style': [
            'error',
            '1tbs'
        ],
        'object-curly-newline': [
            'error',
            { 'multiline': true }
        ],
        'array-bracket-newline': [
            'error',
            { 'multiline': true }
        ],
        'keyword-spacing': [
            'error',
            {
                'before': false,
                'after': true,
                'overrides': {
                    'if': {
                        'after': false
                    },
                    'for': {
                        'after': false
                    },
                    'while': {
                        'after': false
                    }
                }
            }
        ],
        'space-in-parens': [
            'error',
            'never'
        ],
        'space-infix-ops': [
            'error'
        ],
        'padded-blocks':[
            'error',
            'never'
        ],
        'quotes': [
            'error',
            'single',
            {
                'avoidEscape': true,
                'allowTemplateLiterals': true
            }
        ],
        'arrow-parens': [
            'error',
            'always'
        ],
        'arrow-body-style': [
            'error'
        ],
        'template-curly-spacing': [
            'error',
            'never'
        ],
        'max-len': [
            'error',
            128
        ],
        'no-confusing-arrow': [
            'error'
        ],
        'id-length': [
            'error',
            {
                'min': 3,
                'max': 32,
                'exceptions': ['i', 'j', 'id', 'ip', 'cb', '$', 'fs','on', 'fn', 'to', 'in']
            }
        ],
        'func-call-spacing': [
            'error',
            'never'
        ],
        'func-style': [
            'error',
            'declaration'
        ],
        'comma-dangle': [
            'error',
            'never'
        ],
        'object-property-newline': [
            'error',
            {
                allowAllPropertiesOnSameLine: true

            }
        ],
        'array-element-newline': [
            'error',
            {
                multiline: true
            }
        ]
    },
    'overrides': [
        {
            'files': [
                'setupTests.js',
                '**/*.test.js'
            ],
            'extends': [
                'jest-enzyme'
            ],
            'env': {
                'jest': true
            },
            'plugins': ['jest', 'enzyme'],
            'rules': {
                'jest/no-disabled-tests': 'error',
                'jest/no-focused-tests': 'error',
                'jest/no-identical-title': 'error',
                'jest/valid-expect': 'error',
                'jest/valid-title': 'error',
            }
        }
    ]
};