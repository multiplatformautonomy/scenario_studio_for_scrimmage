import React from 'react';
import TitleScreen from './screens/TitleScreen';
import Mission from './screens/Mission';
import CodeEditor from './screens/codeEditor/CodeEditor';
import Docker from './utilities/Docker';
import DialogBox from './components/dialogs/DialogBox';
import Button from './components/Button';


export default class App extends React.Component{
    constructor(props){
        super(props);
        this.startMission = this.startMission.bind(this);
        this.screenHandler = this.changeScreen.bind(this);
        this.toggleEditor = this.toggleEditor.bind(this);
        this.openCodeEditor = this.openCodeEditor.bind(this);
        this.alertHandler = this.showAlert.bind(this);
        this.state = {
            screen: 'titleScreen',
            xmlFilePath: null,
            filePath: null,
            showEditor:false,
            dialogTitle:'',
            dialogMessage:''
        };

        if(process.env.NODE_ENV === 'production'){
            global.__assets = this.getElectron().app.getAppPath()
                .split(/\/|\\/).slice(0, -1).join('/') + '/assets';
            global.__public = this.getElectron().app.getAppPath()
                .split(/\/|\\/).slice(0, -1).join('/') + '/public';
        }else {
            global.__assets = this.getElectron().app.getAppPath()
                + '/src/assets';
            global.__public = this.getElectron().app.getAppPath()
                + '/src/public';
        }
    }

    componentDidMount(){
        this.checkForUpdate();
    }

    getElectron() {
        return window.require('electron').remote;
    }

    showScrimmage() {
        /* URL Safe Base64 Encoding for passing URL */
        function encode(input) {
            let xin = btoa(input);
            xin = xin.replace(/=/g, '_');
            xin = xin.replace(/\+/g, '-');
            xin = xin.replace(/\//g, '.');
            return xin;
        }

        const electron = this.getElectron();
        let win = new electron.BrowserWindow({
            fullscreen: true,
            frame: false,
            webPreferences: {nodeIntegration: true },
            show:false
        });
        let projectPath = `${window.location.origin}`;
        let title = this.state.xmlFilePath ? `SCRIMMAGE ${this.state.xmlFilePath}` : 'SCRIMMAGE';
        title = encode(title);
        let url = encode(Docker.vscrimUrl());

        // projectPath of develop is http://localhost:3000. The build stores the html file in the
        // extraResources directory. Develop looks for the html file inside of the public directory.
        const path = require('path');
        let isDevBuild = process.env.NODE_ENV === 'development';
        let directoryLoc = path.normalize((isDevBuild ? '' : global.__public)
            + '/scrimmage.html') + `?title=${title}&url=${url}`;
        win.loadURL(path.normalize(`${projectPath}${directoryLoc}`));
        win.show();
    }
    showJupyter(){
        const electron = this.getElectron();
        let win = new electron.BrowserWindow({
            fullscreen: false,
            frame: true,
            webPreferences: {nodeIntegration: true },
            show:false
        });
        let url = Docker.jupyterUrl(8888);
        win.loadURL(`${url}`);
        win.show();
    }

    clearMission() {
        this.setState({
            xmlFilePath: null,
            filePath: null
        });
    }

    startMission(xmlFilePath){
        if(xmlFilePath !== undefined){
            xmlFilePath = xmlFilePath.replace(/\\/g, '/');
            this.setState({
                screen: 'scenarioScreen',
                xmlFilePath: xmlFilePath
            });
        }
    }

    changeScreen(str){
        if(str !== this.state.screen){
            this.setState({screen: str });
        }
    }

    toggleEditor(){
        this.setState({showEditor: !this.state.showEditor });
    }

    openCodeEditor(fileInput){
        if(fileInput !== undefined){
            this.setState({
                filePath: fileInput,
                showEditor: true
            });
        }
    }

    showAlert(title, message) {
        this.setState({dialogTitle: title});
        this.setState({dialogMessage: message});
        this.DialogBox.setState({show: true});
    }

    checkForUpdate(){
        const https = this.getElectron().require('https');
        https.get(
            'https://gitlab.com/multiplatformautonomy/scenario_studio_for_scrimmage/-/raw/master/package.json',
            (res)=>{
                res.setEncoding('utf8');
                res.on('data', (data)=>{
                    const obj = JSON.parse(data);
                    if(obj.version !== this.getElectron().app.getVersion()){
                        const link = 'https://mass.gtri.gatech.edu/downloads/sss/';
                        let message = 
                            <>
                                Download new version here: 
                                <br/>
                                <a href='' onClick={(event)=>{
                                    event.preventDefault();
                                    window.require('electron').shell.openExternal(link);
                                }}>{link}</a>
                            </>;
                        this.showAlert('A new version of SSS is availble for download', message);
                    }
                });
            }
        );
    }

    renderScreen() {
        if(this.state.screen === 'titleScreen'){
            return (<>
                {this.state.showEditor ? <>
                    <CodeEditor 
                        screenHandler={this.screenHandler}
                        files={this.state.filePath}
                        onBack={this.toggleEditor}
                    />
                </> : <TitleScreen
                    alertHandler={this.alertHandler}
                    screenHandler={this.screenHandler}
                    clearMission={this.clearMission.bind(this)}
                    showScrimmage={this.showScrimmage.bind(this)}
                    startMission={this.startMission.bind(this)}
                    openCodeEditor={this.openCodeEditor.bind(this)}
                />}
            </>
            );
        }else if(this.state.screen === 'scenarioScreen'){
            if(this.state.xmlFilePath !== null){
                return (<>
                    <div style={!this.state.showEditor === false ? { display:'none'} : null} >
                        <Mission 
                            alertHandler={this.alertHandler}
                            screenHandler = {this.screenHandler}
                            xmlFilePath={this.state.xmlFilePath}
                            showScrimmage={this.showScrimmage.bind(this)}
                            openCodeEditor={this.openCodeEditor.bind(this)}
                            showJupyter={this.showJupyter.bind(this)}

                        />
                    </div>
                    {this.state.showEditor === true ? <>
                        <CodeEditor 
                            screenHandler={this.screenHandler}
                            files={this.state.filePath}
                            onBack={this.toggleEditor}
                            xmlPath={this.state.xmlFilePath}
                        />
                    </> : null}
                </>);
            }
        }else if(this.state.screen === 'codeEditorScreen') {
            return (
                <div style={{height:'100vh'}}>
                    <CodeEditor 
                        onBack={()=>{
                            this.changeScreen('titleScreen');
                        }}
                        filePath={this.state.filePath}
                        xmlPath={this.state.xmlFilePath}
                    />
                </div>
            );
        }
    }

    render(){
        return <>
            {this.renderScreen()}
            <DialogBox ref={(node)=>{
                this.DialogBox = node;
            }}
            title={this.state.dialogTitle}>
                {this.state.dialogMessage}
                <div className='dialog-button-container'>
                    <Button className='dialog-button-blue'
                        clickHandler={()=>this.DialogBox.setState({show: false})}>
                    Dismiss </Button>
                </div>
            </DialogBox>
        </>;
    }
}
