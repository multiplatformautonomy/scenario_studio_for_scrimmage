import React from 'react';
import { shallow, render, mount }from 'enzyme';
import Docker from '../utilities/Docker';
import TitleScreen from './TitleScreen';
// import Git from '../utilities/Git';
jest.mock('../utilities/Docker');
jest.mock('../utilities/Git');
jest.mock('../components/Editor3D.js');

window.require = () => ({
    remote: {
        require: require,
        app: { getPath: () => '' }
    }
});

test('jest functionality works', () => {
    expect(1).toBe(1);
});

test('renders MASS logo', () => {
    const titleScreen = render(<TitleScreen />);
    expect(titleScreen.find('img').attr('alt')).toBe('mass logo');
});

test('check for 6 icon buttons on title screen', () => {
    const titleScreen = shallow(<TitleScreen />);
    const collection = titleScreen.findWhere((_n_) => _n_.name() === 'SSSButton');
    expect(collection).toHaveLength(6);
});

test('clickhandlers should change state', () => {
    const ClickH = {
        clickh() {
            return;
        }
    };
    const spy = jest.spyOn(ClickH, 'clickh');
    const titleScreen = shallow(<TitleScreen screenHandler={ClickH.clickh} />);
    expect(titleScreen.instance().props.screenHandler).toBe(ClickH.clickh);
    const collection = titleScreen.findWhere((node) => node.name() === 'SSSButton');
    collection.forEach((node, i) => {
        if(node.props.screenHandler !== ClickH.clickh) {
            return;
        }//Placed to ignore the Dialogbox popup
        expect(node.name()).toBe('SSSButton');
        node.prop('clickHandler')();
        if(i < collection.length - 1) {
            expect(spy).toBeCalledTimes(i + 1);
        }
    });
});

test('save mission', () => {
    const titleScreen = shallow(<TitleScreen />);
    let savePath = 'src/screens/LiveMission.test.xml';
    expect(titleScreen.instance().saveNewMission(savePath)).toBe(undefined);
});

test('rendered buttons', () => {
    const mockScreenHandler = jest.fn();
    const mockAlertHandler = jest.fn();

    const titleScreen = mount(<TitleScreen 
        screenHandler={mockScreenHandler} 
        alertHandler={mockAlertHandler}/>);

    const updateData = jest.fn(() => { });
    titleScreen.instance().createProjectDirectory = updateData;

    expect(titleScreen.find('button')).toHaveLength(6);
    titleScreen.find('button').at(0).simulate('click');
    expect(updateData).toBeCalled();

    titleScreen.instance().loadMission = updateData;
    titleScreen.find('button').at(1).simulate('click');
    expect(updateData).toBeCalled();

    titleScreen.instance().advancedMode = updateData;
    titleScreen.find('button').at(2).simulate('click');
    expect(updateData).toBeCalled();

    titleScreen.instance().editFile = updateData;
    titleScreen.find('button').at(3).simulate('click');
    expect(updateData).toBeCalled();

    //Need substitute behavior with mock function to prevent creation of
    //new settings.json file
    titleScreen.instance().popUpGitSettingsBox = jest.fn();
    titleScreen.find('button').at(4).simulate('click');
    expect(updateData).toBeCalled();
});

test('advanced mode', () => {
    const mockClear = jest.fn(() => { });
    const mockShow = jest.fn(() => { });
    const titleScreen = mount(<TitleScreen showScrimmage={mockShow} clearMission={mockClear} />);

    titleScreen.instance().advancedMode();
    expect(mockClear).toBeCalled();
    expect(mockShow).toBeCalled();
});


test('load mission alerts user when git is not setup', () => {
    const alertHandler = jest.fn();
    const titleScreen = mount(<TitleScreen startMission={() => { }} alertHandler={alertHandler} />);

    titleScreen.setState({
        gitName: '',
        gitEmail: 'test'
    });
    titleScreen.instance().loadMission();
    expect(alertHandler.mock.calls.length).toBe(1);

    titleScreen.setState({
        gitName: 'foo',
        gitEmail: ''
    });
    titleScreen.instance().loadMission();
    expect(alertHandler.mock.calls.length).toBe(2);
});

test('load mission when xml file is and isnt present in the folder', () => {
    let loadedValue = {
        canceled: false,
        filePaths: ['test']
    };

    const mockSave = jest.fn((first, second, third) => {
        third(1);
    }).mockResolvedValue(loadedValue);
    const getWindow = jest.fn(() => { });

    let obj = { response: 0 };
    const mockShowMessage = jest.fn((first) => first).mockResolvedValue(obj);

    const titleScreen = mount(<TitleScreen startMission={() => { }} />);
    titleScreen.setState({
        gitName: 'test',
        gitEmail: 'test'
    });

    function mockObj() {
        return {
            dialog: { showOpenDialog: mockSave },
            getCurrentWindow: getWindow,
            require: jest.fn(() => ({ existsSync: jest.fn(() => true) }))
        };
    }
    titleScreen.instance().getElectron = mockObj;

    titleScreen.instance().loadMission();
    expect(mockSave).toBeCalled();
    expect(getWindow).toBeCalled();

    // XML file is not present in the selected directory
    function mockAltObj() {
        return {
            dialog: {
                showOpenDialog: mockSave,
                showMessageBox: mockShowMessage
            },
            getCurrentWindow: getWindow,
            require: jest.fn(() => ({ existsSync: jest.fn(() => false) }))
        };
    }

    titleScreen.instance().getElectron = mockAltObj;

    titleScreen.instance().loadMission();
});

test('get electron', () => {
    const titleScreen = mount(<TitleScreen />);
    expect(titleScreen.instance().getElectron()).not.toBe(undefined);
});

test('Get electron does not return undefined', () => {
    const titleScreen = shallow(<TitleScreen screenHandler={() => { }} />);
    expect(titleScreen.instance().getElectron()).not.toBe(undefined);
});

test('Testing Create Project Button', () => {
    const titleScreen = mount(<TitleScreen screenHandler={() => { }} startMission={() => { }} />);
    titleScreen.setState({
        gitName: 'test',
        gitEmail: 'test'
    });

    var mockShowSaveDialog = jest.fn((first1, second) => {
        second(1);
    }).mockResolvedValue('test.js');

    const mockGetElectron = jest.fn(() => ({
        require: jest.fn(() => ({
            execSync: jest.fn(() => { }),
            mkdirSync: jest.fn(() => { })
        })),
        dialog: { showSaveDialog: mockShowSaveDialog }
    }));
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().saveNewMission = jest.fn(() => { });
    titleScreen.instance().initializeDocker = jest.fn(async () => { });
    titleScreen.instance().generateInitializationFile = jest.fn();
    titleScreen.instance().saveNewMisison = jest.fn(() => { });

    titleScreen.instance().createProjectDirectory();

    mockShowSaveDialog = jest.fn((first1, second) => {
        second(1);
    }).mockRejectedValue('test.js');

    let obj = {
        dialog: { showSaveDialog: mockShowSaveDialog },
        require: jest.fn(() => ({ mkdirSync: jest.fn(() => { }) }))
    };
    titleScreen.instance().getElectron = jest.fn(() => obj);

    titleScreen.instance().createProjectDirectory();
});

test('Testing Create Project Button\'s error handling', () => {
    const alertHandler = jest.fn();
    const titleScreen = mount(<TitleScreen screenHandler={() => { }} startMission={() => { }} alertHandler={alertHandler} />);
    titleScreen.setState({
        gitName: 'foo',
        gitEmail: ''
    });


    titleScreen.instance().createProjectDirectory();

    expect(alertHandler.mock.calls.length).toBe(1);

    titleScreen.setState({
        gitName: '',
        gitEmail: 'foo'
    });
    titleScreen.instance().createProjectDirectory();

    expect(alertHandler.mock.calls.length).toBe(2);
});

test('if edit file works', () => {
    let obj = {
        canceled: false,
        filePaths: ['./test.js']
    };
    const mockOpen = jest.fn((first1) => first1);

    const mockGetElectron = jest.fn(() => ({
        dialog: { showOpenDialog: jest.fn().mockResolvedValue(obj) },
        getCurrentWindow: jest.fn(() => { })
    }));

    const titleScreen = mount(<TitleScreen vscrimUrl={() => { }} openCodeEditor={mockOpen} />);
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().editFile();
    expect(mockGetElectron).toBeCalled();
});

test('popUpGitSettingsBox', () => {
    const titleScreen = mount(<TitleScreen />);
    const mockSetState = jest.fn(() => { });
    const mockSetShow = jest.fn(() => { });
    const mockLoadGitSettings = jest.fn(() => ({
        username: '',
        email: ''
    }));
    const mockGitSettingsBoxSetState = jest.fn(() => { });
    titleScreen.instance().loadGitSettings = mockLoadGitSettings;
    titleScreen.instance().setState = mockSetState;
    titleScreen.instance().DialogBox = {
        setState: mockGitSettingsBoxSetState,
        setShow: mockSetShow
    };
    titleScreen.instance().gitSettingsBox = {
        setState: mockGitSettingsBoxSetState,
        setShow: mockSetShow
    };
    titleScreen.instance().popUpGitSettingsBox();
    expect(mockSetState).toBeCalled();
    expect(mockSetShow).toBeCalled();
});

test('handleGitSettingsSubmit', () => {
    const titleScreen = mount(<TitleScreen />);
    const mockSetState = jest.fn(() => { });
    const mockSaveGitSettings = jest.fn(() => { });
    titleScreen.instance().DialogBox = { setShow: mockSetState };
    titleScreen.instance().gitSettingsBox = { setShow: mockSetState };
    titleScreen.instance().saveGitSettings = mockSaveGitSettings;
    titleScreen.instance().handleGitSettingsSubmit();
    expect(mockSetState).toBeCalled();
});

test('gitSettingsBox renders on popUpGitSettingsBox()', () => {
    const mockGitSettingsBox = jest.fn();
    const titleScreen = mount(<TitleScreen />);
    titleScreen.instance().gitSettingsBox = mockGitSettingsBox;
    titleScreen.instance().popUpGitSettingsBox();
    expect(titleScreen.instance().DialogBox.state.show).toBe(true);
    expect(mockGitSettingsBox).toBeCalled();
});

test('git Settings box renders correctly', () => {
    const titleScreen = mount(<TitleScreen />);
    titleScreen.instance().popUpGitSettingsBox();
    titleScreen.update();
    const dialogBox = titleScreen.find('SSSDialogBox');
    expect(dialogBox.length).toBe(1);
    const buttons = dialogBox.find('SSSButton');
    expect(buttons.length).toBe(2);
    const inputs = dialogBox.find('[type="text"]');
    expect(inputs.length).toBe(2);
});

test('loadErrorDialog renders on popUpLoadErrorDialogBox()', () =>{
    const loadDialogErrorRender = jest.fn();
    const titleScreen = mount(<TitleScreen/>);
    titleScreen.instance().loadDialogErrorRender = loadDialogErrorRender;
    titleScreen.instance().popUpLoadErrorDialogBox();
    expect(titleScreen.instance().DialogBox.state.show).toBe(true);
    expect(loadDialogErrorRender).toBeCalled();
});

test('load dialog error box renders correctly', ()=>{
    const titleScreen = mount(<TitleScreen/>);
    titleScreen.instance().popUpLoadErrorDialogBox();
    titleScreen.update();
    const dialogBox = titleScreen.find('SSSDialogBox');
    expect(dialogBox.length).toBe(1);
    const buttons = dialogBox.find('SSSButton');
    expect(buttons.length).toBe(1);
    const inputs = dialogBox.find('[type="text"]');
    expect(inputs.length).toBe(0);
});

test('saveErrorDialog renders on popUpSaveErrorDialogBox()', () =>{
    const saveDialogErrorRender = jest.fn();
    const titleScreen = mount(<TitleScreen />);
    titleScreen.instance().saveDialogErrorRender = saveDialogErrorRender;
    titleScreen.instance().popUpSaveErrorDialogBox();
    expect(titleScreen.instance().DialogBox.state.show).toBe(true);
    expect(saveDialogErrorRender).toBeCalled();
});

test('save dialog error box renders correctly', () => {
    const titleScreen = mount(<TitleScreen />);
    titleScreen.instance().popUpSaveErrorDialogBox();
    titleScreen.update();
    const dialogBox = titleScreen.find('SSSDialogBox');
    expect(dialogBox.length).toBe(1);
    const buttons = dialogBox.find('SSSButton');
    expect(buttons.length).toBe(1);
    const inputs = dialogBox.find('[type="text"]');
    expect(inputs.length).toBe(0);
});

test('load options box renders on popUpLoadOptionsBox()', () => {
    const mockLoadOptionsBox = jest.fn();
    const titleScreen = mount(<TitleScreen alertHandler={() =>{}}/>);
    const testGitName = 'gitName';
    const testGitEmail = 'gitEmail';
    titleScreen.instance().setState({gitName: testGitName});
    titleScreen.instance().setState({gitEmail: testGitEmail});

    titleScreen.instance().loadOptionsBox = mockLoadOptionsBox;
    titleScreen.instance().popUpLoadOptionsBox();
    expect(titleScreen.instance().DialogBox.state.show).toBe(true);
    expect(mockLoadOptionsBox).toBeCalled();
});

test('load option buttons change state properly', () => {
    const titleScreen = mount(<TitleScreen />);
    const mockDirectoryButtonClick = jest.fn();
    titleScreen.instance().handleDirectoryButtonClick = mockDirectoryButtonClick;
    const testGitName = 'gitName';
    const testGitEmail = 'gitEmail';
    titleScreen.instance().setState({gitName: testGitName});
    titleScreen.instance().setState({gitEmail: testGitEmail});

    titleScreen.instance().popUpLoadOptionsBox();
    titleScreen.update();
    const dialogBox = titleScreen.find('SSSDialogBox');
    expect(dialogBox.length).toBe(1);

    const localButton = dialogBox.find('[type="radio"]').at(0);
    expect(localButton.length).toBe(1);
    expect(localButton.prop('checked')).toBe(false);
    localButton.simulate('change', { target: { value: 'local' } });
    expect(titleScreen.instance().state.loadOption).toBe('local');

    const gitCloneButton = dialogBox.find('[type="radio"]').at(1);
    expect(gitCloneButton.length).toBe(1);
    expect(gitCloneButton.prop('checked')).toBe(false);
    gitCloneButton.simulate('change', { target: { value: 'gitClone' } });
    expect(titleScreen.instance().state.loadOption).toBe('gitClone');
    expect(localButton.prop('checked')).toBe(false);

    const mockInputVal = 'hello';
    let gitCloneRepoInput = dialogBox.find('[type="text"]');
    expect(gitCloneRepoInput.length).toBe(1);
    expect(gitCloneRepoInput.prop('value')).toBe('');
    gitCloneRepoInput.simulate('change', { target: { value: mockInputVal } });
    expect(titleScreen.instance().state.gitCloneRepo).toBe(mockInputVal);
    titleScreen.update();
    gitCloneRepoInput = titleScreen.find('SSSDialogBox').find('[type="text"]');
    expect(gitCloneRepoInput.length).toBe(1);
    expect(gitCloneRepoInput.prop('value')).toBe(mockInputVal);

    const buttons = dialogBox.find('button');
    expect(buttons.length).toBe(3);
    const directoryControlButton = buttons.at(0);
    directoryControlButton.simulate('click');
    expect(mockDirectoryButtonClick).toBeCalled();
});

test('generateInitializationFile correctly accesses git settings', () => {
    let results = '';
    const mockWriteFileSync = jest.fn((first, second) => {
        results = second;
    });
    const mockGetElectron = jest.fn(() => ({ require: () => ({ writeFileSync: mockWriteFileSync }) }));
    const titleScreen = mount(<TitleScreen />);
    const testGitName = 'gitName';
    const testGitEmail = 'gitEmail';
    titleScreen.instance().setState({ gitName: testGitName });
    titleScreen.instance().setState({ gitEmail: testGitEmail });
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().generateInitializationFile();
    expect(mockGetElectron).toBeCalled();
    expect(mockWriteFileSync).toBeCalled();
    expect(results.match(`git config user.name "${testGitName}"`).length).toBe(1);
    expect(results.match(`git config user.email "${testGitEmail}"`).length).toBe(1);
});

test('startMissionFromXMLFile starts new mission', ()=> {
    const mockPathJoin = jest.fn((first, second)=>first + second);
    const mockExistsSync = jest.fn(()=>true);
    const mockDirname = jest.fn();
    const mockInitializeDocker = jest.fn(() => {
        const proc = {
            stdout: {
                on: (first, second) => {
                    second();
                }
            }
        };
        return {
            then: (first) => {
                first(proc);
            }
        };
    });
    const mockGetElectron = jest.fn(() => ({
        require: () => ({
            existsSync: mockExistsSync,
            dirname: mockDirname,
            join: mockPathJoin
        })
    }));
    const titleScreen = mount(<TitleScreen />);
    const testXML = '/test/test.xml';
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().initializeDocker = mockInitializeDocker;
    titleScreen.instance().startMissionFromXML(testXML);
    expect(mockGetElectron).toBeCalled();
    expect(mockExistsSync).toBeCalled();
    expect(mockInitializeDocker).toBeCalled();
});

test('startMissionFromXMLFile shows dialog when file does not exist', () => {
    const mockAlertHandler = jest.fn();
    const mockPathJoin = jest.fn((first, second)=>first + second);
    const mockShowMessageBox = jest.fn(()=> ({
        then: (first)=>{
            first(0);
        }
    }));
    const mockExistsSync = jest.fn(() => false);
    const mockDirname = jest.fn();
    const mockInitializeDocker = jest.fn();
    const mockGetElectron = jest.fn(() => ({
        dialog: { showMessageBox: mockShowMessageBox },
        require: () => ({
            existsSync: mockExistsSync,
            dirname: mockDirname,
            join: mockPathJoin
        })
    }));
    const titleScreen = mount(<TitleScreen alertHandler={mockAlertHandler} />);
    const testXML = '/test/test.xml';
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().initializeDocker = mockInitializeDocker;
    titleScreen.instance().startMissionFromXML(testXML);
    expect(mockGetElectron).toBeCalled();
    expect(mockExistsSync).toBeCalled();
    expect(mockInitializeDocker).toBeCalledTimes(0);
    expect(mockAlertHandler).toBeCalled();
});

test('gitClone alerts if fields are not filled out', () => {
    const testName = 'name';
    const mockAlertHandler = jest.fn();
    const mockGenerateInitializationFile = jest.fn();
    const mockStartMissionFromXML = jest.fn();
    const mockGetElectron = jest.fn(() => ({
        require: () => ({
            exec: (first, second) => {
                second();
                return {
                    stderr: {
                        on: (first, second) => {
                            second('');
                        }
                    },
                    on: (first, second) => {
                        second(0);
                    }
                };
            },
            basename: () => { },
            parse: () => ({ name: testName }),
            join: (first, second) => (first + second)
        })
    }));
    const titleScreen = mount(<TitleScreen alertHandler={mockAlertHandler} />);
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().generateInitializationFile = mockGenerateInitializationFile;
    titleScreen.instance().startMissionFromXML = mockStartMissionFromXML;
    titleScreen.instance().gitClone();
    expect(mockAlertHandler).toBeCalledTimes(1);
    titleScreen.setState({ targetDirectory: 'testDirectory' });
    titleScreen.instance().gitClone();
    expect(mockAlertHandler).toBeCalledTimes(2);
});

test('gitClone startsMissionFromXML if no error', (done) => {
    const mockAlertHandler = jest.fn(() => { });
    const mockBaseName = jest.fn((name) => name);
    const mockParse = jest.fn(() => ({ name: 'Foo Bar' }));
    const mockJoin = jest.fn((first, second) => first + second);
    const mockGenerateInitializationFile = jest.fn();
    function mockGetElectron() {
        return {
            require: () => ({
                basename: mockBaseName,
                parse: mockParse,
                join: mockJoin
            })
        };
    }
    function mockStartMissionFromXML() {
        expect(mockGenerateInitializationFile.mock.calls.length).toBe(1);
        expect(mockBaseName.mock.calls.length).toBe(1);
        expect(mockParse.mock.calls.length).toBe(1);
        expect(mockJoin.mock.calls.length).toBe(1);
        done();
    }
    const titleScreen = mount(<TitleScreen alertHandler={mockAlertHandler} />);
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().generateInitializationFile = mockGenerateInitializationFile;
    titleScreen.instance().startMissionFromXML = mockStartMissionFromXML;
    titleScreen.setState({ targetDirectory: 'testDirectory', gitCloneRepo: 'testRepo' });
    titleScreen.instance().gitClone();
});

test('handleDirectoryButtoncClick switches targetDirectory', () => {
    const mockGetElectron = jest.fn(() => ({
        dialog: {
            showOpenDialog: (first, second, third) => {
                third();
                const loadedFile = {
                    filePaths: ['filePath'],
                    canceled: false
                };
                return {
                    then: (first) => {
                        first(loadedFile);
                    }
                };
            }
        },
        getCurrentWindow: () => { }
    }));
    const titleScreen = mount(<TitleScreen />);
    titleScreen.instance().getElectron = mockGetElectron;
    titleScreen.instance().handleDirectoryButtonClick();
    expect(mockGetElectron).toBeCalledTimes(1);
    expect(titleScreen.instance().state.targetDirectory).toBe('filePath');
});

test('handleLoadOptionsContinue calls gitclone() and loadMission()', () => {
    const mockGitClone = jest.fn();
    const mockLoadMission = jest.fn();
    const mockAlertHandler = jest.fn();
    const titleScreen = mount(<TitleScreen alertHandler={mockAlertHandler} />);
    titleScreen.instance().gitClone = mockGitClone;
    titleScreen.instance().loadMission = mockLoadMission;
    titleScreen.instance().handleLoadOptionsContinue();
    expect(mockAlertHandler).toBeCalledTimes(1);
    expect(mockGitClone).toBeCalledTimes(0);
    expect(mockLoadMission).toBeCalledTimes(0);

    titleScreen.instance().setState({ loadOption: 'gitClone' });
    titleScreen.instance().handleLoadOptionsContinue();
    expect(mockAlertHandler).toBeCalledTimes(1);
    expect(mockGitClone).toBeCalledTimes(1);
    expect(mockLoadMission).toBeCalledTimes(0);

    titleScreen.instance().setState({ loadOption: 'local' });
    titleScreen.instance().handleLoadOptionsContinue();
    expect(mockAlertHandler).toBeCalledTimes(1);
    expect(mockGitClone).toBeCalledTimes(1);
    expect(mockLoadMission).toBeCalledTimes(1);
});

test('popUpNotifyUpdate', ()=>{
    const mockPull = jest.fn(async()=>{});
    Docker.pull = mockPull;
    const titleScreen = mount(<TitleScreen startMission={() => { }} alertHandler={()=>{}} />);
    titleScreen.instance().popUpNotifyUpdate();
    titleScreen.update();
    let dialogBox = titleScreen.find('SSSDialogBox');
    expect(dialogBox).toBeTruthy();
    let buttons = dialogBox.find('button');
    expect(buttons.length).toBe(2);
    buttons.forEach((button)=>{
        button.simulate('click');
    });
    expect(mockPull.mock.calls.length).toBe(1);
});

describe('initializeDocker', () => {
    const ORIGINAL_STOP = Docker.stop;
    const ORIGINAL_REMOVE = Docker.removeContainer;
    const ORIGINAL_RUN = Docker.run;
    test('a container gets ran', async (done) => {
        Docker.stop = jest.fn();
        Docker.removeContainer = jest.fn();
        Docker.run = jest.fn((container, object) => {
            expect(object.src).toBe('//c/foo/bar');
            done();
        });
        const titleScreen = shallow(<TitleScreen />);

        await titleScreen.instance().initializeDocker('c:\\foo\\bar');
        expect(Docker.stop.mock.calls.length).toBe(1);
        expect(Docker.removeContainer.mock.calls.length).toBe(1);
        expect(Docker.run.mock.calls.length).toBe(1);
    });


    test('funciton still works with errors thrown', async (done) => {
        Docker.stop = jest.fn(() => {
            throw 1;
        });
        Docker.removeContainer = jest.fn(() => {
            throw 1;
        });
        Docker.run = jest.fn((container, object) => {
            expect(object.src).toBe('//c/foo/bar');
            done();
        });
        const titleScreen = shallow(<TitleScreen />);

        await titleScreen.instance().initializeDocker('c:\\foo\\bar');
        expect(Docker.stop.mock.calls.length).toBe(1);
        expect(Docker.removeContainer.mock.calls.length).toBe(1);
        expect(Docker.run.mock.calls.length).toBe(1);
    });

    Docker.stop = ORIGINAL_STOP;
    Docker.removeContainer = ORIGINAL_REMOVE;
    Docker.run = ORIGINAL_RUN;
});
