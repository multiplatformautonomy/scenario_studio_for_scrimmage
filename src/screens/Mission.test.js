import React from 'react';
import {mount, shallow }from 'enzyme';
import RealMission from './Mission';
import *as readModule from '../utilities/parseXML';
import Docker from '../utilities/Docker';
import UpdateMissionXML from '../utilities/UpdateMissionXML';
import MissionScreen from './MissionScreen';
MissionScreen.prototype.getElectron = ()=>({
    require: require,
    getCurrentWindow: ()=>({
        setSize:()=>{},
        center:()=>{}
    })
});
jest.mock('../components/Editor3D.js');
jest.mock('../utilities/Docker');
jest.mock('../utilities/parseXML');

class Mission extends RealMission {
    updateMissionXmlFile() {
        return; 
    }
}

let mission;
beforeEach(() => {
    mission = mount(<Mission screenHandler={()=>{}} xmlFilePath={'screens/LiveMission.test.xml'} showScrimmage={()=>{}}/>,
        {attachTo: document.body });
});

afterEach(()=>{
    mission.unmount();
});

test('Mission Screen\'s sidebar adds to entity list', ()=>{
    // create an entity
    const openMenu = mission.find('SSSButton').at(1);
    openMenu.simulate('click');

    expect(mission.state('entityList').length).toBe(0);

    const buttonCollection = mission.find('SSSButton');
    buttonCollection.forEach((button, i)=>{
        if(i > 1 && i < 6){
            button.prop('clickHandler')();
            button.simulate('click');
        }
    });

    const collection = mission.state('entityList');
    
    expect(collection.length).toBe(4);
});

test('Click trashcan removes entity from list', ()=>{
    const openMask = mission.find('SSSButton').at(1);
    openMask.simulate('click');
    const openMenu = mission.find('SSSButton').at(4);

    expect(mission.state('entityList').length).toBe(0);
    
    openMenu.prop('clickHandler')();
    openMenu.simulate('click');

    // Card is a flexbox with an li (card_component) inside.
    expect(mission.state('entityList').length).toBe(1);

    // Click the trashcan button on the Entity.
    mission.find('Entity').find('li').at(2).simulate('click');
    expect(mission.state('entityList').length).toBe(0);
});

test('Mission Screen opens properties panel on asset list item click', ()=>{
    expect(mission.state('entityList').length).toBe(0);

    // open entity list
    const openMenu = mission.find('SSSButton').at(1);
    openMenu.simulate('click');
    const buttonCollection = mission.find('SSSButton');
    buttonCollection.forEach((button, i)=>{
        if(i > 1 && i < 6){
            button.prop('clickHandler')();
            button.simulate('click');
        }
    });

    const collection = mission.state('entityList');
    
    expect(collection.length).toBe(4);

    const assetCard = mission.find('ul').find('li').first();
    
    expect(assetCard).toBeTruthy();

    // Each Entity card has two li. First is card_component & second is trashcan.
    assetCard.find('li').first().simulate('click');

    expect(mission.instance().state.selectedCard).toBeTruthy();
    expect(mission.find('PropertiesPanel').length).toBeTruthy();
});

test('Mission Screen renders plugin modal dialog', ()=>{
    expect(mission.find('SSSDialogBox')).toHaveLength(1);

    // create an entity
    const openMenu = mission.find('SSSButton').at(1);
    openMenu.simulate('click');

    const entityButton = mission.find('SSSButton').at(2); // airplane entity button
    entityButton.instance().props.clickHandler();
    entityButton.simulate('click');

    // open the PropertiesPanel
    const assetCard = mission.find('ul').find('li').first();
    assetCard.simulate('click');

    // open the autonomy menu
    const autonomyButton = mission.find('PropertiesPanel').find('SSSButton').first();
    autonomyButton.instance().props.clickHandler();
    autonomyButton.simulate('click');
    
    expect(mission.find('SSSDialogBox')).toHaveLength(3);
});

test('update name test', () => {
    let id = 'mission-settings';

    mission.instance().updateName(id, 'hello');
    expect(mission.state('missionName')).toBe('hello'); 
});

test('update mission name', () => {
    let value = 'hello';

    mission.instance().updateMissionName('world', value);
    expect(mission.state('missionName')).toStrictEqual('hello');
}); 

test('Load in an entity and check to see if the props are passed in and working', () => {
    let data = {
        data: {
            runscript: {
                entity: [
                    {
                        name: 'hello',
                        icon: ['foo']
                    }
                ]
            }
        }
    };
  
    let entityList = mission.instance().loadEntitiesFromXml(data);
    expect(entityList[0]).not.toBe(undefined);
    mission.instance().removeEntity = jest.fn(()=>{});
    mission.instance().clickCard = jest.fn(()=>{});
    mission.instance().updateEntityProperties = jest.fn(()=>{});
    mission.instance().updateName = jest.fn(()=>{});
    entityList[0].props.clickRemoveHandler();
    entityList[0].props.clickHandler();
    entityList[0].props.updateData();
    entityList[0].props.updateName();
    expect(mission.instance().removeEntity).toBeCalled();
    expect(mission.instance().clickCard).toBeCalled();
    expect(mission.instance().updateEntityProperties).toBeCalled();
    expect(mission.instance().updateName).toBeCalled();
});

test('addNewPlugin can add a plugin', ()=>{
    mission.instance().addNewPlugin('autonomy', 'FOOBAR');
    mission.instance().addNewPlugin('autonomy', 'BARFOO');
    expect(mission.state('pluginList')['autonomy']).toContain('FOOBAR.xml');
});

test('put plugin sets proper plugin states', () => {
    let plugins = ['world.xml', 'foo.xml', 'bar.xml'];
    mission.instance().sortScrimmagePlugins(plugins);
});

test('initialize mission from file using filepath', () => {
    let expected = {data: {runscript: {$: {name: 'hello'}}}};
    const mockLoadSettings = jest.fn((data) => data);
    const mockLoadEntities = jest.fn((first, second)=>second);

    function readParse(obj) {
        obj.readParseFile = jest.fn().mockResolvedValue(expected);
    }
    readParse(readModule);
    jest.mock('./MissionScreen', () => jest.fn().mockImplementation(() => ({loadMissionSettingsFromXml: mockLoadSettings})));
    jest.mock('./MissionScreen', () => jest.fn().mockImplementation(() => ({loadEntitiesFromXml: mockLoadEntities})));
    expect(mission.instance().initializeMissionFromFile()).toBe(undefined);
});  

test('simulate actions on the missionEntity returned from loadMissionSettingsFromXml', ()=>{
    let data = {data: {runscript: {hello: 'world'}}};
    let missionEntity = mission.instance().loadMissionSettingsFromXml(data, 'world');
    expect(missionEntity).not.toBe(undefined);
    mission.instance().clickcard = jest.fn(()=>{});
    mission.instance().getPluginParams = jest.fn(()=>{});
    missionEntity.props.clickHandler();
    missionEntity.props.getPluginParams();   
});

test('add newEntity properly concats with mission state', () => {
    mission.instance().addNewEntity('hello', 'world');
    expect(mission.state('entityList')).not.toBe(undefined);
    mission.instance().getPluginParams = jest.fn(()=>{});
    mission.state('entityList')[0].props.getPluginParams(); 
    expect(mission.instance().getPluginParams).toBeCalled();     
});

test('Sort scrimmage plugins into their arrays based on plugin type.', async(done)=>{
    jest.mock('../utilities/UpdateMissionXML');
    let subProc = {
        stdin:{
            setEncoding: ()=>{},
            write: ()=>{}
        },
        stdout:{
            setEncoding: ()=>{},
            on: (first, callback)=>callback('XML_DIR /plugins/autonomy')
        },
        on: (___, func)=>{
            func();
        }
    };
    mission.unmount();
    const HOLD_EXEC = Docker.exec;
    Docker.exec = async()=>subProc;
    UpdateMissionXML._updateMissionSettingsXml = jest.fn();
    mission = shallow(<Mission 
        screenHandler={()=>{}} 
        xmlFilePath={'screens/LiveMission.test.xml'} 
        showScrimmage={()=>{}}/>);
    let arr = new Array(120);
    arr.fill('Foo');
    await mission.instance().sortScrimmagePlugins(arr);
    expect(mission.state('pluginList')['autonomy'][0]).toBe('Foo');
    Docker.exec = HOLD_EXEC;
    done(); 
    jest.unmock('../utilities/UpdateMissionXML');
});

test('ResetPlugins empties the plugin list', ()=>{
    mission.instance().resetPlugins();
    let testPluginList = {
        autonomy:[],
        controller:[],
        interaction:[],
        metrics:[],
        motion:[],
        network:[],
        sensor:[]
    };
    expect(mission.state('pluginList')).toStrictEqual(testPluginList);
});

test('getCopiedProperties retrieves an object that can be loaded into Entity calls', ()=> {
    const Card = require('../components/card/Entity').default;
    let card = new Card({name:'hello', faIcon:'plane'});
    let copiedProps = mission.instance().getCopiedProperties(card);
    // copies the properties of card (in this case - card used default values)
    expect(copiedProps.color).toEqual(['blue']);
    expect(copiedProps.speed).toEqual(['30']);
});

test('getCopiedProperties works with plugins', ()=> {
    let card = {
        state:{
            propertiesData:{
                properties:[
                    // eslint-disable-next-line array-element-newline
                    {name:'team_id', value: ['1']},
                    {name:'speed', value:['30']}
                ],
                enabledPlugins:[
                    {
                        name:'ArduPilot',
                        pluginType:'autonomy', 
                        args: [{name:'from_ardupilot_port', value:'5002'}, {name:'to_ardupilot_port', value:'5003'}]
                    }
                ]
            }
        }
    };

    let copiedProps = mission.instance().getCopiedProperties(card);
    // plugin properties are copied
    expect(copiedProps['team_id']).toEqual(['1']);
    expect(copiedProps['speed']).toEqual(['30']);
    // plugin name is copied
    expect(copiedProps.autonomy[0]['_']).toEqual('ArduPilot');
    // all plugin args are correctly copied over.
    expect(copiedProps.autonomy[0]['$']['from_ardupilot_port']).toEqual('5002');
    expect(copiedProps.autonomy[0]['$']['to_ardupilot_port']).toEqual('5003');
});

test('new entity with props is created', ()=> {
    const Card = require('../components/card/Entity').default;
    let card = new Card({name:'hello', faIcon:'plane'});
    mission.instance().duplicateEntity(card);
    expect(mission.state('entityList').length).toBe(1);
    expect(mission.state('entityList')[0].props.name).toEqual('hello copy');
});

test('duplicate calls add new entity and update', ()=> {
    let card = {
        props: {
            name: 'hello',
            faIcon: 'plane',
            id: 'origUUID'
        }
    };
    let mockUpdate = jest.fn().mockResolvedValue('mockUUID');
    mission.instance().duplicateEntity = mockUpdate;
    mission.instance().duplicateEntity(card);
    expect(mockUpdate).toBeCalled();
    expect(mission.instance().duplicateEntity).toBeCalledWith(card);
});
