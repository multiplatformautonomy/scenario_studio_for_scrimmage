import React from 'react';
import propTypes from 'prop-types';
import './Mission.css';
import Button from '../components/Button';
import Mask from '../components/Mask';
import Sidebar from '../components/Sidebar';
import Card from '../components/card/Card';
import PropertiesPanel from '../components/PropertiesPanel';
import DialogBox from '../components/dialogs/DialogBox';
import CreatePlugin from '../components/dialogs/CreatePlugin';
import Docker from '../utilities/Docker';
import Editor3D from '../components/Editor3D';
import LoadingView from '../components/LoadingView';
import Git from '../utilities/Git';
import Batch from '../components/managers/BatchManager';
import PytorchEditor from './codeEditor/PytorchEditor';


//FontAwesome Dependencies and Setup
import {dom, library }from '@fortawesome/fontawesome-svg-core';
import {
    faCar,
    faShip,
    faCog,
    faFighterJet,
    faCity,
    faPlayCircle,
    faSave,
    faArrowLeft,
    faPlus,
    faFileImport,
    faFileCode,
    faTrash,
    faUpload,
    faToggleOff,
    faLayerGroup,
    faScroll,
    faPlusSquare,
    faPlay,
    faCopy,
    faBook,
    faEdit
}from '@fortawesome/free-solid-svg-icons';
import {faDocker}from '@fortawesome/free-brands-svg-icons';
library.add(
    faCar,
    faShip,
    faCog,
    faFighterJet,
    faCity,
    faPlayCircle,
    faSave,
    faArrowLeft,
    faPlus,
    faFileImport,
    faFileCode,
    faTrash,
    faUpload,
    faToggleOff,
    faLayerGroup,
    faScroll,
    faDocker,
    faPlusSquare,
    faPlay,
    faCopy,
    faBook,
    faEdit
);
dom.watch();

export default class MissionScreen extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            dialogRender:()=>{},
            plugSelect: undefined,
            dialogTitle: '',
            images:[],
            selectedImage:'multiplatformautonomy/vscrimmage-vnc:stable',
            useOtherImage: false,
            otherImage: '',
            pythonOutput: [],
            codeEditorPath: '',
            screenMode:'mission',
            commitMessage:'',
            compileOutput: [],
            compileErrors: 0,
            imageLoading: false,
            pluginFilter: ''
        };

        this.getElectron().getCurrentWindow().setSize(1600, 700);
        this.getElectron().getCurrentWindow().center();
        this.DialogBox = React.createRef();
        this.editor = React.createRef();
        this.LoadingView = React.createRef();
        this.OutputEnd = React.createRef();
        this.popUpBox = this.handlePluginPrompt.bind(this);
        this.handleImageSelect = this.handleImageSelect.bind(this);
        this.openPluginMenu = this.openPluginMenu.bind(this);
        this.batchBox = this.batchBox.bind(this);
        this.selectImageBox = this.selectImageBox.bind(this);
        this.openPythonFile = this.openPythonFile.bind(this);
        this.renderMissionContainer = this.renderMissionContainer.bind(this);
        this.renderScreenMode = this.renderScreenMode.bind(this);
        this.renderCodeEditor = this.renderCodeEditor.bind(this);
        this.dataToState = this.dataToState.bind(this);
        this.handleCommit = this.handleCommit.bind(this);
        this.commitBox = this.commitBox.bind(this);
        this.compileOutputBox = this.compileOutputBox.bind(this);
        this.getPluginList = this.getPluginList.bind(this);
    }
    getElectron() {
        return window.require('electron').remote;
    }
    
    openPluginMenu(plugSelect){
        this.setState({
            plugSelect:plugSelect,
            pluginFilter: ''
        });
        this.pluginMenu.setShow(true);
    }
    getPluginListNames(){
        // flattening the arrays using concat
        let list = this.props.pluginList.autonomy.concat(this.props.pluginList.controller, this.props.pluginList.interaction,
            this.props.pluginList.interaction, this.props.pluginList.metrics, this.props.pluginList.motion,
            this.props.pluginList.network, this.props.pluginList.sensor) ;



        return list; 
    }

    getPluginList(){
        let selectedCard = this.props.selectedCard;
        let list = [];
        let plugin;
        switch (this.state.plugSelect){
        case undefined: return;
        case 'autonomy':plugin = this.props.pluginList.autonomy;break;
        case 'controller':plugin = this.props.pluginList.controller;break;
        case 'interaction':plugin = this.props.pluginList.interaction;break;
        case 'metrics':plugin = this.props.pluginList.metrics;break;
        case 'motion':plugin = this.props.pluginList.motion;break;
        case 'network':plugin = this.props.pluginList.network;break;
        case 'sensor':plugin = this.props.pluginList.sensor;break;
        default:console.warn('Unknown plugin type: ', this.state.plugSelect); return;
        }

        plugin.forEach((item, index)=>{
            let pluginName = item.replace('.xml', '');
            // Retrieves the card color in case if the card was unmounted.
            let color = 'white';
            selectedCard.state.propertiesData.enabledPlugins.forEach((plugin)=>{
                if(pluginName === plugin.name) {
                    color = 'paleGreen';
                }
            });

            let isDisabled = false;
            selectedCard.state.disabledPluginButtons.forEach((disabledName) => {
                if(pluginName === disabledName) {
                    isDisabled = true;
                }
            });

            list.push(<Card key={`asset-${index}`}
                clickHandler={()=>selectedCard.updateEnabledPlugins(pluginName)}
                name={pluginName}
                color={color}
                isDisabled={isDisabled}
                isPropertyCard={true}
                faIcon={'fas fa-file-code fa-2x'}
            />);
        });
        return list;
    }
    
    createPlugin(name, pluginType) {
        if(name.includes(' ')){
            this.props.alertHandler('Error', 'Plugin names cannot have any spaces');
            return;
        }
        if(name !== '' && pluginType !== ''){
            const remote = this.getElectron();
            const path = remote.require('path')
            ;   
            let compilePluginCommand = 
                `/home/scrimmage/scrimmage/scripts/generate-plugin.sh ${pluginType} ${name} /missionMount/ProjectPlugins`;

            Docker.exec('sss_scrimmage', compilePluginCommand).then((proc)=>{
                let dirPath = this.props.xmlFilePath;
                let pluginFiles = [];

                pluginFiles.push(path.normalize(`${dirPath}/ProjectPlugins/include/` 
                    + `ProjectPlugins/plugins/${pluginType}/${name}/${name}.xml`));
                pluginFiles.push(path.normalize(`${dirPath}/ProjectPlugins/include/` 
                    + `ProjectPlugins/plugins/${pluginType}/${name}/${name}.h`));
                pluginFiles.push(path.normalize(`${dirPath}/ProjectPlugins/src` 
                    + `/plugins/${pluginType}/${name}/${name}.cpp`));

                return new Promise((resolve)=>{
                    proc.on('exit', resolve);
                }).then((exitCode)=>{
                    if(!exitCode){
                        this.props.openCodeEditor(pluginFiles);
                        this.props.addNewPlugin(pluginType, name);
                    }
                });
            }
            );
        }
    }


    goBack() {
        this.props.screenHandler('titleScreen');
    }
    
    commitBox(ref){
        return <>
            <label>
                <div className='dialog-label-text'>
                    Message:
                </div>
                <input className='dialog-text-input' type='text' autoFocus
                    value={this.state.commitMessage} onChange={(event)=>{
                        this.setState({commitMessage: event.target.value});
                    }}/>
            </label>
            
            <div className='dialog-button-container'>
                <Button className='dialog-button-blue'
                    clickHandler={()=>this.handleCommit()}>
                    Commit </Button>
                <Button className='dialog-button-red' clickHandler={()=>ref.current.setState({show:false})
                }>Cancel</Button>
            </div>
        </>;
    }
        
    

    batchBox(ref){
        return <Batch
            onRun={async(command)=>{
                Docker.stopAndRemove('batch_scrimmage');

                await new Promise((resolve)=>Docker.run(
                    this.state.selectedImage, 
                    {
                        name:'batch_scrimmage', 
                        interactive:true,
                        source:this.props.xmlFilePath, 
                        dst:'/experiments'
                    },
                    '/bin/bash'
                ).then((subProc)=>{
                    subProc.stdout.setEncoding('utf8');
                    subProc.stdout.on('data', resolve);
                    subProc.stderr.setEncoding('utf8');
                    subProc.stderr.on('data', console.error);
                    Docker.copyToContainer(`${this.props.xmlFilePath}`,
                        'batch_scrimmage',
                        '/home/scrimmage/scrimmage/missions/MASS_mission.xml');
                    subProc.stdin.setEncoding('utf8');
                    subProc.stdin.write(command + '\n');
                }));
            }}

            onCancel={()=>ref.current.setShow(false)}
        />;
    }

    loadDockerImages(){
        Docker.getImages().then((images) =>{
            let options = [];
            images.forEach((image) => {
                let val = image['REPOSITORY'] +  ':' + image['TAG'];
                options.push({value:val, label:val});
            });
            this.setState({images: options});
        });
    }
    
    selectImageBox(ref){
        return <div className='select_image_container'>
            <div className='dialog-label-text' style={{display:'flex', alignItems:'center'}}>
                Available Docker Images:
                {this.state.imageLoading ? <LoadingView
                    ref={(node)=>{
                        this.selectImageLoad = node;
                    }}
                    show={true}
                    static={true}
                    size={'16px'}
                    style={{margin:'5px'}}
                />
                    : ''}
            </div>
            <div>
                <select
                    className='dialog-text-input'
                    value={this.state.selectedImage}
                    onChange={(event) => {
                        this.setState({selectedImage: event.target.value});
                    }} disabled={this.state.useOtherImage}>
                    {this.state.images.map(({value, label}, index) => <option value={value} key={index}>{label}</option>)}
                </select>
            </div>
            <label className='dialog-label-inline'>
                <div className='dialog-check-input'>
                    <input type='checkbox' name='image-check' checked={this.state.useOtherImage} onChange={(event)=>{
                        this.setState({useOtherImage: event.target.checked});
                    }}/>
                </div>

                <div className='dialog-label-text'>
                    Use Other
                </div>
            </label>
            <div>
                <input type='text' className='dialog-text-input'
                    name='image-text' value={this.state.otherImage}
                    disabled={!this.state.useOtherImage} 
                    style={{width:'100%', boxSizing:'border-box'}}
                    onChange={(event)=>{
                        this.setState({otherImage:event.target.value});
                    }}/>
            </div>
            <div className='dialog-button-container'>
                <Button className='dialog-button-blue' clickHandler={() => {
                    this.handleImageSelect();
                }}>Save</Button>
                <Button className='dialog-button-red' clickHandler={()=>ref.current.setShow(false)
                }>Cancel</Button>
            </div>
        </div>;
    }

    compileOutputBox(){
        let objs = [];
        for(const [index, entry]of this.state.compileOutput.entries()) {
            objs.push(<div className={'python-output-line ' + entry.style}
                key={index}> {entry.output}</div>);
        }
        return <>
            <div style={{width:'600px', height:'300px'}}className='python-output-content'>
                {objs}
                <div ref={(node)=>{
                    this.outputEnd = node;
                }}></div>
            </div>
            {this.state.compileErrors > 1 ? 
                <div className='dialog-button-container'>
                    <Button className='dialog-button-red' clickHandler={()=>this.DialogBox.current.setState({show:false})
                    }>Close</Button>
                </div>
                : ''}
        </>;
    }

    handleImageSelect(){
        this.setState({imageLoading: true});
        Docker.stopAndRemove('sss_scrimmage');
        const filepath = this.props.xmlFilePath;
        let dirAlt = filepath.replace(/\\/g, '/').replace(/c:\//gi, '//c/');
        let image = this.state.useOtherImage ? this.state.otherImage : this.state.selectedImage;
        const path = require('path');
        Docker.run(image, {
            src: dirAlt,
            destination: '/missionMount',
            port: ['6901:6901', '8888:8888'],
            name: 'sss_scrimmage',
            envFileName:path.normalize(global.__assets + '/env_file')
        }).then(async (proc)=>{
            proc.stderr.setEncoding('utf8');
            let errorMessage = '';
            proc.stderr.on('data', (error)=>{
                errorMessage += error + '\n';
            });
            await new Promise((resolve)=>proc.stdout.on('data', resolve));
            proc.on('close', (code) => {
                if(code !== 0) {
                    this.props.alertHandler('Error', errorMessage);
                }
            });
            Docker.exec('sss_scrimmage', '/bin/bash /missionMount/.initialize.sh').then((output) => {
                let errorMessage2 = '';
                output.stdout.setEncoding('utf8');
                output.stderr.setEncoding('utf8');
                output.stderr.on('data', (error)=>{
                    errorMessage2 += error + '\n';
                });
                output.on('close', (code)=>{
                    if(code !== 0) {
                        this.props.alertHandler('Error', errorMessage2);
                    }else {
                        this.props.alertHandler('Success', 'Switched to image: ' +  image);
                        this.props.resetPlugins();
                    }
                    this.setState({imageLoading: false});
                });
            });
            Docker.exec('sss_scrimmage', '/bin/bash').then((proc) => {
                proc.stdout.setEncoding('utf8');
                proc.stdout.on('data', console.log);
                proc.stderr.setEncoding('utf8');
                proc.stderr.on('data', console.log);
                proc.stdin.setEncoding('utf8');
                proc.stdin.write(`export PATH="$HOME/.local/bin:$PATH";\n
                    jupyter lab --ip 0.0.0.0 --port 8888 --NotebookApp.token='' /missionMount;\n
                    exit;\n`);
            });
        });
    } 

    handleCommit() {
        if(this.state.commitMessage) {
            Git.gitContainerCommit('sss_scrimmage', this.state.commitMessage);
        }
        this.DialogBox.current.setState({show: false});
        this.setState({commitMessage: ''});
    }

    //Generic dialogBox opener
    // which describes the content of dialog box
    openDialogBox(which) {
        switch (which){
        case 'commit': 
            this.setState({
                commitMessage: '',
                dialogRender:this.commitBox,
                dialogTitle:'Create a commit message'
            });
            break;
        case 'batch':
            // chick if python bindings for scrimmage are setup for batch runs
            this.setState({
                dialogRender:this.batchBox,
                dialogTitle: 'Batch setup'
            });
            break;
        case 'select-image':
            this.loadDockerImages();
            this.setState({
                dialogRender:this.selectImageBox,
                dialogTitle: 'Select Docker Image'
            });
            break;
        case 'compile':
            this.setState({
                dialogRender:this.compileOutputBox,
                dialogTitle: 'Compile Output'
            });
            break;
        default: 
            this.setState({
                dialogRender: ()=>{},
                dialogTitle: ''
            });
        }
        this.DialogBox.current.setShow(true);
    }

    handlePluginPrompt(){
        // Electron dialog does not support prompts, nor complex dialog messages.
        this.dialogBox.setShow(true);
    }

    dataToState(data, type) {
        data = data.trim();
        let outputs = data.split('\n');
        let objs = outputs.map((entry)=>({output: entry, style: type}));
        let update = this.state.compileOutput.concat(objs);
        this.setState({compileOutput: update});
    }

    async buildScrimmageEnv(){
        let compilePluginCommand = 'cd /missionMount/ProjectPlugins ; ';
        compilePluginCommand += 'mkdir -p build ; cd build ; '; 
        compilePluginCommand += 'cmake .. ; make -j 8 ; ';
        compilePluginCommand += 'source /home/scrimmage/.scrimmage/setup.bash; exit \n'; // checks for errors
        
        this.setState({compileOutput: []});
        this.setState({compileErrors: 0});
        this.openDialogBox('compile');
        let errors = 0;

        // builds the env, updates the user with console output, and tracks errors that may occur
        await Docker.exec('sss_scrimmage', '/bin/bash').then((subProc)=>{
            subProc.stdout.setEncoding('utf8');
            subProc.stdout.on('data', (data) => {
                this.dataToState('' + data);
                if(this.outputEnd){
                    this.outputEnd.scrollIntoView();
                }
            });
            subProc.stderr.setEncoding('utf8');
            subProc.stderr.on('data', (data)=>{
                this.dataToState('' + data, 'error');
                if(this.outputEnd){
                    this.outputEnd.scrollIntoView();
                }
                errors += 1;
            });
            subProc.stdin.setEncoding('utf8');
            subProc.stdin.write(compilePluginCommand + '\n');
            return new Promise((resolve) =>{
                subProc.on('close', resolve);
            });
        });
        return errors;
    }

    async runScrimmageMission() {
        // kills the vnc if there is an instance running
        let killCommand = '"pkill scrimmage"';
        Docker.bashExec('sss_scrimmage', killCommand);

        let errors = await this.buildScrimmageEnv();
        this.setState({compileErrors: errors});

        // Opens vScrimmage and opens the mission by sourcing the bash file and invoking scrimmage.
        if(this.state.compileErrors === 1) { // the one error is a locale err.
            this.props.showScrimmage();
            let runMissionCommand = 'source /home/scrimmage/.scrimmage/setup.bash;';
            runMissionCommand += 'scrimmage /missionMount/GeneratedMission.xml ; exit \n';
            this.DialogBox.current.setState({show: false});
            Docker.exec('sss_scrimmage', '/bin/bash').then((subProc)=>{
                subProc.stderr.setEncoding('utf8');
                subProc.stderr.on('data', (data)=>{
                    console.log('error' + data);
                });
                subProc.stdin.write(runMissionCommand);
            });
        }
    }

    openPythonFile() {
        const path = this.getElectron().require('path');
        let filename = 'openAI_train.py';
        let filepath = path.normalize(`${this.props.xmlFilePath}/${filename}`);
        const electron = this.getElectron();
        const fs = electron.require('fs');

        if(!fs.existsSync(filepath)) {
            const path = require('path');
            const srcPath = path.normalize(global.__assets + '/' + filename);
            fs.copyFile(srcPath, filepath, (err) => {
                if(err){
                    throw err;
                }else {
                    this.setState({codeEditorPath: filepath});
                    this.setState({screenMode: 'code'});
                }
            });
        }else {
            this.setState({codeEditorPath: filepath});
            this.setState({screenMode: 'code'});
        }
    }

    getFilteredPluginList(){
        // Apply the plugin filter to the plugin list and return the filtered list
        let pluginList = this.getPluginList();
        let pluginFilter = this.state.pluginFilter;

        let filteredPluginList = [];
        if(pluginFilter === ''){
            filteredPluginList = pluginList; // no filter applied
        }else {
            pluginList.forEach((plugin)=>{
                let pluginName = plugin.props.name.toLowerCase(); // prevent capital filtering
                if(pluginName.includes(pluginFilter.toLowerCase())){
                    filteredPluginList.push(plugin);
                }
            });
        }
        return filteredPluginList;
    }

    renderPluginPanel(){
        let list = [];
        list.push(<div key='create_plugin' style={{backgroundColor:'rgb(34, 139, 34)', margin:'3.5px'}}>
            <Button clickHandler={()=>this.handlePluginPrompt()} faIcon='fas fa-plus fa-2x'/>
        </div>);

        list.push(<div className='filterInputBorder'><input className='filterInput'
            autoFocus
            onChange={(event)=>{
                this.setState({pluginFilter: event.target.value}); // update filter
            }}
        /></div>);
        
        list.push(this.getFilteredPluginList());
        return list;
    }

    renderMissionContainer(){
        return <>
            <Sidebar>
                <div > 
                    <Button  clickHandler={()=>this.goBack()}
                        tag='back'
                        className='backButton'
                        message='Go back to main menu'
                        faIcon='fas fa-arrow-left fa-2x'/>

                    <Mask
                        default={<Button 
                            className='sidebarButton'
                            clickHandler={ () => {
                                this.entityMask.toggleOpen(!this.entityMask.state.isOpen);
                            }}
                            tag='add entities'
                            message='Hide entities'
                            faIcon='fas fa-plus fa-3x'/>    
                        }

                        ref={(ref)=>{
                            this.entityMask = ref;
                        }}
                    >
                        <Button clickHandler={()=>this.props.addNewEntity('aircraft', 'fas fa-fighter-jet fa-2x')}
                            className='sidebarButton' 
                            tag='aircraft'
                            message='Create aircraft entity'
                            faIcon='fas fa-fighter-jet fa-3x' />
                        <Button clickHandler={()=>this.props.addNewEntity('watercraft', 'fas fa-ship fa-2x')}
                            className='sidebarButton' 
                            tag='boat'
                            message='Create watercraft entity'
                            faIcon='fas fa-ship fa-3x'/>
                        <Button clickHandler={()=>this.props.addNewEntity('landcraft', 'fas fa-car fa-2x')}
                            className='sidebarButton'
                            tag='car'
                            message='Create car entity'
                            faIcon='fas fa-car fa-3x'/>
                        <Button clickHandler={()=>this.props.addNewEntity('stationary', 'fas fa-city fa-2x')}
                            className='sidebarButton'
                            tag='city'
                            message='Create stationary entity'
                            faIcon='fas fa-city fa-3x'/>
                    </Mask>
                    
                    


                    <Button clickHandler={()=>this.openDialogBox('commit')}
                        className='sidebarButton'
                        tag='commit'
                        message='Commit any changes to git'
                        faIcon='fas fa-upload fa-3x'
                    />
                    <Button message='Use Jupyter lab' 
                        tag='jupyter'
                        clickHandler={() =>this.props.showJupyter()}
                        faIcon='fas fa-book fa-3x'
                    />
                    <Button message='Open Code Editor'
                        tag='Code Editor'
                        clickHandler={()=>
                            this.props.openCodeEditor([this.props.xmlFilePath + '/GeneratedMission.xml'])
                        }
                        faIcon='fas fa-edit fa-3x'
                    />
                    <Mask
                        default={<Button className='sidebarButton'
                            clickHandler={() => {
                                this.settingsMask.toggleOpen(!this.settingsMask.state.isOpen);
                            }}
                            tag='settings'
                            message='Open settings'
                            faIcon='fas fa-cog fa-3x'/>
                        }
                        
                        ref={(ref)=>{
                            this.settingsMask = ref;
                        }}
                    >
                        <Button className='sidebarButton' clickHandler={()=>this.openDialogBox('select-image')}
                            tag='docker'
                            message='Select docker image'
                            faIcon='fab fa-docker fa-3x'/>
                    </Mask>

                    
                </div>

                <div className='spacer'/>

                <div>
                    <Mask
                        default={<Button 
                            className='sidebarButton'
                            clickHandler={() => {
                                this.runMask.toggleOpen(!this.runMask.state.isOpen);
                            }}
                            tag='run show'
                            message='Run mission'
                            faIcon='fas fa-play-circle fa-3x'/>
                        }

                        ref={(ref)=>{
                            this.runMask = ref;
                        }}
                    >
                        <Button clickHandler={()=>this.openDialogBox('batch')} 
                            className='sidebarButton'
                            tag='batch'
                            message='Run batch mission in the background'
                            faIcon='fas fa-layer-group fa-3x'/>
                        <Button clickHandler={this.openPythonFile} 
                            className='sidebarButton'
                            tag='python'
                            message='OpenAI script editor'
                            faIcon='fas fa-scroll fa-3x'/>
                        <Button clickHandler={()=>this.runScrimmageMission()} 
                            className='sidebarButton'
                            tag='run'
                            message='Run single mission with viewer'
                            faIcon='fas fa-play fa-3x'>
                            
                            <sup className='sup'>1</sup></Button>
                    </Mask>
                </div>
            </Sidebar>
            

            <div className='mission_container' style={{width: '195px'}}>
                {this.props.missionSettings}
                <ul>
                    {this.props.entityList}
                </ul>
            </div>
            {this.props.selectedCard &&
                    <><Sidebar ref={(err)=>{
                        this.pluginMenu = err;
                    }} show={false} className='menu right' closeable style={{overflow:'hidden'}}>
                        <DialogBox ref={(ref)=>{
                            this.dialogBox = ref;
                        }} title='Create Custom Plugin'>
                            <div > 
                                <CreatePlugin onClose={()=>{
                                    this.dialogBox.setShow(false);
                                }} onCreate={(name, pluginType)=>this.createPlugin(name, pluginType)}
                                pluginType={this.state.plugSelect}
                                pluginList={ this.getPluginListNames()}
                                onClick={(event) =>  event.preventDefault()}
                                />
                            </div>
                        </DialogBox>
                        {this.props.pluginList.autonomy.length === 0 ? <LoadingView
                            ref={(node)=>{
                                this.loadingView = node;
                            }}
                            show={true}
                            style={{padding: '70px'}}
                        ></LoadingView> : ''}
                        <div style={{overflowY:'auto', overflowX:'hidden'}}>
                            {this.renderPluginPanel()}
                        </div>
                    </Sidebar>
                    <PropertiesPanel 
                        card={this.props.selectedCard}
                        updatePropertiesData={(key, value)=>this.props.selectedCard.updatePropertiesData(key, value)}
                        entityProperties={this.props.entityProperties}
                        entityList={this.props.entityList}
                        updateAllMissionProperties={()=>this.props.updateAllMissionProperties()}
                        updateName={(name, err)=>this.props.selectedCard.updateName(name, err)}
                        openPluginMenu={this.openPluginMenu}
                        openCodeEditor={this.props.openCodeEditor}
                        xmlFilePath={this.props.xmlFilePath}
                    /></>}
        </>;
    }

    renderCodeEditor(){
        return <PytorchEditor
            pythonFileName={'openAI_train.py'}
            pythonFilePath={this.state.codeEditorPath}
            onBack={()=>{
                this.setState({screenMode: 'mission'});
            }} 
        />;
    }

    renderScreenMode(){
        switch (this.state.screenMode) {
        case 'mission' : return this.renderMissionContainer();
        case 'code' : return this.renderCodeEditor();
        default: console.warn('unknown screen mode ' + this.state.screenMode);
        }
    }

    render(){
        return (
            <div className='container'>
                {this.renderScreenMode()}
                <DialogBox ref={this.DialogBox}
                    title={this.state.dialogTitle}>
                    {this.state.dialogRender(this.DialogBox, this)}
                </DialogBox>
                
                <Editor3D ref={this.editor}
                    card={this.props.selectedCard}
                    entityList={this.props.entityProperties}
                    show={false}
                    missionSettings={this.props.missionSettings}
                    entityComponents={this.props.entityList}
                    initializeMission={this.props.initializeMission}/>
            </div>
        );
    }
}

MissionScreen.propTypes = {
    selectedCard: propTypes.object,
    pluginList: propTypes.object,
    missionSettings: propTypes.object,
    xmlFilePath: propTypes.string,
    entityList: propTypes.array,
    addNewEntity: propTypes.func,
    addNewPlugin:  propTypes.func,
    showScrimmage: propTypes.func,
    alertHandler: propTypes.func,
    screenHandler: propTypes.func,
    updatePropertiesData: propTypes.func,
    updateName: propTypes.func,
    entityProperties: propTypes.object,
    openCodeEditor:propTypes.func,
    resetPlugins: propTypes.func,
    updateAllMissionProperties: propTypes.func,
    initializeMission: propTypes.func,
    showJupyter: propTypes.func
};
