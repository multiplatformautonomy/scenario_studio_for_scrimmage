import React from 'react';
import { shallow, mount }from 'enzyme';
import RealMissionScreen from './MissionScreen';
import 'fs';
jest.mock('fs');
jest.mock('../utilities/Docker');
jest.mock('../components/Editor3D.js');

class MissionScreen extends RealMissionScreen {
    updateMissionXmlFile() {
        return; 
    }
    getElectron(){
        return {
            require: ()=>({
                normalize: jest.fn((path)=>path),
                readFileSync: jest.fn(()=>'')
            }),
            getCurrentWindow: ()=>({
                setSize:()=>{},
                center:()=>{}
            })
        };
    }
}

function setupScreenWithPlugins(){
    let pluginList = {
        autonomy:['Straight.xml', 'ROSAirSim.xml', 'ROSControl.xml', 'Predator.xml', 'TakeFlag.xml'],
        controller: ['SingleIntegratorControllerWaypoint.xml'],
        interaction:[],
        metrics:[],
        motion:[],
        network:[],
        sensor:[]
    };
    
    let selectedCard = {
        state: {
            propertiesData: {enabledPlugins: [{name: 'controller'}]},
            disabledPluginButtons: ['controller']
        }
    };

    return shallow(<MissionScreen 
        screenHandler={()=>{}} pluginList={pluginList} selectedCard={selectedCard}
    />);
}

test('Mission screen renders', ()=>{
    const missionScreen = shallow(<MissionScreen screenHandler={()=>{}} />);
    expect(missionScreen.instance()).toBeTruthy();
});

test('Mission Screen has 11 icon buttons in sidebar', () => {
    const missionScreen = shallow(<MissionScreen screenHandler={()=>{}}/>);
    const sidebar = missionScreen.find('SSSSidebar').first();
    const collection = sidebar.findWhere((node) => node.name() === 'SSSButton');
    expect(collection).toHaveLength(12);
});

test('Get Plugin List', () => {
    const missionScreen = setupScreenWithPlugins();
    missionScreen.setState({plugSelect: 'controller'});

    const result = missionScreen.instance().getPluginList();
    expect(result[0]).not.toBe(undefined);
    expect(result).toHaveLength(1);
});

describe('Plugin Panel tests', () => {
    const missionScreen = setupScreenWithPlugins();
    missionScreen.setState({plugSelect: 'autonomy', pluginFilter:''});

    test('All plugins render when there is no filter set', () => {
        let filteredPluginList = missionScreen.instance().getFilteredPluginList();
        expect(filteredPluginList).toHaveLength(5);
    });

    test('Filter plugin 1', () => {
        missionScreen.setState({pluginFilter:'ROS'});
        let filteredPluginList = missionScreen.instance().getFilteredPluginList();
        expect(filteredPluginList).toHaveLength(2);
    });

    test('Filter plugin 2', () => {
        missionScreen.setState({pluginFilter:'predator'});
        let filteredPluginList = missionScreen.instance().getFilteredPluginList();
        expect(filteredPluginList).toHaveLength(1);
        expect(filteredPluginList[0].props.name).toBe('Predator');
    });
});

test('Plugin list switch cases', () => {
    // Each plugin list should be empty, aside from the 'Add custom plugin' button.
    const missionScreen = setupScreenWithPlugins();

    missionScreen.setState({plugSelect: 'interaction'});
    expect(missionScreen.instance().getPluginList()).toStrictEqual([]);

    missionScreen.setState({plugSelect: 'metrics'});
    expect(missionScreen.instance().getPluginList()).toStrictEqual([]);

    missionScreen.setState({plugSelect: 'motion'});
    expect(missionScreen.instance().getPluginList()).toStrictEqual([]);

    missionScreen.setState({plugSelect: 'network'});
    expect(missionScreen.instance().getPluginList()).toStrictEqual([]);

    missionScreen.setState({plugSelect: 'sensor'});
    expect(missionScreen.instance().getPluginList()).toStrictEqual([]);

    missionScreen.setState({plugSelect: 'undefined'});
    expect(missionScreen.instance().getPluginList()).toStrictEqual(undefined);
});

test('simulate settings and play button and test handle plugin', () => {
    const missionScreen = mount(<MissionScreen xmlFilePath = '/path/to/Dummy.file'/>);
    const mockOpenMenu = jest.fn(()=>{});
    missionScreen.instance().openMenu = mockOpenMenu;
    missionScreen.instance().run = mockOpenMenu;

    const mockSet = jest.fn(()=>{});
    missionScreen.instance().dialogBox = {setShow: mockSet};
    missionScreen.instance().handlePluginPrompt();
    expect(mockSet.mock.calls.length).toBe(1);
});

test('Docker run', async(done)=>{
    const MOCK = jest.fn(()=>{});
    const mockDataToState = jest.fn();
    const mockScrollIntoView = jest.fn();
    await new Promise((resolve)=>{
        const missionScreen = shallow(<MissionScreen showScrimmage={()=>resolve(MOCK())}/>);
        missionScreen.instance().dataToState = mockDataToState;
        missionScreen.instance().DialogBox = {current: {setShow: ()=>{}, setState: ()=>{}}};
        missionScreen.instance().outputEnd = {scrollIntoView: mockScrollIntoView};
        missionScreen.instance().runScrimmageMission();
    });
    expect(mockDataToState).toBeCalled();
    expect(mockScrollIntoView).toBeCalled();
    expect(MOCK.mock.calls.length).toBe(1);

    done();
});

/**
test('if open menu works', () => {
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    missionScreen.instance().assetMenu = {setState: jest.fn(()=>{})};
    missionScreen.instance().openMenu();
});
*/

test('if the back button properly executes commands', () => {
    const mockGetElectron = jest.fn(()=>({require: jest.fn(()=>({execSync: jest.fn(()=>{})}))}));

    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    missionScreen.instance().getElectron = mockGetElectron;

    missionScreen.instance().goBack();
    missionScreen.find('SSSButton').first().instance().props.clickHandler();
});

test('different types of dialogboxes', ()=>{
    const mockGetElectron = jest.fn(()=>({require: jest.fn(()=>({execSync: jest.fn(()=>{})}))}));

    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    missionScreen.instance().getElectron = mockGetElectron;
    missionScreen.instance().openDialogBox('commit');
    missionScreen.instance().openDialogBox('batch');
    missionScreen.instance().openDialogBox('select-image');
    missionScreen.instance().openDialogBox('UNKOWN');
});

test('commit dialogBox buttons', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    missionScreen.instance().openDialogBox('commit');
    missionScreen.update();
    let dialogbox = missionScreen.find('SSSDialogBox');
    expect(dialogbox.length).toBe(1);
    let buttonCollection = dialogbox.find('button');
    expect(buttonCollection.length).toBe(2);
    buttonCollection.forEach((button)=>{
        button.simulate('click');
    });
});

test('batch dialogBox buttons', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    missionScreen.instance().openDialogBox('batch');
    missionScreen.update();
    let dialogbox = missionScreen.find('SSSDialogBox');
    expect(dialogbox.length).toBe(1);
    let buttonCollection = dialogbox.find('button');
    expect(buttonCollection.length).toBe(2);
    buttonCollection.forEach((button)=>{
        button.simulate('click');
    });
});


test('select-image dialogBox buttons', ()=>{
    const missionScreen = mount(<MissionScreen  screenHandler={()=>{}} alertHandler={()=>{}}/>);
    const mockHandleImageSelect = jest.fn(() => {});
    missionScreen.instance().handleImageSelect = mockHandleImageSelect;
    const mockLoadDockerImages = jest.fn(() => {});
    missionScreen.instance().loadDockerImages = mockLoadDockerImages;
    missionScreen.setState({
        images :[{value:'val', label:'tag'}],
        selectedImage: 'image'
    });
    missionScreen.instance().openDialogBox('select-image');
    missionScreen.update();
    expect(mockLoadDockerImages).toBeCalled();
    
    let dialogbox = missionScreen.find('SSSDialogBox');
    expect(dialogbox.length).toBe(1);
    
    let select = dialogbox.find('select');
    expect(select.length).toBe(1);
    select.simulate('change', {target: {value : '100'}});

    let check = dialogbox.find({type:'checkbox'});
    expect(check.length).toBe(1);
    expect(check.props().checked).toBe(false);
    check.simulate('change', {target: {checked: true}});
    expect(missionScreen.instance().state.useOtherImage).toBe(true);

    let textInput = dialogbox.find({type:'text'});
    expect(textInput.length).toBe(1);
    expect(textInput.props().value).toBe('');
    textInput.simulate('change', {target: {value: 'new Image'}});
    expect(missionScreen.instance().state.otherImage).toBe('new Image');


    let buttonCollection = dialogbox.find('button');
    expect(buttonCollection.length).toBe(2);
    buttonCollection.forEach((button)=>{
        button.simulate('click');
    });
});

test('loadDockerImages executes', () => {
    const missionScreen = mount(<MissionScreen  screenHandler={()=>{}} alertHandler={()=>{}}/>);
    const mockSetState = jest.fn(()=>{});
    missionScreen.instance().setState = mockSetState;
    missionScreen.instance().loadDockerImages();
    expect(mockSetState).toBeCalled();
});


test('commit dialog box sets state when user enters message', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    const mockHandleCommit = jest.fn();
    missionScreen.instance().handleCommit = mockHandleCommit;
    missionScreen.instance().openDialogBox('commit');
    missionScreen.update();
    const dialogBox = missionScreen.find('SSSDialogBox');
    expect(dialogBox.length).toBe(1);
    const textInput = dialogBox.find('[type="text"]');
    expect(textInput.length).toBe(1);
    textInput.simulate('change', {target:{value:'commitMessage'}});
    expect(missionScreen.instance().state.commitMessage).toBe('commitMessage');
    const continueButton = dialogBox.find('button').at(0);
    continueButton.simulate('click');
    expect(mockHandleCommit).toBeCalled();
});

test('handleCommit', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    const mockHandleCommit = jest.fn();
    missionScreen.instance().handleCommit = mockHandleCommit;
    missionScreen.instance().openDialogBox('commit');
    missionScreen.update();
    const dialogBox = missionScreen.find('SSSDialogBox');
    expect(dialogBox.length).toBe(1);
    const textInput = dialogBox.find('[type="text"]');
    expect(textInput.length).toBe(1);
    textInput.simulate('change', {target:{value:'commitMessage'}});
    expect(missionScreen.instance().state.commitMessage).toBe('commitMessage');
    const continueButton = dialogBox.find('button').at(0);
    continueButton.simulate('click');
    expect(mockHandleCommit).toBeCalled();
});

test('if handleCommit executes', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    missionScreen.instance().handleCommit();
    missionScreen.setState({commitMessage: 'commitMessage'});
    missionScreen.instance().handleCommit();
});

test('handleImageSelect executes', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}} alertHandler={()=>{}} xmlFilePath=''/>);
    const mockReplace = jest.fn(() => ({replace: jest.fn()}));
    const mockGetPath = jest.fn(() => ({replace: mockReplace}));
    missionScreen.instance().getPath = mockGetPath;
    missionScreen.instance().handleImageSelect();
});

test('handleImageSelect throws error', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}} alertHandler={()=>{}} xmlFilePath=''/>);
    missionScreen.setState({selectedImage: 'error'});
    const mockReplace = jest.fn(() => ({replace: jest.fn()}));
    const mockGetPath = jest.fn(() => ({replace: mockReplace}));
    missionScreen.instance().getPath = mockGetPath;
    missionScreen.instance().handleImageSelect();
});

test('handleImageSelect uses otherImage field', ()=>{
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}} alertHandler={()=>{}} xmlFilePath=''/>);
    missionScreen.setState({selectedImage: 'error', useOtherImage: true, otherImage:'image'});
    const mockReplace = jest.fn(() => ({replace: jest.fn()}));
    const mockGetPath = jest.fn(() => ({replace: mockReplace}));
    missionScreen.instance().getPath = mockGetPath;
    missionScreen.instance().handleImageSelect();
});

test('get electron', () => {
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    missionScreen.instance().getElectron();
});

test('openPythonFile creates new file and sets screenMode to code', () => {
    let filepath = '/path/to/file';
    const mockNormalize = jest.fn((path)=>path);
    const mockGetPath = jest.fn(()=>filepath);
    const mockExistsSync = jest.fn(()=>{
        false;
    });
    const mockCopyFile = jest.fn((first, second, third)=>{
        third(false);
    });
    const mockReadFileSync = jest.fn();
    const mockGetElectron = jest.fn(()=>({
        app: {getAppPath: ()=>{}},
        require: ()=>({
            normalize: mockNormalize,
            readFileSync: mockReadFileSync,
            existsSync: mockExistsSync,
            copyFile: mockCopyFile
        })
    }));
    const mockRenderScreenMode = jest.fn();
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}} xmlFilePath={filepath}/>);
    missionScreen.instance().getElectron = mockGetElectron;
    missionScreen.instance().getPath = mockGetPath;

    missionScreen.instance().renderScreenMode = mockRenderScreenMode;
    missionScreen.instance().openPythonFile();
    expect(mockGetElectron).toBeCalled();
    expect(mockExistsSync).toBeCalled();
    expect(mockCopyFile).toBeCalled();
    expect(mockRenderScreenMode).toBeCalled();
    expect(missionScreen.instance().state.codeEditorPath).toBe(filepath + '/openAI_train.py');
    expect(missionScreen.instance().state.screenMode).toBe('code');
});



test('openPythonFile throws error', () => {
    let filepath = '/path/to/file';
    const mockNormalize = jest.fn((path)=>path);
    const mockExistsSync = jest.fn(()=>{
        false;
    });
    const mockCopyFile = jest.fn((first, second, third)=>{
        third(true);
    });
    const mockReadFileSync = jest.fn();
    const mockGetElectron = jest.fn(()=>({
        app: {getAppPath: ()=>{}},
        require: ()=>({
            normalize:mockNormalize,
            readFileSync: mockReadFileSync,
            existsSync: mockExistsSync,
            copyFile: mockCopyFile
        })
    }));
    const mockRenderScreenMode = jest.fn();
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}} xmlFilePath={filepath}/>);
    missionScreen.instance().getElectron = mockGetElectron;

    try {
        missionScreen.instance().renderScreenMode = mockRenderScreenMode;
    }catch (err){
        null;
    }
    expect(missionScreen.instance().openPythonFile).toThrow();
    expect(mockGetElectron).toBeCalled();
    expect(mockExistsSync).toBeCalled();
    expect(mockCopyFile).toBeCalled();
});

test('openPythonFile creates new file when file does not exist', () => {
    let filepath = '/path/to/file';
    const mockNormalize = jest.fn((path)=>path);
    const mockGetPath = jest.fn(()=>filepath);
    const mockExistsSync = jest.fn(()=>{
        false;
    });
    const mockCopyFile = jest.fn((first, second, third)=>{
        third(false);
    });
    const mockReadFileSync = jest.fn();
    const mockGetElectron = jest.fn(()=>({
        app: {getAppPath: ()=>{}},
        require: ()=>({
            normalize: mockNormalize,
            readFileSync: mockReadFileSync,
            existsSync: mockExistsSync,
            copyFile: mockCopyFile
        })
    }));
    const mockRenderScreenMode = jest.fn();
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}} xmlFilePath={filepath}/>);
    missionScreen.instance().getElectron = mockGetElectron;
    missionScreen.instance().getPath = mockGetPath;
    missionScreen.instance().renderScreenMode = mockRenderScreenMode;
    missionScreen.instance().openPythonFile();
    expect(mockGetElectron).toBeCalled();
    expect(mockExistsSync).toBeCalled();
    expect(mockCopyFile).toBeCalled();
    expect(mockRenderScreenMode).toBeCalled();
    expect(missionScreen.instance().state.codeEditorPath).toBe(filepath + '/openAI_train.py');
    expect(missionScreen.instance().state.screenMode).toBe('code');
});

// test('openPythonFile skips file creation when file exists', () => {
//     let filepath = '/path/to/file';
//     const mockGetPath = jest.fn(()=>filepath);
//     const mockExistsSync = jest.fn(()=>true);
//     const mockCopyFile = jest.fn((first, second, third)=>{
//         third(true);
//     });
//     const mockGetElectron = jest.fn(()=>({
//         app: {getAppPath: ()=>{}},
//         require: ()=>({
//             existsSync: mockExistsSync,
//             copyFile: mockCopyFile
//         })
//     }));
//     const mockRenderScreenMode = jest.fn();
//     const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
//     missionScreen.instance().getElectron = mockGetElectron;
//     missionScreen.instance().getPath = mockGetPath;
//     missionScreen.instance().renderScreenMode = mockRenderScreenMode;
//     missionScreen.instance().openPythonFile();
//     expect(mockCopyFile).toBeCalledTimes(0);
//     expect(mockRenderScreenMode).toBeCalled();
//     expect(missionScreen.instance().state.codeEditorPath).toBe(filepath + '/openAI_train.py');
//     expect(missionScreen.instance().state.screenMode).toBe('code');
// });


describe('createPlugin', ()=>{
    test('createPlugin throws exception if no parameters passed in', ()=>{
        const missionScreen = mount(<MissionScreen xmlFilePath=''/>);
    
        try {
            missionScreen.instance().createPlugin();
        }catch (except){
            expect(except.message).toBe('Cannot read property \'includes\' of undefined');
        }
    
        missionScreen.unmount();
    });
    
    test('createPlugin throws exception if plugin already exists', ()=>{
        const missionScreen = mount(<MissionScreen pluginList = {{Foo:['BAR']}} xmlFilePath=''/>);
    
        try {
            missionScreen.instance().createPlugin('BAR', 'BLAH');
        }catch (except){
            expect(except.message).toBe('Plugin name exists');
        }
    
        missionScreen.unmount();
    }); 
    
    test('createPlugin calls alertHandler if invalid name passed in', ()=>{
        const mockAlert = jest.fn();
        const missionScreen = mount(<MissionScreen pluginList = {{Foo:['BAR']}} xmlFilePath='' alertHandler = {mockAlert} />);
    
        missionScreen.instance().createPlugin('BAR BAR', 'Foo');
        expect(mockAlert.mock.calls.length).toBe(1);
    
        missionScreen.unmount();
    });
    
    test('createPlugin adds new plugin and opens files', (done)=>{
        let mockAddNewPlugin = jest.fn();
        let mockOpenCodeEditor = jest.fn(()=>{
            expect(mockOpenCodeEditor.mock.calls.length).toBe(1);
            done();
        });
        const missionScreen = mount(<MissionScreen 
            pluginList = {{Foo:['BAR']}}
            xmlFilePath=''
            addNewPlugin = {mockAddNewPlugin}
            openCodeEditor = {mockOpenCodeEditor}
        />);
        let mockGetPath = {normalize:()=>{}};
        missionScreen.instance().getPath = ()=>mockGetPath;
    
        try {
            missionScreen.instance().createPlugin('Fighter', 'Foo');
        }catch (except){
            console.error(except);
        }
    
        missionScreen.unmount();
    });
});

test('renderScreenMode for mission param', () => {
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    const mockRenderMissionContainer = jest.fn();
    missionScreen.instance().renderMissionContainer = mockRenderMissionContainer;
    missionScreen.instance().render();
    missionScreen.instance().setState({screenMode: 'mission'});
    expect(mockRenderMissionContainer).toBeCalled();
}); 

test('renderScreenMode for code param', () => {
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    const mockRenderCodeEditor = jest.fn();
    missionScreen.instance().renderCodeEditor = mockRenderCodeEditor;
    missionScreen.instance().render();
    missionScreen.instance().setState({screenMode: 'code'});
    expect(mockRenderCodeEditor).toBeCalled();
}); 

test('renderScreenMode for other param', () => {
    const missionScreen = mount(<MissionScreen screenHandler={()=>{}}/>);
    const mockWarn = jest.fn();
    console.warn = mockWarn;
    missionScreen.instance().render();
    missionScreen.instance().setState({screenMode: ''});
    expect(mockWarn).toBeCalled();
}); 

test('renderCodeEditor returns to mission screen mode on back', () => {
    const mockRenderMissionContainer = jest.fn();
    const missionScreen = mount(<MissionScreen xmlFilePath={'src/screens/LiveMission.test.xml'}/>);
    const pytorchEditor = missionScreen.instance().renderCodeEditor();
    missionScreen.renderMissionContainer = mockRenderMissionContainer();
    pytorchEditor.props.onBack();
    expect(missionScreen.instance().state.screenMode).toBe('mission');
    expect(mockRenderMissionContainer).toBeCalled();
}); 

test('dataToState updates state correctly', () => {
    const testString = 'this is line1\nthis is line2';
    const missionScreen = mount(<MissionScreen xmlFilePath={'src/screens/LiveMission.test.xml'}/>);
    missionScreen.instance().dataToState(testString);
    expect(missionScreen.instance().state.compileOutput.length).toBe(2);
    expect(missionScreen.instance().state.compileOutput[0].output).toBe('this is line1');
    expect(missionScreen.instance().state.compileOutput[1].output).toBe('this is line2');
});

test('openDialogBox renders compileOutputBox', ()=>{
    const missionScreen = mount(<MissionScreen xmlFilePath={'src/screens/LiveMission.test.xml'}/>);
    const testOutput =
        [{output:'text', style:'className'}, {output:'text', style:'className'}, {output:'text', style:'className'}];
    missionScreen.instance().setState({compileOutput: testOutput});
    missionScreen.instance().openDialogBox('compile');
    missionScreen.update();
    expect(missionScreen.instance().state.dialogTitle).toBe('Compile Output');
    const output = missionScreen.find('.python-output-line');
    expect(output.length).toBe(testOutput.length);
    const classOutput = missionScreen.find('.className');
    expect(classOutput.length).toBe(testOutput.length);
});

test('compileOutputBox renders cancel button if > 1 error', ()=>{
    const missionScreen = mount(<MissionScreen xmlFilePath={'src/screens/LiveMission.test.xml'}/>);
    missionScreen.instance().setState({compileErrors: 2});
    missionScreen.instance().openDialogBox('compile');
    missionScreen.update();
    expect(missionScreen.instance().state.dialogTitle).toBe('Compile Output');
    const cancelButton = missionScreen.find('SSSDialogBox').find('SSSButton');
    expect(cancelButton.length).toBe(1);
});

test('compileOutputBox renders no button if <= 1 error', ()=>{
    const missionScreen = mount(<MissionScreen xmlFilePath={'src/screens/LiveMission.test.xml'}/>);
    missionScreen.instance().setState({compileErrors: 1});
    missionScreen.instance().openDialogBox('compile');
    missionScreen.update();
    expect(missionScreen.instance().state.dialogTitle).toBe('Compile Output');
    const cancelButton = missionScreen.find('SSSDialogBox').find('SSSButton');
    expect(cancelButton.length).toBe(0);
});
