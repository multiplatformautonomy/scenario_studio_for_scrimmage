import React from 'react';
import { mount }from 'enzyme';
import UpdateMissionXML from '../utilities/UpdateMissionXML';

import RealMission from './Mission';
import *as readModule from '../utilities/parseXML';
jest.mock('../utilities/Docker');
jest.mock('../components/Editor3D.js');
jest.mock('../screens/MissionScreen');

class Mission extends RealMission {
    updateMissionXmlFile() {
        return; 
    }
}

let mission;
beforeEach(() => {
    mission = mount(<Mission screenHandler={()=>{}} xmlFilePath={'./LiveMission.test.xml'} showScrimmage={()=>{}}/>);
});

test('update mission settings with properties', () => {
    const mockClear = jest.fn(() => {        
        let obj = {data: {runscript: {$: {name: 'hello'}}}};
        return obj;
    });

    const mockUpdate = jest.fn(() => {
        let obj = {data: {runscript: {entity: {0: 'world'}}}};
        return obj;
    });

    const mockUpdatePlugin = jest.fn((first, second)=>{
        console.log(first);
        console.log(second);
    });

    function readParse(obj) {
        obj.readParseFile = jest.fn().mockResolvedValue('placeholder');
    }
    readParse(readModule);

    mission.setState({missionName: 'foo'});

    mission.instance().entityProperties = {
        hello: {
            name: 'foo',
            properties: [{name: 'bar'}]
        }
    };
    
    mission.instance().clearLiveXmlFile = mockClear;
    mission.instance()._updateMissionSettingsXml = mockUpdate;
    mission.instance()._updatePluginXml = mockUpdatePlugin;
    
    let entityList = [
        {
            props: {
                id: 'hello',
                faIcon: 'foo'
            }
        }
    ];
    let missionName = 'foo';

    UpdateMissionXML.updateMissionXmlFile(mission.instance().xmlFilePath, missionName, entityList, {});
});  
