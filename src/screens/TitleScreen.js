import React from 'react';
import './TitleScreen.css';
import './Mission';
import massLogo from '../assets/MASS_logo.png';
import Button from '../components/Button';
import Sidebar from '../components/Sidebar';
import DialogBox from '../components/dialogs/DialogBox';
import propTypes from 'prop-types';
import Docker from '../utilities/Docker';
import LoadingView from '../components/LoadingView';
import {saveJSToXml }from '../utilities/parseXML';
import Git from '../utilities/Git';
//FontAwesome Dependencies and Setup
import {dom, library}from '@fortawesome/fontawesome-svg-core';
import {
    faAsterisk, faFolder, faServer, faTerminal, faCog,
    faUserGraduate, faEdit, faCodeBranch
}from '@fortawesome/free-solid-svg-icons';
library.add(faAsterisk, faFolder, faServer, faCog, faTerminal, faUserGraduate, faEdit, faCodeBranch);
dom.watch();


//----------------------------------

export default class TitleScreen extends React.Component {
    constructor(props){
        super(props);
        this.popUpGitSettingsBox = this.popUpGitSettingsBox.bind(this);
        this.handleGitSettingsSubmit = this.handleGitSettingsSubmit.bind(this);
        this.handleLoadOptionsContinue = this.handleLoadOptionsContinue.bind(this);
        this.gitSettingsBox = this.gitSettingsBox.bind(this);
        this.loadOptionsBox = this.loadOptionsBox.bind(this);
        this.saveDialogErrorRender = this.saveDialogErrorRender.bind(this);
        this.loadDialogErrorRender = this.loadDialogErrorRender.bind(this);
        this.setupVncErrorRender = this.setupVncErrorRender.bind(this);
        this.generateInitializationFile = this.generateInitializationFile.bind(this);
        this.gitClone = this.gitClone.bind(this);
        this.handleDirectoryButtonClick = this.handleDirectoryButtonClick.bind(this);
       
        this.DialogBox = React.createRef();
        this.state = {
            dialogCloseable: true,
            dialogTitle: '',
            dialogRender: ()=>{},
            gitName: '',
            gitEmail: '',
            loadOption: '',
            gitCloneRepo: '',
            targetDirectory: ''
        };
    }

    getElectron(){
        return window.require('electron').remote;
    }

    initializeDocker(filepath){
        // TODO - check if this takes longer than 20 seconds. If so, then Docker is not responding to stopping the container
        // and the user should be notified to manually restart Docker.
        try {
            Docker.stop('sss_scrimmage');
        }catch (error){
            console.error('sss_scrimmage container not running');
        }
        try {
            Docker.removeContainer('sss_scrimmage');
        }catch (error){
            console.error('sss_scrimmage container not running');
        }

        let dirAlt = filepath.replace(/c:\\/gi, '//c/').replace(/\\/g, '/');

        //Check if the stable image is up to date
        return Docker.imageUpToDate('multiplatformautonomy/vscrimmage-vnc', 'stable').catch(()=>true).then((upToDate)=>{
            if(!upToDate){
                return this.popUpNotifyUpdate();
            }
        }).then(()=>{
            const path =  require('path');
            return Docker.run('multiplatformautonomy/vscrimmage-vnc:stable', {
                src: dirAlt,
                destination: '/missionMount',
                port: ['6901:6901', '8888:8888'],
                fullPort: '8888:8888',
                name: 'sss_scrimmage',
                envFileName:path.normalize(global.__assets + '/env_file')
            });
        }
        );
    }

    saveNewMission(savePath){
        try {
            const missionData = require('../assets/MissionXmlTemplate.js').default;
            saveJSToXml(savePath, missionData.data);
        }catch (err){
            console.log('Failed to save a new mission file.');
        }
    }

    createAndStartMission(result){
        const remote = this.getElectron();
        const fs = remote.require('fs');

        let dir = result.filePath;
        fs.mkdirSync(dir);
        this.saveNewMission(dir + '/GeneratedMission.xml'); 
        this.initializeDocker(dir).then(()=>{
            this.LoadingView.setState({show:true});
            this.generateInitializationFile(dir);
        }).then(async () => {
            this.setState({isError: true});
            await this.setupVncDirectories(20);

            this.LoadingView.setState({show:false});
            if(!this.state.isError){
                this.props.startMission(dir);
            }
        });
    }

    async setupVncDirectories(retries) {
        await Docker.exec('sss_scrimmage', '/bin/bash').then((subProc) => new Promise((res) => {
            subProc.stderr.setEncoding('utf8');
            subProc.stderr.on('data', (data) => {
                if(data.trim() === 'foo'){
                    this.setState({isError: false}); // .git and ProjectPlugins directories successfully initialized
                    setTimeout(res, 1000); // give time to set isError=false
                }else {
                    // directories failed to initialize - wait a second and try again.
                    setTimeout(res, 1000);
                }
            });
            subProc.stdin.write('/missionMount/.initialize.sh\necho -e "foo" 1>&2\n');
        }));
        if(this.state.isError && retries > 0){
            return this.setupVncDirectories(retries - 1);
        }else if(!this.state.isError){
            console.log(
                `                ##############################
                #########  Scenario  #########
                #########   Studio   #########
                ########      for     ########
                #######    SCRIMMAGE   #######
                ##############################`
            ); // resolve
        }else {
            // error dialog - container not setup correctly
            this.popUpSetupVncErrorDialogBox();
        }
    }

    createProjectDirectory() {
        if(!this.state.gitName || !this.state.gitEmail) {
            this.props.alertHandler('Error', 'Please set up a github username and email');
            return;
        }
        
        // imports
        const remote = this.getElectron();
        const dialog = remote.dialog;
        const path = remote.require('path');
        dialog.showSaveDialog(
            {title: 'Create Scrimmage Project', buttonLabel : 'Save Project'}
        ).then((saveResult) =>{
            let projectName = path.basename(saveResult.filePath);
            let whiteList = /^[a-zA-Z0-9_-]+$/g;
            if(saveResult.canceled){
                // do nothing
                console.log('User Canceled Directory creation');
            }else if(projectName.match(whiteList) && projectName.length < 80) {
                this.createAndStartMission(saveResult);
            }else {
                // Display error for project name length and no special characters
                this.popUpSaveErrorDialogBox();
            }
        });
    } // need to add catch phrase. To infinity and Beyond!

    loadMission(){
        if(!this.state.gitName || !this.state.gitEmail) {
            this.props.alertHandler('Error', 'Please set up a github username and email');
            return;
        }
        
        // imports
        const remote = this.getElectron();
        const path = remote.require('path');
        remote.dialog.showOpenDialog(
            remote.getCurrentWindow(), 
            {title : 'Select directory containing xml file', properties: ['openDirectory']}
        ).then((loadResult)=>{
            let projectName = path.basename(loadResult.filePaths[0]);
            let whiteList = /^[a-zA-Z0-9_-]+$/g;
            if(loadResult.canceled){
                // do nothing
                console.log('User Canceled Directory load');
            }else if(projectName.match(whiteList) && projectName.length < 80) {
                const xmlDir = loadResult.filePaths[0];
                this.startMissionFromXML(xmlDir);
            }else {
                // Display error for project name length and no special characters
                this.popUpLoadErrorDialogBox();
            }
        });
    }

    generateInitializationFile(dir){
        const electron = this.getElectron();
        const fs = electron.require('fs');
        let execCommand = '#!/bin/bash \n'
        + '/home/scrimmage/scrimmage/scripts/create-scrimmage-project.py ProjectPlugins /missionMount/;\n'
        + 'mkdir /missionMount/ProjectPlugins/build; \ncd /missionMount/ProjectPlugins/build; \ncmake ..;\n'
        + 'source /home/scrimmage/.scrimmage/setup.bash;\n' // TODO: find a way to get around permissions
        + 'cd /missionMount; \ngit init; \ngit add .;'
        + `git config user.name "${this.state.gitName}";\n`
        + `git config user.email "${this.state.gitEmail}";`;
        fs.writeFileSync(dir + '/.initialize.sh', execCommand);
    }

    startMissionFromXML(xmlDir) {
        const electron = this.getElectron();
        const fs = electron.require('fs');
        const path = electron.require('path');
        
        const missionFile = path.join(xmlDir, 'GeneratedMission.xml');
        if(fs.existsSync(missionFile)) {
            this.LoadingView.setState({show: true});
            this.initializeDocker(xmlDir).then(async(proc)=>{
                await new Promise((resolve)=>proc.stdout.on('data', resolve));
                Docker.exec('sss_scrimmage', '/bin/bash', {'u' : 1000, 'tty': true}).then((proc) => {
                    proc.stdout.setEncoding('utf8');
                    proc.stdout.on('data', console.log);
                    proc.stderr.setEncoding('utf8');
                    proc.stderr.on('data', console.log);
                    proc.stdin.setEncoding('utf8');
                    proc.stdin.write(`export PATH="$HOME/.local/bin:$PATH";\n
                        jupyter lab --ip 0.0.0.0 --port 8888 --NotebookApp.token='' /missionMount;\n
                        exit;\n`);
                });
                await new Promise((resolve)=>Docker.exec('sss_scrimmage', '/bin/bash').then((subProc)=>{
                    subProc.on('close', resolve);
                    subProc.stdin.setEncoding('utf8');
                    subProc.stdin.write('cd /missionMount/ProjectPlugins/build; ' +
                    'cmake ..; source /home/scrimmage/.scrimmage/setup.bash; exit\n');
                }));
                this.LoadingView.setState({show: false});
                this.props.startMission(xmlDir);
            });
        }else {
            this.props.alertHandler('Error', 'The directory you selected does not contain the GeneratedMission.xml file');
        }
    }

    advancedMode() {
        this.props.clearMission();
        this.props.showScrimmage();
    }

    popUpNotifyUpdate(){
        this.LoadingView.setState({show:false});
        return new Promise((resolve)=>{
            let render = <>
                Would you like to update this container?
                <br/>
                <br/>
                <br/>
                <div className='container'>
                    <Button className='dialog-button-blue'
                        clickHandler={()=>{
                            Docker.pull('multiplatformautonomy/vscrimmage-vnc:stable').then(()=>{
                                this.LoadingView.setState({show:true});
                                resolve();
                            });
                        }}
                    >Update</Button>
                    <Button className='dialog-button-red'
                        clickHandler={()=>{
                            this.LoadingView.setState({show:true});
                            this.DialogBox.setShow(false);
                            resolve();
                        }}
                    >No</Button>
                </div>
            </>;
            this.setState({
                dialogCloseable: false,
                dialogTitle: 'Update for multiplatformautonomy/vscrimmage-vnc',
                dialogRender: ()=>render
            }, ()=>this.DialogBox.setShow(true));
        });
    }

    popUpGitSettingsBox(){
        const gitSettings = Git.loadGitSettings();
        this.setState({
            gitName: gitSettings.username,
            gitEmail: gitSettings.email,
            dialogTitle: 'Settings for git profile',
            dialogRender: this.gitSettingsBox
        });
        this.DialogBox.setShow(true);
    }

    popUpLoadOptionsBox(){
        if(!this.state.gitName || !this.state.gitEmail) {
            this.props.alertHandler('Error', 'Please set up a github username and email');
            return; 
        }
        this.setState({dialogTitle: 'Load Options', dialogRender: this.loadOptionsBox});
        this.DialogBox.setShow(true);
    }

    popUpLoadErrorDialogBox(){
        this.setState({dialogTitle: 'Load Project Error', dialogRender: this.loadDialogErrorRender});
        this.DialogBox.setShow(true);
    }

    popUpSaveErrorDialogBox(){
        this.setState({dialogTitle: 'Save Project Error', dialogRender: this.saveDialogErrorRender});
        this.DialogBox.setShow(true);
    }
    
    popUpSetupVncErrorDialogBox(){
        this.setState({dialogTitle: 'Start Mission Error', dialogRender: this.setupVncErrorRender});
        this.DialogBox.setShow(true);
    }

    handleGitSettingsSubmit() {
        Git.saveGitSettings({
            gitSettings: {
                username: this.state.gitName,
                email: this.state.gitEmail
            }
        });
        this.DialogBox.setShow(false);
    }

    handleLoadOptionsContinue(){
        switch (this.state.loadOption) {
        case 'gitClone':
            this.gitClone();
            break;
        case 'local':
            this.loadMission();
            this.DialogBox.setShow(false);
            break;
        default:
            this.props.alertHandler('Error', 'Please select an option for loading the mission');
        }
    }
       
    handleDirectoryButtonClick(){
        const electron = this.getElectron();
        const dialogOptions = {
            title : 'Select directory containing xml file',
            properties: ['openDirectory']
        };

        electron.dialog.showOpenDialog(electron.getCurrentWindow(), dialogOptions,
            (loadedFile)=>loadedFile).then((loadedFile)=>{
            if(loadedFile.canceled === false) {
                this.setState({targetDirectory: loadedFile.filePaths[0]});
            }
        });
    }

    handleLoadErrorConfirmation(){
        this.loadMission();
        this.DialogBox.setShow(false);
    }

    handleSaveErrorConfirmation(){
        this.createProjectDirectory();
        this.DialogBox.setShow(false);
    }

    editFile() {
        const electron = this.getElectron();
        electron.dialog.showOpenDialog(electron.getCurrentWindow()).then((chosenFile)=>{
            if(chosenFile.canceled === false){
                this.props.openCodeEditor([chosenFile.filePaths[0]]);
            }
        });
    }

    gitClone() {
        if(this.state.targetDirectory === '') {
            this.props.alertHandler('Error', 'Please choose a target directory for git clone');
            return;
        } 
        if(this.state.gitCloneRepo === '') {
            this.props.alertHandler('Error', 'Please enter a link to the git repo for git clone');
            return;
        }
        const electron = this.getElectron();
        const path = electron.require('path');
        Git.gitClone(this.state.gitCloneRepo, this.state.targetDirectory).then((out)=>{
            let [code, error] = out;
            if(!code){
                const dirName = path.parse(path.basename(this.state.gitCloneRepo)).name;
                const dirPath = path.join(this.state.targetDirectory,  dirName);
                this.generateInitializationFile(dirPath);
                this.startMissionFromXML(dirPath);
                this.DialogBox.setShow(false);
            }else {
                this.props.alertHandler('Error', 'Could not clone repo because of the following error:\n' + error);
            } 
        });
    }

    gitSettingsBox() {
        return <>
            <div>
                <label>
                    <div className='dialog-label-text'>
                            Username:
                    </div>
                    <input className='dialog-text-input' type="text" name="gitName"
                        value={this.state.gitName}
                        onChange={(event)=>{
                            this.setState({gitName: event.target.value});
                        }}/>
                </label>
            </div>
            <div>
                <label>
                    <div className='dialog-label-text'>
                            Email:
                    </div>
                    <input className='dialog-text-input' type="text" name="gitEmail"
                        value={this.state.gitEmail}
                        onChange={(event)=>{
                            this.setState({gitEmail: event.target.value});
                        }}/>
                </label>
            </div>
            <div className='dialog-button-container'>
                <Button className='dialog-button-blue'
                    clickHandler={this.handleGitSettingsSubmit}>
                        Submit</Button>
                <Button className='dialog-button-red'
                    clickHandler={()=>this.DialogBox.setShow(false)}>
                        Cancel</Button>
            </div>
        </>;
    }

    loadOptionsBox(){
        return <>
            <div style={{paddingBottom:'5px'}}>
                Choose how to load your project:
            </div>
            
            <label className='dialog-label-inline'>
                <div className='dialog-radio-input'>
                    <input type="radio" id="local" value="local"
                        onChange={(event)=>this.setState({loadOption: event.target.value})}
                        checked={this.state.loadOption === 'local'}/>
                </div>
                <div className='dialog-label-text'>
                    Local Directory
                </div>
            </label>
            <label className='dialog-label-inline'>
                <div className='dialog-radio-input'>
                    <input type="radio" id="gitClone" value="gitClone"
                        onChange={(event)=>this.setState({loadOption: event.target.value})}
                        checked={this.state.loadOption === 'gitClone'}/>
                </div>
                <div className='dialog-label-text'>
                    Git Clone
                </div>
            </label>
            <div style={{
                display: this.state.loadOption === 'gitClone' ? 'block' : 'none',
                paddingLeft:'5px',
                paddingRight:'5px'
            }}>
                <label>
                    <div className='dialog-label-text'>
                        Link to remote git repository:
                    </div>
                    <input name='gitCloneRepo' className='dialog-text-input' type='text'
                        onChange={(event)=>this.setState({gitCloneRepo: event.target.value})}
                        value={this.state.gitCloneRepo}/>
                </label>
                <label>
                    <div className='dialog-label-text'>
                        Target path for git clone
                    </div>
                    <button style={{fontSize: '10px'}}onClick={this.handleDirectoryButtonClick}>Choose Directory</button>
                    <div style={{display:'inline', fontSize:'10px', paddingLeft:'5px'}}>
                        {this.state.targetDirectory}
                    </div>
                </label>
            </div>
            <div className='dialog-button-container'>
                <Button className='dialog-button-blue'
                    clickHandler={this.handleLoadOptionsContinue}>
                    Continue</Button>
                <Button className='dialog-button-red'
                    clickHandler={()=>this.DialogBox.setShow(false)}>
                    Cancel</Button>
            </div>
        </>;
    }

    loadDialogErrorRender(){
        return <>
            <div style={{paddingButtom:'5px'}}>
                <p>The project name cannot contain special characters or spaces.</p>
                
                <p>It must also be less than 80 characters in length.</p>

                <p>Change the name of the mission file in File Explorer or the Terminal</p>
            </div>

            <div className='dialog-button-container'>
                <Button className='dialog-button-blue'
                    clickHandler={()=>this.handleLoadErrorConfirmation()}>
                    OK
                </Button>
            </div>
        </>;
    }

    saveDialogErrorRender(){
        return <>
            <div style={{paddingBottom:'5px'}}>
                <p>The project name cannot contain special characters or spaces.</p>
                
                <p>It must also be less than 80 characters in length.</p>
            </div>

            <div className='dialog-button-container'>
                <Button className='dialog-button-blue'
                    clickHandler={()=>this.handleSaveErrorConfirmation()}>
                    OK
                </Button>
            </div>
        </>;
    }

    setupVncErrorRender(){
        return <>
            <div style={{paddingBottom:'5px'}}>
                <p>The Docker container is not running properly.</p>

                <p>The virtualized container for Docker is required in order to mount the 
                  SSS mission and run SCRIMMAGE missions.</p>

                <p>Check that Docker is installed and contains the image for multiplatformautonomy/vscrimmage-vnc</p>
            </div>

            <div className='dialog-button-container'>
                <Button className='dialog-button-blue'
                    clickHandler={()=>this.DialogBox.setShow(false)}>
                  OK
                </Button>
            </div>
        </>;
    }

    componentDidMount() {
        const gitSettings = Git.loadGitSettings();
        if(gitSettings.username && gitSettings.email) {
            this.setState({
                gitName: gitSettings.username,
                gitEmail: gitSettings.email
            });
        }
    }

    render(){
        return (
            <div className='container'>
                <LoadingView
                    ref={(node)=>{
                        this.LoadingView = node;
                    }}
                    show={false}
                    size={'64px'}
                    fullScreen={true}
                />
                <Sidebar>
                    <div className="button_container">
                        <Button message='Create a Mission Folder' 
                            tag='create_project'
                            clickHandler={() =>{
                                if(!this.state.gitName || !this.state.gitEmail) {
                                    this.props.alertHandler('Error', 'Please set up a github username and email');
                                    this.popUpGitSettingsBox();
                                    this.createProjectDirectory();
                                }else {
                                    this.createProjectDirectory();
                                }
                            }}
                            faIcon='fas fa-asterisk fa-3x'/>
                        
                        <Button message='Load in a Mission Folder' 
                            tag='load_project'
                            clickHandler={() =>{
                                if(!this.state.gitName || !this.state.gitEmail) {
                                    this.props.alertHandler('Error', 'Please set up a github username and email');
                                    this.popUpGitSettingsBox();
                                    this.popUpLoadOptionsBox();
                                }else {
                                    this.popUpLoadOptionsBox();
                                }
                            }}
                            faIcon='fas fa-folder fa-3x'/>
                        
                        <Button message='Enable Advanced User Mode' 
                            tag='advanced'
                            clickHandler={() =>this.advancedMode()}
                            faIcon='fas fa-terminal fa-3x'/>
                        
                        <Button message='Open Code Editor'
                            tag='edit'
                            clickHandler={() =>this.editFile()}
                            faIcon='fas fa-edit fa-3x'/>
                        
                        <Button message='Git Profile Settings'
                            tag='git_settings'
                            clickHandler={() => this.popUpGitSettingsBox()}
                            faIcon="fas fa-code-branch fa-3x"/>

                    </div>
                    <div className='spacer'/>
                    <div className="learning_container">
                        <Button message='View Documentation'
                            tag='documentation'
                            clickHandler={()=>{
                                const { BrowserWindow } = this.getElectron();
                                let docs = new BrowserWindow({
                                    nodeIntegration: false,
                                    show: false
                                });
                                docs.maximize();
                                docs.loadURL('https://mass.gtri.gatech.edu/docs/sss');
                                docs.once('ready-to-show', ()=>{
                                    docs.show();
                                });
                            }}
                            faIcon='fas fa-user-graduate fa-3x'/>
                    </div>
                </Sidebar>
                <img src={massLogo} alt="mass logo" className="logo_img" draggable="false"></img>
                <DialogBox ref={(node)=>{
                    this.DialogBox = node;
                }}
                key={this.state.dialogCloseable}
                closeable = {this.state.dialogCloseable}
                title={this.state.dialogTitle}>  
                    {this.state.dialogRender()}
                </DialogBox>
            </div>
        );
    }
}

TitleScreen.propTypes = {
    alertHandler: propTypes.func,
    screenHandler: propTypes.func,
    clearMission: propTypes.func,
    openCodeEditor: propTypes.func,
    showScrimmage: propTypes.func,
    startMission: propTypes.func
};
