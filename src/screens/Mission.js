import React from 'react';
import propTypes from 'prop-types';
import './Mission.css';
import Entity from '../components/card/Entity';
import MissionEntity from '../components/card/MissionEntity';
import Docker from '../utilities/Docker';
import {readParseFile}from '../utilities/parseXML';
import UpdateMissionXML from '../utilities/UpdateMissionXML';
import MissionScreen from './MissionScreen';
import VariableTracker from '../components/managers/VariableTracker';

//FontAwesome Dependencies and Setup
import {dom, library }from '@fortawesome/fontawesome-svg-core';
import {
    faCar,
    faShip,
    faCog,
    faFighterJet,
    faCity,
    faPlayCircle,
    faSave,
    faArrowLeft,
    faPlus,
    faFileImport,
    faFileCode,
    faTrash,
    faUpload,
    faLayerGroup,
    faCopy
}from '@fortawesome/free-solid-svg-icons';
library.add(
    faCar,
    faShip,
    faCog,
    faFighterJet,
    faCity,
    faPlayCircle,
    faSave,
    faArrowLeft,
    faPlus,
    faFileImport,
    faFileCode,
    faTrash,
    faUpload,
    faLayerGroup,
    faCopy
);
dom.watch();
//----------------------------------

export default class Mission extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selectedCard: null,
            entityList: [],
            missionSettings:null,
            missionName: 'New Mission',
            pluginList:{
                autonomy:[],
                controller:[],
                interaction:[],
                metrics:[],
                motion:[],
                network:[],
                sensor:[]
            }
        };
        let variableTrackerInst = VariableTracker.getInstance();
        variableTrackerInst.initializeVariables(this.props.xmlFilePath);
        this.loadPlugins();
        this.missionSettingsKey = 'mission-settings';
        this.entityProperties = {};
        this.mounted = false;
    }

    resetPlugins() {
        this.setState({
            pluginList:{
                autonomy:[],
                controller:[],
                interaction:[],
                metrics:[],
                motion:[],
                network:[],
                sensor:[]
            }
        });

        this.loadPlugins();
    }

    loadPlugins(){
        Docker.exec('sss_scrimmage', '/bin/bash').then(async (proc) => {
            let plugins = [];
            proc.stdout.setEncoding('utf8');
            proc.stdout.on('data', (data)=>{
                let plugin = data.match(/([^\xff,^\x20]+)\.xml/g);
                if(plugin) {
                    plugins = plugins.concat(plugin);
                }
            });
            proc.stdin.setEncoding('utf8');
            proc.stdin.write('source /home/scrimmage/.scrimmage/setup.bash; scrimmage-plugin -v \xff; exit \n');

            await new Promise((resolve)=>proc.on('close', resolve));

            this.sortScrimmagePlugins(plugins);
        });
    }

    async sortScrimmagePlugins(plugins){
        let tempPluginList = {
            autonomy:[],
            controller:[],
            interaction:[],
            metrics:[],
            motion:[],
            network:[],
            sensor:[]
        };
        let promises = 0;
        const concurrent = 100;
        let waitID;
        for(let plugin of plugins){
            // eslint-disable-next-line 
            await Docker.exec('sss_scrimmage', '/bin/bash').then(async(proc)=>{
                promises += 1;
                this.mounted = true;
                proc.stdout.setEncoding('utf8');
                proc.stdout.on('data', (data)=>{
                    let match = data.match(/XML_DIR[^]+\/plugins\/(\w+)/);
                    if(match) {
                        tempPluginList[match[1]] = tempPluginList[match[1]].concat(plugin).sort();
                        // this._updatePluginState(match[1], plugin, tempPluginList);
                    }
                });
                proc.stdin.setEncoding('utf8');
                proc.stdin.write(`source /home/scrimmage/.scrimmage/setup.bash; scrimmage-plugin ${plugin}; exit \n`);
                proc.on('close', ()=>{
                    promises -= 1;
                });
                if(promises >= concurrent) {
                    await new Promise((resolve)=>{
                        waitID = setInterval(()=>{
                            if(promises <= 0) {
                                resolve();
                            }    
                        }, 100);
                    });
                    clearInterval(waitID);
                }
            });
        }
        await new Promise((resolve)=>{
            waitID = setInterval(()=>{
                if(promises <= 0) {
                    resolve();
                }    
            }, 100);
        });
        clearInterval(waitID);

        for(let listName in tempPluginList){
            tempPluginList[listName] = tempPluginList[listName]
                .sort((str1, str2)=>str1.toLowerCase().localeCompare(str2.toLowerCase()));
        }

        this.setState({pluginList : tempPluginList});
    }

    addNewPlugin(type, name){
        this.setState((prevState)=>{
            let nextState = prevState;
            nextState.pluginList[type] = prevState.pluginList[type].concat(name + '.xml')
                .sort((str1, str2)=>str1.toLowerCase().localeCompare(str2.toLowerCase()));
            return nextState;
        });
    }

    writeMissionStateToFile(){
        UpdateMissionXML.updateMissionXmlFile(
            this.props.xmlFilePath + '/GeneratedMission.xml', 
            this.state.missionName, this.state.entityList, this.entityProperties,
            VariableTracker.getInstance().missionVariableDefaults
        );
    }

    updateEntityProperties(id, propertiesData){
        this.entityProperties[id] = propertiesData;
        this.writeMissionStateToFile();
        this.forceUpdate(); // refresh for enabled plugins.
    }

    updateAllMissionProperties(){
        this.writeMissionStateToFile();
        this.forceUpdate(); // refresh.
    }

    async updateName(id, name){
        if(id === this.missionSettingsKey){
            await new Promise((resolve)=>this.setState({missionName: name }, resolve));
        }else {
            this.entityProperties[id].name = name;
        }
        this.writeMissionStateToFile();
    }

    updateMissionName(name, value){
        this.setState({missionName:value });
        this.writeMissionStateToFile();
    }

    clickCard(card){
        this.setState({selectedCard: card });
    }

    removeEntity(card){
        // TODO: find solution to remove the entity without iterating through the whole list.
        let entityList = this.state.entityList;
        entityList.forEach((entity, index)=>{
            if(entity.key === card.props.id){
                entityList.splice(index, 1);
            }
        });

        delete this.entityProperties[card.props.id];

        this.setState({
            selectedCard: null,
            entityList: entityList
        });
        this.writeMissionStateToFile();
        let missionCard = document.getElementsByClassName('card_component')[0];
        missionCard.click();
    }

    addNewEntity(str, icon){
        const uuidv4 = require('uuid/v4');
        let uuid = uuidv4();
        this.setState({
            entityList: this.state.entityList.concat(<Entity
                key={uuid}
                id={uuid}
                isRemovable={true}
                clickRemoveHandler={(card)=>this.removeEntity(card)}
                clickCopyHandler={(card)=>this.duplicateEntity(card)}
                clickHandler={(card, data)=>this.clickCard(card, data)}
                updateData = {(id, data)=>this.updateEntityProperties(id, data)}
                updateName = {(id, name)=>this.updateName(id, name)}
                getPluginParams= {(container, str)=>this.getPluginParams(container, str)}
                name={'New Entity'}
                faIcon={icon}
            />)
        });
    }

    getCopiedProperties(card){
        let copiedProperties = {};
        card.state.propertiesData.properties.forEach((prop)=>{
            copiedProperties[prop.name] = prop.value;
        });

        card.state.propertiesData.enabledPlugins.forEach((plugin)=>{
            if(!copiedProperties[plugin.pluginType]){
                // initialize the plugin type if it does not exist
                copiedProperties[plugin.pluginType] = [];
            }
            // append the plugin name and args to the copied property
            let copiedPlugin = {'$':[], '_':[]};
            plugin.args.forEach((arg)=>{
                copiedPlugin['$'][arg.name] = arg.value;
            });
            copiedPlugin['_'] = plugin.name;

            copiedProperties[plugin.pluginType].push(copiedPlugin);
        });

        return copiedProperties;
    }

    async duplicateEntity(card) {
        const uuidv4 = require('uuid/v4');
        let uuid = uuidv4();
        // gets the properties that can be passed on for initializing the entity.
        let copiedProperties = this.getCopiedProperties(card);

        this.setState({
            entityList: this.state.entityList.concat(
                <Entity
                    key={uuid}
                    id={uuid}
                    loadedProperties={copiedProperties}
                    isRemovable={true}
                    clickRemoveHandler={(card)=>this.removeEntity(card)}
                    clickCopyHandler={(card)=>this.duplicateEntity(card)}
                    clickHandler={(card, data)=>this.clickCard(card, data)}
                    updateData = {(id, data)=>this.updateEntityProperties(id, data)}
                    updateName = {(id, name)=>this.updateName(id, name)}
                    name={card.props.name + ' copy'}
                    faIcon={card.props.faIcon}
                />
            )
        });
        
        this.setState({selectedCard: card});
        this.writeMissionStateToFile();
        return uuid;
    }

    loadMissionSettingsFromXml(data, loadedMissionName){
        let missionSettingsData = JSON.parse(JSON.stringify(data.data.runscript));
        delete missionSettingsData['$'];
        delete missionSettingsData['entity'];
        
        return <MissionEntity
            select
            key={this.missionSettingsKey}
            id={this.missionSettingsKey}
            className={'mission'}
            loadedProperties = {missionSettingsData}
            clickHandler={(card, data)=>this.clickCard(card, data)}
            updateData = {(id, data)=>this.updateEntityProperties(id, data)}
            updateName = {(id, name)=>this.updateName(id, name)}
            getPluginParams= {(container, str)=>this.getPluginParams(container, str)}
            name={loadedMissionName}
            faIcon={'fas fa-cog fa mission-icon'}
        />;
    }

    loadEntitiesFromXml(data){
        let newEntityList = [];
        try { 
            data.data['runscript']['entity'].forEach((entity)=>{
                let entityName = 'New Entity';
                if(entity.name) {
                    entityName = entity.name[0];
                }
                const uuidv4 = require('uuid/v4');
                let uuid = uuidv4();
                // Initialize entities from loaded-in xml file and add to list.
                newEntityList = 
                    [
                        ...newEntityList, 
                        <Entity
                            key={uuid}
                            id={uuid}
                            loadedProperties={entity}
                            isRemovable={true}
                            clickRemoveHandler={(card)=>this.removeEntity(card)}
                            clickCopyHandler={(card)=>this.duplicateEntity(card)}
                            clickHandler={(card, data)=>this.clickCard(card, data)}
                            updateData = {(id, data)=>this.updateEntityProperties(id, data)}
                            updateName = {(id, name)=>this.updateName(id, name)}
                            // getPluginParams= {(container, str)=>this.getPluginParams(container, str)}
                            name={entityName}
                            faIcon={'fas fa-fighter-jet fa-2x'}
                        />
                    ];
            });
        }catch (err){
            console.error(err);
        }
        return newEntityList;
    }

    // Takes a new or loaded-in file and set mission and entity states to the file's data.
    initializeMissionFromFile(){
        let filePath = this.props.xmlFilePath + '/GeneratedMission.xml';
        this.updatePromise = readParseFile(filePath).then((data) =>{
            let loadedMissionName = data.data['runscript']['$']['name'];
            let newMissionSettings = this.loadMissionSettingsFromXml(data, loadedMissionName);
            let newEntityList = this.loadEntitiesFromXml(data);
            this.setState({
                missionName:loadedMissionName,
                entityList: newEntityList,
                missionSettings: newMissionSettings
            });
        });
    }

    componentDidMount(){
        if(this.props.xmlFilePath !== undefined){
            this.initializeMissionFromFile();
        }
    }

    componentWillUnmount(){
        this.mounted = false;
    }

    render(){
        return (
            <MissionScreen
                entityList={this.state.entityList}
                missionName={this.state.missionName}
                missionSettings={this.state.missionSettings}
                alertHandler={this.props.alertHandler}
                screenHandler={this.props.screenHandler}
                showScrimmage={this.props.showScrimmage.bind(this)}
                addNewEntity={this.addNewEntity.bind(this)}
                addNewPlugin={this.addNewPlugin.bind(this)}
                selectedCard={this.state.selectedCard}
                pluginList={this.state.pluginList}
                xmlFilePath={this.props.xmlFilePath}
                entityProperties={this.entityProperties}                
                openCodeEditor={this.props.openCodeEditor}
                resetPlugins={this.resetPlugins.bind(this)}
                updateAllMissionProperties={this.updateAllMissionProperties.bind(this)}
                showJupyter={this.props.showJupyter}
                initializeMission={this.initializeMissionFromFile.bind(this)}
            />
        );
    }
}

Mission.propTypes = {
    alertHandler: propTypes.func,
    screenHandler: propTypes.func,
    xmlFilePath: propTypes.string,
    showScrimmage: propTypes.func,
    openCodeEditor: propTypes.func,
    showJupyter: propTypes.func

};

