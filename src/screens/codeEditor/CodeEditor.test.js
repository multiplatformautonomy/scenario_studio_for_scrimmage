import React from 'react';
import { shallow, mount }from 'enzyme';
import RealCodeEditor from './CodeEditor';

class CodeEditor extends RealCodeEditor {
    componentDidMount() {
        return; 
    }
}


jest.mock('./FileBrowser');
const fs = require('fs');
jest.mock('fs');
fs.readFileSync = jest.fn(()=>'');
jest.mock('os');



test('Saves any edits to the file you are workinjg on and writes it back to the file location', () => {
    const codeEditor = shallow(<CodeEditor files={['test-path']} screenHandler={()=>{} } toggleEditor={()=>{}} />);

    fs.readFileSync.mockReturnValue('test');
    codeEditor.instance().domRefs['test-path'] = {editor: {getValue: jest.fn(()=>{})}};
    
    expect(codeEditor.instance().saveEdits('test-path')).toBe(undefined);
});

test('Displays a message box asking if you are sure you want to delete a file', () => {
    const codeEditor = mount(<CodeEditor files={['test-path.css']} screenHandler={()=>{} } toggleEditor={()=>{}} />);

    var mockShow = jest.fn().mockResolvedValue({response: 0});
    const mockRequire = jest.fn(()=>({unlinkSync: jest.fn(()=>{})}));
    const mockGetElectron = jest.fn(() => {
        let obj = {
            dialog: {showMessageBox: mockShow},
            require: mockRequire
        };
        return obj;
    });

    codeEditor.instance().getElectron = mockGetElectron;

    codeEditor.instance().deleteFile('test-path');

    mockShow = jest.fn().mockResolvedValue({response: 1});

    expect(codeEditor.instance().deleteFile('test-path')).toBe(undefined);
});

test('to see if buttons on the page can handle clicks', ()=> {
    const codeEditor = mount(<CodeEditor files={['test-path']} screenHandler={()=>{} } toggleEditor={()=>{}} />);

    codeEditor.instance().deleteFile = jest.fn(()=>{});
    codeEditor.instance().fileRead = jest.fn(()=>{});
    codeEditor.instance().saveEdits = jest.fn(()=>{});

    const response = {response: 1};
    const mockElectron = jest.fn(()=>({dialog: {showMessageBox: jest.fn().mockResolvedValue(response)}}));

    codeEditor.instance().getElectron = mockElectron;

    expect(codeEditor.find('SSSButton')).toHaveLength(5);
    codeEditor.find('SSSButton').at(0).prop('clickHandler')();
    codeEditor.find('SSSButton').at(1).prop('clickHandler')();
    codeEditor.find('SSSButton').at(2).prop('clickHandler')();
    codeEditor.find('SSSButton').at(3).prop('clickHandler')();
    codeEditor.find('SSSButton').at(4).prop('clickHandler')();
});

test('Tests if file type is returning the proper styling guid', ()=> {
    const codeEditor = mount(<CodeEditor files={['test-path.css']} screenHandler={()=>{} } toggleEditor={()=>{}}  />);

    expect(codeEditor.instance().fileType('test.css')).toBe('css');
    expect(codeEditor.instance().fileType('test.js')).toBe('javascript');
    expect(codeEditor.instance().fileType('test.cpp')).toBe('c_cpp');
    expect(codeEditor.instance().fileType('test.py')).toBe('python');
    expect(codeEditor.instance().fileType('test.yaml')).toBe('yaml');
    expect(codeEditor.instance().fileType('test.yml')).toBe('yaml');
    expect(codeEditor.instance().fileType('test.json')).toBe('javascript');
    expect(codeEditor.instance().fileType('test.pb')).toBe('protobuf');
    expect(codeEditor.instance().fileType('test.proto')).toBe('protobuf');
    expect(codeEditor.instance().fileType('test.h')).toBe('c_cpp');
    expect(codeEditor.instance().fileType('test.xml')).toBe('xml');
});

test('going back to title screen', () => {
    const codeEditor = mount(<CodeEditor files={['test-path']} screenHandler={()=>{} }  toggleEditor={()=>{}}/>);

    var mockShow = jest.fn().mockResolvedValue({response: 0});
    
    const mockRequire = jest.fn(()=>{});
    const mockGetElectron = jest.fn(() => {
        let obj = {
            dialog: {showMessageBox: mockShow},
            require: mockRequire
        };
        return obj;
    });

    codeEditor.instance().saveEdits = jest.fn(()=>{});

    fs.readFileSync.mockReturnValue('test');
    codeEditor.instance().fileRead = jest.fn(()=>{});
    codeEditor.instance().getElectron = mockGetElectron;

    codeEditor.instance().saveAndContinue(codeEditor.instance().props.onBack);

    mockShow = jest.fn().mockResolvedValue({response: 1});
    codeEditor.instance().saveAndContinue(codeEditor.instance().props.onBack);

    mockShow = jest.fn().mockResolvedValue({response: 2});
    expect(codeEditor.instance().saveAndContinue(codeEditor.instance().props.onBack)).toBe(undefined);
});

test('saveAndContinue response = 0', () =>{
    const codeEditor = mount(<CodeEditor files={['test-path']} screenHandler={()=>{}}/>);
    const mockFileRead = jest.fn(()=>false);
    const mockGetValue = jest.fn(()=>true);
    const mockContinue = jest.fn();
    const mockSaveEdits = jest.fn();
    let mockShow = jest.fn(()=>({
        then: (first)=>{
            first({response: 0});
        }
    }));
    let mockGetElectron = jest.fn(() => {
        let obj = {require:require, dialog: {showMessageBox: mockShow}};
        return obj;
    });

    codeEditor.instance().domRefs['test-path'].editor.getValue = mockGetValue;
    codeEditor.instance().fileRead = mockFileRead;
    codeEditor.saveEdits = mockSaveEdits;
    codeEditor.instance().saveEdits = mockSaveEdits;
    codeEditor.instance().getElectron = mockGetElectron;
    codeEditor.instance().saveAndContinue(mockContinue);
    expect(mockContinue).toBeCalled();

    mockShow = jest.fn(()=>({
        then: (first)=>{
            first({response: 1});
        }
    }));
    mockGetElectron = jest.fn(() => {
        let obj = {dialog: {showMessageBox: mockShow}};
        return obj;
    });
    codeEditor.instance().getElectron = mockGetElectron;
    codeEditor.instance().saveAndContinue(mockContinue);
    expect(mockContinue).toBeCalledTimes(2);

    mockShow = jest.fn(()=>({
        then: (first)=>{
            first({response: 2});
        }
    }));
    mockGetElectron = jest.fn(() => {
        let obj = {dialog: {showMessageBox: mockShow}};
        return obj;
    });
    codeEditor.instance().getElectron = mockGetElectron;
    codeEditor.instance().saveAndContinue(mockContinue);
    expect(mockContinue).toBeCalledTimes(2);
});

test('run button renders when runnable is true', ()=>{
    const mockOnRun = jest.fn();
    const codeEditor = mount(<CodeEditor runnable={true} onRun={mockOnRun} files={['test-path']} screenHandler={()=>{}}/>);

    let mockShow = jest.fn(()=>({
        then: (first)=>{
            first({response: 1});
        }
    }));
    let mockGetElectron = jest.fn(() => {
        let obj = {require: require, dialog: {showMessageBox: mockShow}};
        return obj;
    });

    codeEditor.instance().getElectron = mockGetElectron;
    const runButton = codeEditor.instance().renderRunButton();
    runButton.props.clickHandler();
    expect(mockOnRun).toBeCalled();
});

test('resize on update', ()=>{
    const mockResize = jest.fn();
    const codeEditor = mount(<CodeEditor files={['test-path']} screenHandler={()=>{}}/>);
    codeEditor.instance().domRefs['test-path'].editor.resize = mockResize;
    codeEditor.instance().componentDidUpdate();
    expect(mockResize).toBeCalled();
});


// test('openFile', ()=>{
//     const codeEditor = mount(<CodeEditor files={['FOO.BAR']} screenHandler={()=>{}}/>);
//     codeEditor.instance().openFile('BAR.FOO');
// });

// test('aceFile', ()=>{
//     const codeEditor = mount(<CodeEditor files={['FOO.BAR']} screenHandler={()=>{}}/>);
// });

// test('_closeFile', ()=>{
//     const codeEditor = mount(<CodeEditor files={['FOO.BAR']} screenHandler={()=>{}}/>);
// });

// test('_promptSaveAndCloseFile', ()=>{
//     const codeEditor = mount(<CodeEditor files={['FOO.BAR']} screenHandler={()=>{}}/>);
// });

// test('handleClose', ()=>{
//     const codeEditor = mount(<CodeEditor files={['FOO.BAR']} screenHandler={()=>{}}/>);
// });

// test('tabClick', ()=>{
//     const codeEditor = mount(<CodeEditor files={['FOO.BAR']} screenHandler={()=>{}}/>);
// });

// test('fileTabs', ()=>{
//     const codeEditor = mount(<CodeEditor files={['FOO.BAR']} screenHandler={()=>{}}/>);
// });
