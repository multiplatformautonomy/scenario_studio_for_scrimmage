import React from 'react';
import Docker from '../../utilities/Docker';
import CodeEditor from './CodeEditor';
import propTypes from 'prop-types';


//FontAwesome Dependencies and Setup
import {dom, library}from '@fortawesome/fontawesome-svg-core';
import {faCaretUp, faCaretDown}from '@fortawesome/free-solid-svg-icons';
library.add(faCaretUp, faCaretDown);
dom.watch();
export default class PytorchEditor extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            pythonOutput:[],
            showOutput:false
        };
        this.executeTerminalCommand = this.executeTerminalCommand.bind(this);
        this.pythonOutputBox = this.pythonOutputBox.bind(this);
        this.compileAndRun = this.compileAndRun.bind(this);
    }

    executeTerminalCommand(command) {
        return Docker.exec('sss_scrimmage', '/bin/bash').then((subProc)=>{
            subProc.stdout.setEncoding('utf8');
            subProc.stdout.on('data', (data) => {
                this.dataToState(data);
                if(this.outputEnd){
                    this.outputEnd.scrollIntoView();
                }
            });
            subProc.stderr.setEncoding('utf8');
            subProc.stderr.on('data', (data)=>{
                this.dataToState(data, 'error');
                if(this.outputEnd){
                    this.outputEnd.scrollIntoView();
                }
            });          
            subProc.stdin.setEncoding('utf8');
            subProc.stdin.write(command + '\n');
            
            return new Promise((resolve) =>{
                subProc.on('close', resolve);
            });
        });
    }

    dataToState(data, type) {
        data = data.trim();
        let outputs = data.split('\n');
        let objs = outputs.map((entry)=>({output: entry, style: type}));
        let update = this.state.pythonOutput.concat(objs);
        this.setState({pythonOutput: update});
    }

    async compileAndRun(){
        const path = require('path');
        Docker.stopAndRemove('pytorch_scrimmage');
        await Docker.run(
            'multiplatformautonomy/vscrimmage-headless:stable', 
            {
                name:'pytorch_scrimmage', 
                interactive:true,
                source:path.dirname(this.props.pythonFilePath), 
                dst:'/missionMount'
            },
            '/bin/bash'
        ).then(async()=> {
            this.setState({pythonOutput: []});
            this.setState({showOutput:true});

            const runCommand = 'python3 /missionMount/' + this.props.pythonFileName  + '; exit';
            let compilePluginCommand = 'cd /missionMount/ProjectPlugins ; ';
            compilePluginCommand += 'mkdir -p build ; cd build ; ';
            compilePluginCommand += 'source /home/scrimmage/.scrimmage/setup.bash; ';
            compilePluginCommand += 'cmake .. ; make -j 8; exit';

            await this.executeTerminalCommand(compilePluginCommand);
            await this.executeTerminalCommand(runCommand);
        });
    }

    pythonOutputBox() {
        let objs = [];
        for(const [index, entry]of this.state.pythonOutput.entries()) {
            objs.push(<div className={'python-output-line ' + entry.style}
                key={index}> {entry.output}</div>);
        }
        if(this.state.showOutput) {  
            return <div className='python-output-content'>
                {objs}
                <div ref={(node)=>{
                    this.outputEnd = node;
                }}></div>
            </div>;    
        }
    }

    render() {
        return <div className="code-editor-container">
            <div style={{flex:'1', overflow:'hidden'}}>
                <CodeEditor ref={(node) => {
                    this.codeEditor = node;
                }}
                onBack={this.props.onBack}
                files={[this.props.pythonFilePath]}
                runnable={true}
                onRun={this.compileAndRun}>
                </CodeEditor>
            </div>
            <div style={{flexShrink:'0', zIndex:'1'}}>
                <div className="python-output-title"
                    onClick={()=>{
                        this.setState({showOutput: !this.state.showOutput});
                    }}
                >
                    Python Output console
                    <div className={this.state.showOutput ? 'python-output-none' : 'python-output-visible'}>
                        <i className={'fas fa-1x fa-caret-up'} />
                    </div>
                    <div className={this.state.showOutput ? 'python-output-visible' : 'python-output-none'}>
                        <i className={'fas fa-1x fa-caret-down'} />
                    </div>
                </div>
                {this.pythonOutputBox()}
            </div>
        </div>;
    }
}

PytorchEditor.propTypes = {
    pythonFilePath: propTypes.string,
    pythonFileName:propTypes.string,
    onBack:propTypes.func
};
