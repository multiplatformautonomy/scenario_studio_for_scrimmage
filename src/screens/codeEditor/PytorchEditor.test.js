import React from 'react';
import { mount }from 'enzyme';
import PytorchEditor from './PytorchEditor';
jest.mock('./CodeEditor.js');
jest.mock('../../utilities/Docker');
jest.mock('fs');

test('render PytorchEditor renders output box', () => {
    const pytorchEditor = mount(<PytorchEditor 
        pythonFilePath={'test-path.js'}
        onBack={()=>{}}/>);
    const mockPythonOutputBox = jest.fn();
    pytorchEditor.instance().pythonOutputBox = mockPythonOutputBox;
    pytorchEditor.instance().render();
    expect(mockPythonOutputBox).toBeCalled();
});

test('pythonOutput box renders one div per pythonOutput element', ()=> {
    const testOutput = ['output1', 'output2'];
    const pytorchEditor = mount(<PytorchEditor 
        pythonFilePath={'test-path.js'}
        onBack={()=>{}}
    />);
    pytorchEditor.setState({pythonOutput: testOutput});
    pytorchEditor.setState({showOutput: true});
    pytorchEditor.instance().render();
    const pythonOutputContent = pytorchEditor.find('.python-output-content');
    expect(pythonOutputContent.length).toBe(1);
    const childDivs = pythonOutputContent.find('div');
    //add 1 to account for div in the parent
    expect(childDivs.length).toBe(testOutput.length + 2);
});

test('clicking header on python output box toggles visibility', ()=> {
    const pytorchEditor = mount(<PytorchEditor 
        pythonFilePath={'test-path.js'}
        onBack={()=>{}}
    />);
    pytorchEditor.setState({showOutput: true});
    pytorchEditor.instance().render();
    const pythonOutputTitle = pytorchEditor.find('.python-output-title');
    expect(pythonOutputTitle.length).toBe(1);
    pythonOutputTitle.simulate('click');
    expect(pytorchEditor.instance().state.showOutput).toBe(false);
    pythonOutputTitle.simulate('click');
    expect(pytorchEditor.instance().state.showOutput).toBe(true);
});

test('compileAndRun populates changes terminal show state', async()=> {
    const pytorchEditor = mount(<PytorchEditor 
        pythonFilePath={'test-path.js'}
        pythonFileName={'test-path'}
        onBack={()=>{}}
    />);
    const mockExecuteTerminalCommand = jest.fn(()=>{
        new Promise((resolve)=>{
            resolve();
        });
    });
    pytorchEditor.instance().executeTerminalCommand = mockExecuteTerminalCommand;
    await pytorchEditor.instance().compileAndRun();
    expect(pytorchEditor.instance().state.showOutput).toBe(true);
    expect(mockExecuteTerminalCommand).toBeCalledTimes(2);
});

test('dataToState updates state correctly', () => {
    const testString = 'this is line1\nthis is line2';
    const pytorchEditor = mount(<PytorchEditor 
        pythonFilePath={'test-path.js'}
    />);
    pytorchEditor.instance().dataToState(testString);
    expect(pytorchEditor.instance().state.pythonOutput.length).toBe(2);
    expect(pytorchEditor.instance().state.pythonOutput[0].output).toBe('this is line1');
    expect(pytorchEditor.instance().state.pythonOutput[1].output).toBe('this is line2');
});

test('executeTerminalCommand sets up subproc listeners', async ()=>{
    const pytorchEditor = mount(<PytorchEditor 
        pythonFilePath={'test-path.js'}
        pythonFileName={'test-path'}
        onBack={()=>{}}
    />);
    const mockDataToState = jest.fn();
    pytorchEditor.instance().dataToState = mockDataToState;
    await pytorchEditor.instance().executeTerminalCommand('command');
    expect(mockDataToState).toBeCalledTimes(3);
});
