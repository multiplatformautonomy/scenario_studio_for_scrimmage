import React from 'react';
import propTypes from 'prop-types';
import './CodeEditor.css';
import Button from '../../components/Button';
import Sidebar from '../../components/Sidebar';
import AceEditor from 'react-ace';
import FileBrowser from './FileBrowser';
import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/mode-css';
import 'ace-builds/src-noconflict/mode-c_cpp';
import 'ace-builds/src-noconflict/mode-xml.js';
import 'ace-builds/src-noconflict/theme-monokai';
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/mode-python';
import 'ace-builds/src-noconflict/mode-protobuf';
import 'ace-builds/src-noconflict/mode-yaml';

//FontAwesome Dependencies and Setup
import { dom, library }from '@fortawesome/fontawesome-svg-core';
import { faArrowLeft, faTrash, faSave, faUndo, faRedo, faPlay }from '@fortawesome/free-solid-svg-icons';
library.add(faArrowLeft, faTrash, faSave, faUndo, faRedo, faPlay);
dom.watch();
//----------------------------------

export default class CodeEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = { currentFile: '', files: this.props.files };
        this.state.currentFile = this.state.files[0];

        let path = this.getElectron().require('path');
        if(this.props.xmlPath){
            this.state.currentDirectory = path.normalize(this.props.xmlPath);
        }else {
            let file = this.state.currentFile;
            this.state.currentDirectory = path.dirname(path.normalize(file));
        }

        this.state.editorValues = [];
        this.options = {
            buttons: ['Save', 'Don\'t Save', 'Cancel'],
            message: 'Do you want to save your changes?'
        };
        this.domRefs = {};
    }


    getElectron() {
        return window.require('electron').remote;
    }
    // Write to file path and save any changes
    saveEdits(filePath) {
        const fs = this.getElectron().require('fs');

        fs.writeFileSync(filePath, this.domRefs[this.state.currentFile].editor.getValue());
        return;
    }

    // Delete the file at selected file path
    deleteFile(filePath) {
        let options = {
            buttons: ['Yes', 'No'],
            message: 'Are you sure you want to delete this file?',
            title: 'Delete file'
        };
        const electron = this.getElectron();
        electron.dialog.showMessageBox(options)
            .then((response) => {
                if(response.response === 0) {
                    const remote = this.getElectron();
                    const fs = remote.require('fs');
                    if(this.state.files.length === 1) {
                        fs.unlinkSync(filePath, this.domRefs[this.state.currentFile].editor.getValue());
                        this.props.toggleEditor(false);
                    }else {
                        this.setState({
                            files: this.state.files.filter(function (files) {
                                return files !== filePath;
                            })
                        });

                        this.setState({ currentFile: this.state.files[0] });

                        fs.unlinkSync(filePath, this.domRefs[this.state.currentFile].editor.getValue());
                    }

                    fs.unlinkSync(filePath, this.domRefs[this.state.currentFile].editor.getValue());
                    this.props.onBack();
                }else if(response.response === 1) {
                    return;
                }
            });
    }

    // Select styling for selected file path
    fileType(filePath) {
        const path = this.getElectron().require('path');
        switch (path.extname(filePath)){
        case '.css': return 'css';
        case '.json':
        case '.js': return 'javascript';
        case '.cpp':
        case '.h':
        case '.c':
        case '.cc': return 'c_cpp';
        case '.xml': return 'xml';
        case '.py': return 'python';
        case '.proto':
        case '.pb': return 'protobuf';
        case '.yaml':
        case '.yml': return 'yaml';
        default: return 'text';
        }
    }

    // Read in the contents at specified file path
    fileRead(filePath) {
        const remote = this.getElectron();
        const fs = remote.require('fs');

        return fs.readFileSync(filePath).toString('utf-8');
    }

    saveAndContinue(continueFunction) {
        if(this.fileRead(this.state.currentFile) !== this.domRefs[this.state.currentFile].editor.getValue()) {
            const electron = this.getElectron();
            
            electron.dialog.showMessageBox(this.options)
                .then((response) => {
                    if(response.response === 0) {
                        this.saveEdits(this.state.currentFile);
                        continueFunction();
                    }else if(response.response === 1) {
                        continueFunction();
                    }else if(response.response === 2) {
                        return;
                    }
                });
        }else {
            continueFunction();
        }
    }

    renderRunButton() {
        if(this.props.runnable) {
            return <Button clickHandler={() => this.saveAndContinue(this.props.onRun)}
                tag='run'
                message='Run this file'
                faIcon='fas fa-play fa-2x' />;
        }
    }

    openFile(file) {
        if(this.state.editorValues.length < this.state.files.length) {
            let temp = this.state.editorValues;
            temp.push(this.fileRead(file));
            this.setState({editorValues: temp});
        }
        this.setState({currentFile: file});
    }

    aceFile(file){
        if(this.state.editorValues.length === 0) {
            return this.fileRead(file);
        }
        for(let i = 0; i < this.state.files.length; i++) {
            if(this.state.files[i] === file) {
                return this.state.editorValues[i];
            }
        }
    }

    _closeFile() {
        let temp = [];
        for(let i = 0; i < this.state.files.length; i++) {
            if(!(this.state.files[i] === this.state.currentFile)) {
                temp.push(this.state.files[i]);
            }
        }

        this.browser.openFiles = temp;
        let editorValueTemp = this.state.editorValues;            
        editorValueTemp.splice(this.state.files.indexOf(this.state.currentFile), 1);
        this.setState({
            files: temp,
            currentFile: temp[0],
            editorValues: editorValueTemp
        });
    }

    _promptSaveAndCloseFile() {
        if(this.domRefs[this.state.currentFile].editor.getValue() !== this.fileRead(this.state.currentFile)) {
            const electron = this.getElectron();
            electron.dialog.showMessageBox(this.options)
                .then((response) => {
                    if(response.response === 0 || response.response === 1) {
                        if(response.response === 0) {
                            this.saveEdits(this.state.currentFile);
                        }

                        this._closeFile();
                    }else if(response.response === 2) {
                        return;
                    }
                });
        }else {
            this._closeFile();
        }
    }

    handleClose(event, element) {
        this._promptSaveAndCloseFile();
        if(event.target !== element) {
            event.stopPropagation();
            return;
        }
    }

    tabClick(file) {
        let temp = this.state.editorValues;
        temp[this.state.files.indexOf(this.state.currentFile)] = this.domRefs[this.state.currentFile].editor.getValue();
        this.setState({editorValues: temp});
        this.openFile(file);
    }

    fileTabs() {
        return (
            this.state.files.map((file) => <div key={file}
                onClick={() => this.tabClick(file)}
                className={'fileTab' + (this.state.currentFile === file ? ' current' : '')}>
                {this.state.currentFile === file ?
                    <button className='tabClose' onClick={(event, element) => this.handleClose(event, element)}>
                        x
                    </button>
                    : null}
                {file.split('\\').pop().split('/').pop()}
            </div>)
        );
    }

    componentDidMount(){
        if(!this.props.runnable && this.browser.openFiles.length < this.state.files.length) {
            this.browser.openFiles = this.state.files;
            let temp = [];
            for(let i = 0; i < this.state.files.length; i++) {
                this.setState({currentFile: this.state.files[i]});
                temp.push(this.fileRead(this.state.files[i]));
            }
            this.setState({editorValues: temp});
        }
    }

    componentDidUpdate() {
        this.domRefs[this.state.currentFile].editor.resize();
    }
    renderEditors(){
        // render each editor using the files array. This will ensure that all undo and redo data is kept. 
        return (
            this.state.files.map((file) => (
                <AceEditor
                    key={file}
                    style={this.state.currentFile !== file ? {display: 'none' } : null}
                    mode={this.fileType(file)}
                    className='editor'
                    theme='monokai'
                    ref={(ref) => {
                        this.domRefs[file] = ref;
                    }}
                    onLoad={this.onLoad}
                    height='100%'
                    width='100%'
                    value={this.aceFile(file)}
                    editorProps={{ $blockScrolling: true }}
                    setOptions={{
                        enableBasicAutocompletion: true,
                        enableLiveAutocompletion: true,
                        enableSnippets: true
                    }}
                />
            )));
    }
    render() {
        return (
            <div className='container' style={{ height: '100%' }}>
                <Sidebar className='sidebar-container'>
                    <div>
                        <Button clickHandler={() => this.saveAndContinue(this.props.onBack)}
                            tag='back'
                            message={`Go back`}
                            faIcon='fas fa-arrow-left fa-2x' />
                        <Button clickHandler={() => {
                            this.domRefs[this.state.currentFile].editor.undo();
                        }}
                        tag='undo'
                        message='Undo'
                        faIcon='fas fa-undo fa-2x' />
                        <Button clickHandler={() => {
                            this.domRefs[this.state.currentFile].editor.redo();
                        }}
                        tag='redo'
                        message='Redo'
                        faIcon='fas fa-redo fa-2x' />
                        <Button clickHandler={() => this.saveEdits(this.state.currentFile)}
                            tag='save'
                            message='Save your edits'
                            faIcon='fas fa-save fa-2x' />
                        <Button clickHandler={() => this.deleteFile(this.state.currentFile)}
                            tag='delete'
                            message='Delete this file'
                            faIcon='fas fa-trash fa-2x' />
                        {this.renderRunButton()}
                    </div>
                </Sidebar>

                {/** Do not render this in the pytorch editor */}
                {!this.props.runnable && <FileBrowser ref={(node) => {
                    this.browser = node;
                }}
                filepath={this.state.currentDirectory} 
                openFile={(file) => this.openFile(file)} 
                openTab={(file)=>{
                    this.tabClick(file);
                }}/>
                }

                <div className='tabContainer'>

                    <div className='fileTabContainer'>
                        {this.state.files.length > 1 ? this.fileTabs() : null}
                    </div>

                    <div className='fileLabel'>{this.state.currentFile.split('\\').pop().split('/').pop()}</div>
                    {this.renderEditors()}

                </div>
            </div>
        );
    }
}

CodeEditor.defaultProps = {
    runnable: false,
    onBack: () => { }
};
CodeEditor.propTypes = {
    screenHandler: propTypes.func,
    files: propTypes.array,
    toggleEditor: propTypes.func,
    onBack: propTypes.func,
    filePath: propTypes.string,
    runnable: propTypes.bool,
    onRun: propTypes.func,
    xmlPath: propTypes.string
};
