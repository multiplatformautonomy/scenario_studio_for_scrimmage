import React from 'react';
import './FileBrowser.css';
import propTypes from 'prop-types';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import IndeterminateCheckBoxIcon from '@material-ui/icons/IndeterminateCheckBox';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import AddBoxIcon from '@material-ui/icons/AddBox';
import { fade, withStyles }from '@material-ui/core/styles';
import { Collapse }from '@material-ui/core';
import { useSpring, animated }from 'react-spring/web.cjs';

function TransitionComponent(props) {
    const style = useSpring({
        from: { opacity: 0, transform: 'translate3d(20px,0,0)' },
        to: { opacity: props.in ? 1 : 0, transform: `translate3d(${props.in ? 0 : 20}px,0,0)` }
    });

    return (
        <animated.div style={style}>
            <Collapse {...props} />
        </animated.div>
    );
}

TransitionComponent.propTypes = {
    /**
     * Show the component; triggers the enter or exit states
     */
    in: propTypes.bool
};

const StyledTreeItem = withStyles((theme) => ({
    iconContainer: {'& .close': {opacity: 0.3}},
    group: {
        marginLeft: 7,
        paddingLeft: 18,
        borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`
    }
}))((props) => <TreeItem {...props} TransitionComponent={TransitionComponent} />);

class FileBrowser extends React.Component {
    constructor(props) {
        super(props);
        this.filepaths = [];
        this.openFiles = [];
        this.nodeId = 0;
    }

    getElectron() {
        return window.require('electron').remote;
    }

    incrementNodeId() {
        this.nodeId = this.nodeId + 1;
    }

    handleClick() {
        var elements = document.getElementsByClassName('Mui-selected');
        for(let i = 0; i < this.filepaths.length; i++){
            let fileName = '';
            for(let j = this.filepaths[i].length - 1; j > 0; j--) {
                if(this.filepaths[i][j] === '\\') {
                    fileName = this.filepaths[i].substring(j + 1, this.filepaths[i].length);
                    break;
                }
            }
            if(fileName === elements['0'].innerText){
                if(!this.openFiles.includes(this.filepaths[i])){
                    this.openFiles.push(this.filepaths[i]);
                    this.props.openFile(this.filepaths[i]);
                }else {
                    this.props.openTab(this.filepaths[i]);
                }
                break;
            }
        }
    }

    treeChildren(subDirectories, filepath) {
        this.incrementNodeId();
        let tree = [];
        for(let i = 0; i < subDirectories.length; i++) {
            let childPath = filepath + '\\' + subDirectories[i];
            tree.push(this.renderTreeItems(childPath));
            this.incrementNodeId();
        }
        return tree;
    }

    renderTreeItems(filepath) {
        const remote = this.getElectron();
        const fs = remote.require('fs');

        let fileName = '';
        for(let i = filepath.length - 1; i > 0; i--) {
            if(filepath[i] === '\\') {
                fileName = filepath.substring(i + 1, filepath.length);
                break;
            }
        }

        var stats = fs.statSync(filepath);
        let subDir;

        if(stats.isDirectory()) {
            subDir = fs.readdirSync(filepath);
        }else {
            this.filepaths.push(filepath);
            return (
                <StyledTreeItem onLabelClick={() => {
                    setTimeout(()=>{
                        this.handleClick();
                    }, 10);
                }} key={filepath} nodeId={'' + this.nodeId} label={fileName} />
            );
        }
        return (
            <StyledTreeItem key={filepath} nodeId={'' + this.nodeId} label={fileName}>
                {this.treeChildren(subDir, filepath)}
            </StyledTreeItem>
        );
    }

    renderTreeView() {
        this.nodeId = 0;

        return (
            <TreeView
                defaultExpanded={['1']}
                defaultCollapseIcon={<IndeterminateCheckBoxIcon />} /* IndeterminateCheckBoxIcon */
                defaultExpandIcon={<AddBoxIcon />} /* AddBoxIcon */
                defaultEndIcon={<InsertDriveFileIcon/>}
            >
                {this.renderTreeItems(this.props.filepath)}
            </TreeView>
        );
    }

    render() {
        return (
            <div className='browser-component'>
                {this.renderTreeView()}
            </div>
        );
    }
}

FileBrowser.propTypes = {
    filepath: propTypes.string,
    openFile: propTypes.func,
    openTab: propTypes.func
};

export default FileBrowser;
