import React from 'react';
import {shallow, mount}from 'enzyme';
import RealApp from './App';

jest.mock('./components/Editor3D.js');
jest.mock('./screens/TitleScreen');

let mockGetOn = jest.fn();
class App extends RealApp {
    getElectron() {
        return {
            BrowserWindow: jest.fn(()=>({
                show: jest.fn(),
                loadURL: jest.fn()
            })),
            require: jest.fn(()=>({
                get: jest.fn((first, second)=>{
                    second({
                        setEncoding: jest.fn(),
                        on:mockGetOn
                    });
                })
            })),
            app: { getVersion: jest.fn(()=>'BAR'), getAppPath: jest.fn()}
        };
    }
}

test('jest functionality works', () => {
    expect(1).toBe(1);
});

test('checkForUpdate finds app out of date', ()=>{
    mockGetOn = (___, func)=>{
        func('{"version":"FOO"}');
    };
    const app = mount(<App/>);
    app.instance().checkForUpdate();
    app.update();
    expect(app.state('dialogTitle')).toBe('A new version of SSS is availble for download');
    mockGetOn = jest.fn();
});

test('changeScreen changes state', () => {
    const app = shallow(<App/>);
    expect(app.state('screen')).toBe('titleScreen');
    app.instance().changeScreen('scenarioScreen');
    expect(app.state('screen')).toBe('scenarioScreen');
});

test('set screen overwrite does not update', () => {
    const Screen = shallow(<App/>);
    expect(Screen.state('screen')).toBe('titleScreen');
    let inst = Screen.instance();
    let setState = jest.spyOn(inst, 'setState');
    inst.changeScreen('titleScreen');
    expect(setState).not.toBeCalled();
});

test('clickhandlers should change state', () => {
    const app = shallow(<App/>);
    expect(app.state('screen')).toBe('titleScreen');
    const collection = app.findWhere((node) => node.name() === 'SSSButton');
    const mockDialogBox = {setState: jest.fn()};
    app.instance().DialogBox = mockDialogBox;
    collection.forEach((node, i) => {
        let cachedState = app.state('screen');
        expect(node.name()).toBe('SSSButton');
        node.prop('clickHandler')();
        if(i === 0 || i === collection.length - 1) {
            // last 2 buttons do not change screen (tutorial button)
            expect(app.state('screen')).toBe(cachedState);
        }else {
            expect(app.state('screen')).not.toBe('titleScreen');
        }
    });
});

test('start mission does not set state if undefined', () => {
    const app = shallow(<App/>);
    expect(app.state('screen')).toBe('titleScreen');
    app.instance().startMission(undefined);
    expect(app.state('screen')).toBe('titleScreen');
});

test('start mission does set state if not undefined', () => {
    const app = shallow(<App/>);
    expect(app.state('screen')).toBe('titleScreen');
    app.instance().startMission('mission.xml');
    expect(app.state('screen')).toBe('scenarioScreen');
});

test('app renders scenario screen only when xml file path is given', () => {
    const app = shallow(<App/>);
    const newapp2 = app.setState({
        screen: 'scenarioScreen',
        xmlFilePath: null
    });
    expect(newapp2.find('Mission')).toHaveLength(0);
    const newapp1 = app.setState({
        screen: 'scenarioScreen',
        xmlFilePath: 'hello.xml'
    });
    expect(newapp1.find('Mission')).toHaveLength(1);
});

test('app renders nothing if no known screen is set', () => {
    // Note: this is a weird test.  We may need to modify this in the future for some default functionality to occur.
    const app = shallow(<App/>);
    const noapp = app.setState({screen: 'asdfghjkl;'});
    expect(noapp.find('Mission')).toHaveLength(0);
    expect(noapp.find('TitleScreen')).toHaveLength(0);
});

test('get electron does not return undefined', () => {
    const app = shallow(<App/>);
    expect(app.instance().getElectron()).not.toBe(undefined);
});



test('clear mission sets state of xmlFilePath to null', () => {
    const app = shallow(<App/>);
    app.setState({xmlFilePath: 'hello/world.js'});

    app.instance().clearMission();
    expect(app.state('xmlFilePath')).toBe(null);
});

test('show scrimmage opens browser', () => {
    const app = shallow(<App/>);
    app.instance().showScrimmage();
});

test('show jupyter opens browser', () => {
    const app = shallow(<App/>);
    app.instance().showJupyter();
});

test('open file changes state', ()=> {
    const app = shallow(<App/>);
    app.instance().openCodeEditor('hello');
    expect(app.state('screen')).toBe('titleScreen');
    expect(app.state('filePath')).toBe('hello');
});

test('toggle editor gets called.', ()=> {
    const app = shallow(<App/>);
    let showEditor = app.state('showEditor');
    app.instance().toggleEditor();
    expect(app.state('showEditor')).toBe(!showEditor);
    app.instance().toggleEditor();
    expect(app.state('showEditor')).toBe(showEditor);
});
