import sys
import copy
import gym
import scrimmage.utils
import random
import time

sys.settrace

def get_action(obs):
    return [random.randint(0, 4)]

def return_action_func(action_space, obs_space, params):
    return get_action(obs_space)

# define actions that can be taken in the ctf environment
# TODO: consider finding a way to access this from scrimmage rather than hardcoding
def get_actions(state):
    return [0, 1, 2, 3]

def test_openai():
    # set up openAI gym environment
    try:
        env = gym.make('scrimmage-ctf-v0')
    except gym.error.Error:
        mission_file = scrimmage.utils.find_mission('ctf-pytorch.xml')

        gym.envs.register(
            id='scrimmage-ctf-v0',
            entry_point='scrimmage.bindings:ScrimmageOpenAIEnv',
            max_episode_steps=1e9,
            reward_threshold=1e9,
            kwargs={"enable_gui": False,
                    "mission_file": mission_file}
        )
        env = gym.make('scrimmage-ctf-v0')
    
    temp_obs = env.reset()
    for i in range(5):
        action = get_action(temp_obs)
        temp_obs, reward, done = env.step(action[0])[:3]
        print("count = ", i, ", reward =", reward, ", action=", action, ", obs =", temp_obs)
        obs.append(copy.deepcopy(temp_obs))
        total_reward += reward

        if done:
            break
    env.close() # prints results of the last training episode
    print("ran the template file")
    
if __name__ == '__main__':
    test_openai()
   

