export const mission = {
    data: {
        runscript: {
            $: {
                'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                name: 'New Mission'
            },
            'entity': [],
            'entity_interaction': [],
            'metrics': [],
            'network': []
        }
    }
};

export default mission;
