import SelectBox from '../components/properties/SelectBox';
import {VariableInputBox}from '../components/properties/TextBox';

export default class EntityProperties {
    constructor(){
        const uuidv4 = require('uuid/v4');
        this.properties =
            [
                {
                    key: uuidv4(),
                    name:'team_id',
                    type:VariableInputBox,
                    value: ['1'],
                    defaultValue: ['1']
                },
                {
                    key: uuidv4(),
                    name:'speed',
                    type:VariableInputBox,
                    value: ['30'],
                    defaultValue: ['30']
                },
                {
                    key: uuidv4(),
                    name:'count',
                    type:VariableInputBox,
                    value: ['1'],
                    defaultValue: ['1']
                },
                {
                    key: uuidv4(),
                    name:'generate_rate',
                    type:VariableInputBox,
                    value: ['1 /2'],
                    defaultValue: ['1 /2']
                },
                {
                    key: uuidv4(),
                    name:'health',
                    type:VariableInputBox,
                    value: ['200'],
                    defaultValue: ['200']
                },
                {
                    key: uuidv4(),
                    name:'x',
                    type:VariableInputBox,
                    value: ['0'],
                    defaultValue: ['0']
                },
                {
                    key: uuidv4(),
                    name:'y',
                    type:VariableInputBox,
                    value: ['0'],
                    defaultValue: ['0']
                },
                {
                    key: uuidv4(),
                    name:'z',
                    type:VariableInputBox,
                    value: ['0'],
                    defaultValue: ['0']
                },
                {
                    key: uuidv4(),
                    name:'heading',
                    type:VariableInputBox,
                    value: ['0'],
                    defaultValue: ['0']
                },
                {
                    key: uuidv4(),
                    name:'color',
                    type:SelectBox,
                    value:['blue'],
                    defaultValue:['blue'],
                    options:['green', 'yellow', 'blue', 'red' ]
                },
                {
                    key: uuidv4(),
                    name:'visual_model',
                    type:SelectBox,
                    value:['zephyr-blue'],
                    defaultValue:['zephyr-blue'],
                    options:['zephyr-blue', 'zephyr-red', 'sea-angler', 'volkswagen' ]
                }
            ];

        /*
            enabledPlugins is a list of objects represented as:
            
            {
                name: 'elementName',            entity_interaction, motion_model, autonomy, etc.
                value: pluginName,              This value should be non-mutable.
                args: [                         Creates <hr/>s
                    {
                        name: paramName1,
                        value: paramValue1
                    },
                    {
                        name: paramName2,
                        value: paramValue2,
                    },
                    ...
                ],
            }
        */
        this.enabledPlugins = [];
    }
}

    
