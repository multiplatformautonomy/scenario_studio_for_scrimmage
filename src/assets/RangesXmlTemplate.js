export const variables = {
    data: {
        ranges: {
            $: {
                'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                name: 'Mission Variables'
            }
        }
    }
};

export default variables;
