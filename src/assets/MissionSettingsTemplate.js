import SelectBox from '../components/properties/SelectBox';
import BaseProperty from '../components/properties/BaseProperty';
import TextBox, {VariableInputBox}from '../components/properties/TextBox';

export default class MissionSettingProperties {
    constructor(){
        const uuidv4 = require('uuid/v4');
        this.properties = 
            [
                
                {
                    key: uuidv4(),
                    name:'stream_port',
                    type:TextBox,
                    value:50051,
                    defaultValue:50051
                },
                {
                    key: uuidv4(),
                    name:'stream_ip',
                    type:TextBox,
                    value:'localhost',
                    defaultValue:'localhost'
                },
                {
                    key: uuidv4(),
                    name:'end_condition',
                    type:TextBox,
                    value:'time, all_dead',
                    defaultValue:'time, all_dead'
                },
                {
                    key: uuidv4(),
                    name:'grid_spacing',
                    type:VariableInputBox,
                    value:10,
                    defaultValue:10
                },
                {
                    key: uuidv4(),
                    name:'grid_size',
                    type:VariableInputBox,
                    value:1000,
                    defaultValue:1000
                },
                {
                    key: uuidv4(),
                    name:'terrain',
                    type:SelectBox,
                    value:'mcmillan',
                    options:['mcmillan' ],
                    defaultValue:'mcmillan'
                },
                {
                    key: uuidv4(),
                    name:'background_color',
                    type:TextBox,
                    value:'191 191 191',
                    defaultValue:'191 191 191'
                },
                {
                    key: uuidv4(),
                    name:'gui_update_period',
                    type:VariableInputBox,
                    value:10,
                    defaultValue:10
                },
                {
                    key: uuidv4(),
                    name:'plot_tracks',
                    type:SelectBox,
                    value:'false',
                    options:['true', 'false'],
                    defaultValue:'false'
                },
                {
                    key: uuidv4(),
                    name:'output_type',
                    type:SelectBox,
                    value:'all',
                    options:['all' ],
                    defaultValue:'all'
                },
                {
                    key: uuidv4(),
                    name:'show_plugins',
                    type:SelectBox,
                    value:'false',
                    options:['true', 'false' ],
                    defaultValue:'false'
                },
                {
                    key: uuidv4(),
                    name:'log_dir',
                    type:TextBox,
                    value:'~/.scrimmage/logs',
                    defaultValue:'~/.scrimmage/logs'
                },
                {
                    key: uuidv4(),
                    name:'create_latest_dir',
                    type:SelectBox,
                    value:'true',
                    options:['true', 'false' ],
                    defaultValue:'true'
                },
                {
                    key: uuidv4(),
                    name:'latitude_origin',
                    type:VariableInputBox,
                    value:35.721025,
                    defaultValue:35.721025
                },
                {
                    key: uuidv4(),
                    name:'longitude_origin',
                    type:VariableInputBox,
                    value:-120.767925,
                    defaultValue:-120.767925
                },
                {
                    key: uuidv4(),
                    name:'altitude_origin',
                    type:VariableInputBox,
                    value:300,
                    defaultValue:300
                },
                {
                    key: uuidv4(),
                    name:'show_origin',
                    type:SelectBox,
                    value:'true',
                    options:['true', 'false' ],
                    defaultValue:'true'
                },
                {
                    key: uuidv4(),
                    name:'origin_length',
                    type:VariableInputBox,
                    value:10,
                    defaultValue:10
                },
                {
                    key: uuidv4(),
                    name:'enable_screenshots',
                    type:SelectBox,
                    value:'false',
                    options:['true', 'false' ],
                    defaultValue:'false'
                },
                // Add seed here
                {
                    key: uuidv4(),
                    name:'run',
                    type:BaseProperty,
                    value:null,
                    args: [
                        {
                            key: uuidv4(),
                            name:'start',
                            type:VariableInputBox,
                            value:0.0,
                            defaultValue:0.0
                        },
                        {
                            key: uuidv4(),
                            name:'end',
                            type:VariableInputBox,
                            value:100,
                            defaultValue:100
                        },
                        {
                            key: uuidv4(),
                            name:'dt',
                            type:VariableInputBox,
                            value:0.1,
                            defaultValue:0.1
                        },
                        {
                            key: uuidv4(),
                            name:'time_warp',
                            type:VariableInputBox,
                            value:10,
                            defaultValue:10
                        },
                        {
                            key: uuidv4(),
                            name:'enable_gui',
                            type:SelectBox,
                            value:'true',
                            options:['true', 'false' ],
                            defaultValue:'true'
                        },
                        {
                            key: uuidv4(),
                            name:'network_gui',
                            type:SelectBox,
                            value:'false',
                            options:['true', 'false' ],
                            defaultValue:'false'
                        },
                        {
                            key: uuidv4(),
                            name:'start_paused',
                            type:SelectBox,
                            value:'true',
                            options:['true', 'false' ],
                            defaultValue:'true'
                        },
                        {
                            key: uuidv4(),
                            name:'full_screen',
                            type:SelectBox,
                            value:'true',
                            options:['true', 'false' ],
                            defaultValue:'true'
                        },
                        {
                            key: uuidv4(),
                            name:'window_width',
                            type:VariableInputBox,
                            value:'800',
                            defaultValue:'800'
                        },
                        {
                            key: uuidv4(),
                            name:'window_height',
                            type:VariableInputBox,
                            value:'600',
                            defaultValue:'600'
                        }
                    ]
                },
                {
                    key: uuidv4(),
                    name:'multi_threaded',
                    type:SelectBox,
                    value:'false',
                    defaultValue:'false',
                    options:['true', 'false' ],
                    args: [
                        {
                            key: uuidv4(),
                            name:'num_threads',
                            type:VariableInputBox,
                            value:8,
                            defaultValue:8
                        }
                    ]
                }
            ];

        /*
            enabledPlugins is a list of objects represented as:
            
            {
                name: 'pluginName',             This value should be immutable.
                pluginType: pluginType,         entity_interaction, motion_model, autonomy, etc.
                                                This is needed for xml but not visible in properties panel.
                args: [                         Creates <hr/>s
                    {
                        name: paramName1,
                        value: paramValue1
                    },
                    {
                        name: paramName2,
                        value: paramValue2,
                    },
                    ...
                ],
            }
        */
        this.enabledPlugins = [];
    }
}
