import UpdateMissionXML from './UpdateMissionXML';

test('update mission settings with properties', () => {
    let entityProperties = {
        'mission-settings': {
            properties: [
                {
                    name: 'hello',
                    args: [
                        {
                            name: 'foo',
                            value: 'bar'
                        }
                    ],
                    value: 'world'
                }
            ],
            enabledPlugins: [
                {
                    pluginType: 'world',
                    args: [
                        {
                            value: 'foo',
                            name: 'bar'
                        }
                    ]
                }
            ]
        }
    };
    let data = {data: {runscript: {}}};

    let expected = {
        data: {
            runscript: {
                hello: {
                    $: {foo: 'bar'},
                    '_': 'world'
                },
                world: [
                    {
                        $: {bar: 'foo'},
                        '_': undefined
                    }
                ]
            }
        }
    };

    expect(UpdateMissionXML._updateMissionSettingsXml(data, entityProperties)).toStrictEqual(expected);
});

test('update plugin', () => {
    let entityProperties = {
        hello: {
            enabledPlugins: [
                {
                    pluginType: 'world',
                    args: [
                        {
                            value: 'foo',
                            name: 'bar'
                        }
                    ]
                }
            ]
        }
    };

    let dataObj = {pluginName: {index: ''}};

    let entityTypeStr = 'hello';

    expect(UpdateMissionXML._updatePluginXml(entityTypeStr, dataObj, entityProperties)).toBe(undefined);
    dataObj = {world: {index: ''}};
    expect(UpdateMissionXML._updatePluginXml(entityTypeStr, dataObj, entityProperties)).toBe(undefined);
});
