export function validateFileIn(fileIn = ''){
    let success = false;
    let filePath = null;

    if(fileIn.length === 0){
        if(process.argv.length < 3) {
            statusOut(1, ' Usage -> npm run parseXml FILE');
        }else {
            fileIn = process.argv[2];
        }
    }
    if(fileIn.length > 4 && fileIn.slice(-4) === '.xml'){
        filePath = __dirname + '/../' + fileIn; //rel to dir of parseXml script
        if(fileIn[0] === '/') {
            filePath = fileIn;
        }
        // statusOut(2, ' filePath: ' + filePath);
        success = true;
    }else {
        statusOut(1, ' Invalid File');
    }
    return [success, filePath ];
}

function statusOut(success, msg){
    let status = {0: 'Success', 1: 'Error', 2: 'Info' };
    console.log(status[success] + ':' + msg);
}

function getElectron() {
    return window.require('electron').remote;
}

export function saveJSToXml(filePath, jsObject){
    return new Promise(function (resolve, reject) {
        let xml2js = require('xml2js');
        const remote = getElectron();
        const fs = remote.require('fs');

        var builder = new xml2js.Builder();
        var xml = builder.buildObject(jsObject);

        fs.writeFile(filePath, xml, (err)=>{
            if(err){
                statusOut(1, ' Unable to write js properties');
                reject(false);
            }else {
                statusOut(0, ' ' + filePath + ' saved!');
                resolve(true);
            }
        });
    });
}

export function readParseFile(filePath = ''){
    return new Promise(function (resolve, reject) {
        let path = require('path');
        let xml2js = require('xml2js');
        let parser = new xml2js.Parser();
        const remote = getElectron();
        const fs = remote.require('fs');
        
        let success = false;
        let valid = validateFileIn(filePath);
        if(valid[0] !== true) {
            reject(success);
        }

        // statusOut(2, ' readParseFile - filePath: ' + filePath);
        fs.readFile(filePath, function(err, data){
            if(err){
                statusOut(1, ' Invalid file/path');
                reject(success);
            }else {
                let fileName = path.basename(filePath);
                parser.parseString(data, function(err, result){
                    if(err){
                        statusOut(1, ' Unable to parse file');
                        reject(success);
                    }else {
                        resolve({
                            data: result,
                            fileName: fileName,
                            sucess: success
                        });
                    }
                });
            }
        });
    });
}
