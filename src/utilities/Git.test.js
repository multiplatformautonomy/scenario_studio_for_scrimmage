import Git from './Git';

import Docker from './Docker';
jest.mock('./Docker');

test('getElectron', ()=>{
    let error;
    try {
        Git.getElectron();
    }catch (err){
        error = err;
    }finally {
        expect(error).toBeFalsy();
    }
});

describe('getElectron dependent', ()=>{
    let originalGetElectron;
    beforeAll(()=>{
        originalGetElectron = Git.getElectron;
        Git.getElectron = jest.fn(()=>({
            app: { getPath: ()=>'userDataPath' },
            remote:{ app: {getPath: ()=>'userDataPath'}},
            require:()=>({
                exec:()=>({
                    stderr:{on:(first, second)=>second('FOOBAR')},
                    on:(first, second)=>second(0)
                }),
                join: (first, second)=>(first + second),
                parse:()=>{},
                writeFileSync: ()=>{},
                readFileSync: ()=>'{gitSettings:{username:\'\', email:\'\'}}',
                stderr:{on:(first, second)=>second('FOOBAR')},
                on:()=>0
            })
        }));
    });

    afterAll(()=>{
        Git.getElectron = originalGetElectron;
    });
    test('getGitSettingsFilePath', ()=>{
        expect(Git.getGitSettingsFilePath()).toBe('userDataPathsettings.json');
    });

    test('loadGitSettings', ()=>{
        let object = Git.loadGitSettings();
        expect('username' in object).toBeTruthy();
        expect('email' in object).toBeTruthy();
    });

    test('loadGitSettings catch error if invalid file', ()=>{

    });

    test('saveGitSettings throws error if invalid object', ()=>{
        try {
            Git.saveGitSettings({});
        }catch (err){
            expect(err.message).toBe('Invalid Object');
        }
    });

    test('saveGitSettings saves', ()=>{
        expect(
            Git.saveGitSettings({gitSettings:{username:'Foo', email:'Bar'}})
        ).toBe('Git settings saved successfully'); 
    });

    test('saveGitSettings catch error while saving', ()=>{
        Git.getElectron.mockImplementationOnce(()=>({
            app: { getPath: ()=>'userDataPath' },
            remote:{ app: {getPath: ()=>'userDataPath'}},
            require:()=>({
                join: (first, second)=>(first + second),
                parse:()=>{}
            })
        }));
        expect(
            Git.saveGitSettings({gitSettings:{username:'Foo', email:'Bar'}})
        ).toContain('Could not save because of the following error:\n');
    });

    test('gitClone', async(done)=>{
        let code = await Git.gitClone('Foo', 'Bar');
        expect(code).not.toBe(undefined);
        done();
    });
});

test('gitContainerCommit', (done)=>{
    const mockWrite = jest.fn(()=>{
        expect(mockWrite.mock.calls[0][0]).toBe('cd /missionMount ; git add . ; git commit -m "Bar"\n');
        done();
    });
    Docker.exec = async()=>({
        stderr:{
            setEncoding:()=>{},
            on:()=>{}
        },
        stdin:{
            setEncoding:()=>{},
            write:mockWrite
        }
    });
    Git.gitHostCommit();
    Git.gitContainerCommit('Foo', 'Bar');
});
