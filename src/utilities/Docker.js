class Docker{
    constructor(){
        if(!Docker.instance){
            Docker.instance = this;
            this._process = this.getElectron().require('process');
            this._childProcess = this.getElectron().require('child_process');
            this.scrimmageContainers = [];
            this.version = undefined;
            this.ip = undefined;
            this._vscrim = undefined;
            this.ready = this.getReady();
        }
        return Docker.instance;
    }

    getElectron() {
        // console.log(window.require('electron'));
        // console.log(window.require('electron').remote.require('child_process').exec('which docker'));
        return window.require('electron').remote;
    }

    vscrimUrl(hostname, port, password) {
        this._vscrim = this._vscrim ? this._vscrim : {
            hostname,
            port,
            password
        };
        this._vscrim.hostname = hostname ? hostname : this._vscrim.hostname;
        this._vscrim.port = port ? port : this._vscrim.port;
        this._vscrim.password = password ? password : this._vscrim.password;
        return `http://${this._vscrim.hostname}:${this._vscrim.port}/?password=${this._vscrim.password}`;
    }

    async getReady() {
        return await this.getDocker().then((ver)=>{
            this.version = ver;
            return this.readyUp(ver);
        }, (err)=>{
            console.error('Could not find Docker', err);
            let errmsg = 'Error:  Scenario Studio for SCRIMMAGE was unable to start vSCRIMMAGE using Docker.\n\n';
            errmsg += 'This error may occur if Docker is not installed on the system';
            errmsg += ' or if docker daemon is not running.\n\n';
            errmsg += 'To install Docker, please follow the directions on https://docs.docker.com/get-docker/.\n';
            errmsg = errmsg.concat(err);
            console.error('Could not find docker');
            const dialog = this.getElectron().dialog;
            const options = {title:'Could not find docker', message: errmsg};
            dialog.showMessageBox(options).then(()=>{
                this.getElectron().app.quit();
            });
        });
    }
    jupyterUrl(port) {
        this._vscrim = this._vscrim ? this._vscrim : {port};
        return `http://${this._vscrim.hostname}:${port}`;
    }

    async readyUp(){
        switch (this.version){
        case 'mac_Desktop':
        case 'win_Desktop':
            this.ip = '127.0.0.1';
            break;
        case 'mac_Toolbox':
        case 'win_Toolbox':
            this.ip = await this.dockerMachine();
            break;
        case 'linux':
            this.ip = '127.0.0.1';
            break;
        default:
            return Promise.reject('Could not start docker');
        }
        this.vscrimUrl(this.ip, 6901, 'vncpassword');
        this.scrimmageContainers = await this.getContainers();
        return Promise.resolve(true);
    }

    getDocker(){
        return new Promise((resolve, reject)=>{
            switch (this._process.platform){
            case 'darwin':
                this._childProcess.exec('which docker', (error)=>{
                    if(error){
                        reject(error + '\n\nBe sure docker is in your system\'s "Path" environment variable');
                    }else if(this._process.env.DOCKER_MACHINE_NAME){
                        resolve('mac_Toolbox');
                    }else {
                        resolve('mac_Desktop');
                    }
                });
                break;
            case 'win32':
                this._childProcess.exec('where docker', (error)=>{
                    if(error){
                        reject(error + '\n\nBe sure docker is in your system\'s "Path" environment variable');
                    }else if(this._process.env.DOCKER_MACHINE_NAME){
                        resolve('win_Toolbox');
                    }else {
                        resolve('win_Desktop');
                    }
                });
                break;
            case 'linux':
            default: // If platform is unkown will try as if linux
                this._childProcess.exec('which docker', (error, stdout)=>{
                    if(error || /^\s*$/.test(stdout)){
                        reject(error + '\n\nBe sure docker is in your system\'s "Path" environment variable');
                    }else if(stdout){
                        resolve('linux');
                    }
                });
            }
        });
    }

    dockerMachine(){
        const proc = this._childProcess.exec('docker-machine start default', (error, stdout)=>{
            stdout && console.log(stdout);
            error && console.error(error);
        });
        //wait for docker-machine to start vms
        return new Promise((resolve)=>{
            proc.on('exit', ()=>{
                this._childProcess.exec('docker-machine ip default', (error, stdout)=>{
                    if(error){
                        console.error(error);
                    }
                    resolve(stdout.replace(/\r?\n|\r/g, ''));
                });
            });
        });
    }

    getContainers(){
        let list = ['', '' ];
        const ps1 = this._childProcess.exec('docker ps -a -f ancestor=multiplatformautonomy/vscrimmage-vnc', (error, stdout)=>{
            if(error) {
                let errmsg = 'Error:  Scenario Studio for SCRIMMAGE was unable to access the Docker service.\n\n';
                errmsg += 'This error may occur if the docker daemon is not running,';
                errmsg += ' or if there is an issue with your Docker installation.\n\n';
                errmsg += 'If you have docker installed, you can make sure it is'; 
                errmsg += ' running using `docker info` from the command line.\n';
                errmsg += 'If it is not running, try restarting Docker for Windows,'; 
                errmsg += 'Docker for Mac, or dockerd on Linux.\n\n';
                errmsg = errmsg.concat(error);
                console.error(error);
                const dialog = this.getElectron().dialog;
                const options = {title:'Could not find docker', message: errmsg};
                dialog.showMessageBox(options).then(()=>{
                    this.getElectron().app.quit();
                });
                return;
            }
            list[0] += stdout;
        });
        const ps2 = this._childProcess.exec('docker ps -a -f ancestor=multiplatformautonomy/vscrimmage-headless'
            , (error, stdout)=>{
                if(error){
                    console.error(error);return;
                }
                list[1] += stdout;
            });
        let ps1Close = new Promise((resolve)=> ps1.on('close', ()=> resolve()));
        let ps2Close = new Promise((resolve)=> ps2.on('close', ()=> resolve()));
        
        return Promise.all([ps1Close, ps2Close ]).then(() =>{
            list[0] = list[0].split(/\n/);
            list[1] = list[1].split(/\n/);
            let params = list[0][0].split(/\s\s+/);
            let data = [...list[0].slice(1, list[0].length - 1), ...list[1].slice(1, list[0].length - 1) ];
            return this.extractArrayObjects(params, data);
        });  
    }

    extractArrayObjects(params, data) {
        let objs = [];
        return new Promise((resolve)=>{
            data.forEach((value, i, arr)=> {
                arr[i] = value.split(/\s\s+/);
            });
            data.forEach((value) => {
                if(value[0] === ''){
                    return; 
                }
                let container = {};
                params.forEach((ver, i) =>{
                    container[ver] = value[i];
                });
                objs.push(container);
            });
            resolve(objs);
        });
    }

    getImages(){
        let list = [''];
        const ps1 = this._childProcess.exec('docker images multiplatformautonomy/vscrimmage-vnc', (error, stdout)=>{
            list[0] += stdout;
            if(error) {
                const dialog = this.getElectron().dialog;
                const options = {
                    title:'Could not find docker',
                    message: 'There was an error connecting to docker when getting Images\n' + error
                };
                dialog.showMessageBox(options).then(()=>{
                    this.getElectron().app.quit();
                });
            }
        });
        let ps1Close = new Promise((resolve)=> ps1.on('close', ()=> resolve()));
        return Promise.all([ps1Close]).then(() => {
            list[0] = list[0].split(/\n/);
            let params = list[0][0].split(/\s\s+/);
            let data = [...list[0].slice(1, list[0].length - 1)];
            return this.extractArrayObjects(params, data);
        });  
    }

    async exec(container, command, options = {}){
        if(await this.ready !== true) {
            return Promise.reject(1);
        }
        let optionList = [];
        const detached = options.detached || options.detach || options.d;
        if(detached){
            optionList.push('-d');
        }
        const workdir = options.workdir || options.w;
        if(workdir){
            optionList.push('-w', workdir);
        }

        if(options.privileged){
            optionList.push('--privileged');
        }

        if(options.t || options.tty){
            optionList.push('-t');
        }

        let user = options.user;
        user = user === undefined ? options.u : user;
        if(user !== undefined){
            optionList.push('-u', user);
        }

        const env = options.e || options.env;
        if(env){
            optionList.push('-e', env);
        } 

        return this._childProcess.exec(
            `docker exec -i ${optionList.join(' ')} ${container} ${command}`
        );
    }

    async pull(image, tag){
        if(await this.ready !== true) {
            return Promise.reject(1);
        }
        let _image = image;
        if(tag){
            _image += ':' + tag;
        }
        return this._childProcess.exec(`docker pull ${_image}`, {encoding:'utf8'});
    }

    bashExec(container, command){
        try {
            return this._childProcess.execSync(`docker exec -i ${container} /bin/bash -ic ${command}`, {encoding:'utf8'});
        }catch (err) {
            return err;
        }   
    }

    async run(image, options, command, commandArgs){
        const exec = this._childProcess.exec;
        if(!options){
            return exec('docker ' + ['run', image, command || '', commandArgs || '' ].join(' '));
        }

        let params = ['run' ];

        if(options.i || options.interactive){
            params.push('-i');
        }

        if(options.d || options.detached){
            params.push('-d');
        }

        if(options.t || options.tty){
            params.push('-t');
        }

        if(options.name){
            params.push('--name');
            params.push(options.name);
        }

        if(options.envFile){
            params.push(`--env-file=${options.envFile}`);
        }

        const port = options.p || options.port;
        if(port){
            port.forEach((portMapping) => {
                params.push('-p');
                params.push(portMapping);
            });
        }
        
        // Add quotes around each path to account for spaces in user directories.
        const src = `"${(options.src || options.source).replace(/["']/, '')}"`;
        const dst = `"${(options.dst || options.destination || options.target).replace(/["']/, '')}"`;
        if(src){
            params.push('--mount');
            params.push(`type=bind,src=${src.replace(/^c:/gi, '//c').replace(/\\/g, '/')},dst=${dst}`);
        }

        params.push(image);

        let cArgs = commandArgs || '';
        if(command){
            if(command.includes(' ')){
                let commandSplit = command.split(/\s+/);
                params.push(commandSplit.splice(0, 1));
                cArgs = commandSplit;
            }else {
                params.push(command);
            }
            params.push(...cArgs);
        }
        return exec('docker ' + params.join(' '));
    }

    runSync(image, config){
        this.run(image, config).then(async(proc)=>{
            proc.stdout.setEncoding('utf8');
            proc.stderr.setEncoding('utf8');
            proc.stdout.on('data', console.log);
            proc.stderr.on('data', console.error);
            return await new Promise((resolve)=>proc.on('close', resolve));
        });
    }

    async start(container){
        return this._childProcess.exec(`docker start ${container}`);
    }

    stop(container){
        try {
            return this._childProcess.execSync(`docker stop ${container}`,  {encoding:'utf8'});
        }catch (err){
            return err;
        }
    }

    removeContainer(container){
        try {
            return this._childProcess.execSync(`docker rm ${container}`,  {encoding:'utf8'});
        }catch (err){
            return err;
        }
    }

    stopAndRemove(container){
        return [this.stop(container), this.removeContainer(container)];
    }
    // destination path has the be absolute. 
    copyToContainer(file, container, destination) {
        // must use absolute paths for the destination path
        return this._childProcess.execSync(`docker cp ${file} ${container}:${destination}`,  {encoding:'utf8'});
    }
    // source path has to be absolute. 
    copyFromContainer(container, source, destination) {
        // must use absolute paths for the source path
        return this._childProcess.execSync(`docker cp ${container}:${source} ${destination}`,  {encoding:'utf8'});
    }
    
    listContainers(){
        return this._childProcess.execSync('docker ps -a', {encoding:'utf8'});
    }


    async imageUpToDate(name, tag = 'latest'){
        let token;
        let repoPromise;
        let localPromise;
        try {
            token = await this._getTokenObject(name);
            token = token.token;

            repoPromise = this._getRepoID(name, tag, token);
            localPromise = this._getLocalID(name, tag);

            return Promise.all([localPromise, repoPromise]).then((values)=>{
                let localID = values[0];
                let repoID = values[1].config.digest;
                return (localID.trim() === repoID.trim());
            });
        }catch (err){
            console.warn(err);
            return;
        }
    }

    _https(options){
        const https = this.getElectron().require('https');
        return new Promise((resolve, reject)=>{
            https.get(options, (res)=>{
                if(res.statusCode !== 200){
                    reject(res.statusCode);
                }
                res.setEncoding('utf8');
                res.on('data', (data)=>{
                    resolve(JSON.parse(data));
                });
            });
        });
    }

    _getTokenObject(repo){
        return this._https({
            hostname:'auth.docker.io',
            path: `/token?scope=repository:${repo}:pull&service=registry.docker.io`
        });
    }

    _getRepoID(name, tag, token){
        return this._https({
            hostname:'registry-1.docker.io',
            path: `/v2/${name}/manifests/${tag}`,
            headers:{
                Accept: 'application/vnd.docker.distribution.manifest.v2+json',
                Authorization: `Bearer ${token}`
            }
        });
    }

    _getLocalID(name, tag){
        return new Promise((resolve)=>{
            let response = '';
            let exec = this._childProcess.exec(`docker image inspect -f "{{.ID}}" ${name}:${tag}`, (error, stdout)=>{
                if(error){
                    console.error(error);
                    return;
                }
                response += stdout.toString();
            });
            exec.on('close', ()=>resolve(response));
        });
    }
}
const instance = new Docker();

if(!window.testing){
    Object.preventExtensions(instance);
}
export default instance;
