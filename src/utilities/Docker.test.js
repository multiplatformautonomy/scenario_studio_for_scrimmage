import Docker from './Docker.js';

Docker.getElectron = ()=>({
    app:{quit:()=>{} },
    require: ()=>({
        get: (first, second)=>[
            second({
                statusCode:200,
                on: (first, second)=>{
                    second('{"token":"FOOBAR"}');
                },
                setEncoding:()=>{}
            })
        ]
    })
});

test('docker initializes', async(done) => {
    await new Promise((resolve)=>{
        const mockShowMessageBox = jest.fn(()=>({
            then: (first)=>{
                first();
            }
        }));
        const mockQuit = jest.fn();
        const mockGetElectron = jest.fn(()=>({
            dialog: {showMessageBox: mockShowMessageBox},
            app: {quit: mockQuit}
        }));
        Docker.getElectron = mockGetElectron;
        resolve();
    });
    await Docker.ready;
    expect(Docker._process.platform).toBeTruthy();
    done();
});

test('ready up sets proper ip', async() => {
    await Docker.ready;

    const _dockerMachine = Docker.dockerMachine;
    Docker.dockerMachine = jest.fn(() => '1.1.1.1');
    
    Docker.version = 'win_Desktop';
    await Docker.readyUp();
    expect(Docker.ip).toStrictEqual('127.0.0.1');

    Docker.version = 'win_Toolbox';
    await Docker.readyUp();
    expect(Docker.dockerMachine).toBeCalledTimes(1);
    expect(await Docker.ip).toStrictEqual('1.1.1.1');

    Docker.version = 'mac_Toolbox';
    await Docker.readyUp();
    expect(Docker.dockerMachine).toBeCalledTimes(2);
    expect(await Docker.ip).toStrictEqual('1.1.1.1');
    
    Docker.version = 'mac_Desktop';
    await Docker.readyUp();
    expect(Docker.ip).toStrictEqual('127.0.0.1');
    
    Docker.version = 'linux';
    await Docker.readyUp();
    expect(Docker.ip).toStrictEqual('127.0.0.1');

    Docker.version = 'default';
    await expect(Docker.readyUp()).rejects.toBe('Could not start docker');
    Docker.dockerMachine = _dockerMachine;
});

test('that containers are returning non undefined values', () => {
    expect(Docker.stop('test')).not.toBe(undefined);
    expect(Docker.removeContainer('test')).not.toBe(undefined);

    Docker._childProcess = {execSync: jest.fn((first) => first)};

    expect(Docker.copyToContainer('hello', 'world', 'foo')).toStrictEqual('docker cp hello world:foo');
    expect(Docker.copyFromContainer('hello', 'world', 'foo')).toStrictEqual('docker cp hello:world foo');
});

test('get docker switch cases with reject error returned', ()=>{
    Docker._process = {platform: 'darwin'};

    let mockExec = jest.fn((first1, second) => {
        second('world');
    });
    Docker._childProcess = {exec: mockExec};

    Docker.getDocker();

    Docker._process = {platform: 'win32'};
    Docker.getDocker();

    Docker._process = {platform: 'linux'};
    Docker.getDocker();
    expect(mockExec).toBeCalledTimes(3);
});

test('get docker switch cases with DOCKER_MACHINE_NAME', ()=>{
    Docker._process = {
        env: {DOCKER_MACHINE_NAME: 'hello'},
        platform: 'darwin'
    };

    let mockExec = jest.fn((first1, second) => {
        second(0);
    });
    Docker._childProcess = {exec: mockExec};

    Docker.getDocker();

    Docker._process = {
        env: {DOCKER_MACHINE_NAME: 'hello'},
        platform: 'win32'
    };
    Docker.getDocker();

    let mockExecLinux = jest.fn((first1, second) => {
        second(0, 'hello');
    });
    Docker._childProcess = {exec: mockExecLinux};
    Docker._process = {platform: 'linux'};
    Docker.getDocker();

    expect(mockExec).toBeCalledTimes(2);
    expect(mockExecLinux).toBeCalledTimes(1);
});

test('get container throws error message', () => {
    let mockReturn = jest.fn((first1, second)=> {
        second();
    });

    let mockExec = jest.fn((first1, second)=> {
        second(1, 'hello');
        return {on: mockReturn};
    });
    const mockShowMessageBox = jest.fn(()=>({
        then: (first)=>{
            first();
        }
    }));
    let mockGetElectron = jest.fn(()=> ({
        dialog: {showMessageBox: mockShowMessageBox},
        app: {quit: jest.fn(()=>{})}
    }));
    Docker.getElectron = mockGetElectron;
    Docker._childProcess = {exec: mockExec};

    Docker.getContainers();
    expect(mockExec).toBeCalled();
    expect(mockReturn).toBeCalled();

    mockExec = jest.fn((first1, second)=> {
        second(0, 'hello');
        return {on: mockReturn};
    });
    Docker.getContainers();
});

test('if docker machine console logs proper error message', () => {
    const _childProcess = Docker._childProcess;
    let first = null;
    Docker._childProcess = {
        exec: jest.fn((first1, second) => {
            first = first1;
            second(0, 'hello');
            return {
                on(string, fun) {
                    expect(['exit', 'data', 'error']).toContain(string);
                    fun();
                }
            };
        })
    };

    Docker.dockerMachine();
    let first1 = null;
    let mockReturn = jest.fn((first2, second)=> {
        first1 = first2;
        second();
    });

    
    Docker._childProcess = {
        exec: jest.fn((first1, second) => {
            second(1, 'hello');
            return {on: mockReturn};
        })
    };
    Docker.dockerMachine();

    expect(first).toStrictEqual('docker-machine ip default');
    expect(first1).toStrictEqual('exit');
    Docker.child_process = _childProcess;
});

test('exec returns proper command', async () => {
    Docker.ready = false;
    let options = {
        detached:true,
        workdir:true,
        privileged:true,
        tty:true,
        user:0,
        env:'FOOBAR'
    };
    await expect(Docker.exec('hello', 'world')).rejects.toBe(1);

    Docker.ready = true;
    Docker._childProcess = {spawn: jest.fn((first1, second) => second)};
    Docker.exec('hello', 'world', options);
});

test('if docker runs', async () => {
    Docker.ready = true;
    Docker._childProcess = {spawn: jest.fn((first1, second) => second)};
    Docker.run('hello', 0);

    let options = {
        interactive: true,
        detached:true,
        tty:true,
        name: 'world',
        envFileName: 'foo',
        port: '6000',
        source: 'test',
        destination: 'target'
    };

    let command = 'ls -a';

    Docker.getContainers = jest.fn(()=>{});

    Docker.run('hello', options, command);

    options = {
        name: 'world',
        envFileName: 'foo',
        port: '6000',
        source: 'test',
        target: 'target'
    };

    Docker.run('hello', options);
});

test('docker pull', async(done)=>{
    const mock = jest.fn((input)=>input);
    Docker._childProcess = {exec: mock};
    await Docker.pull('FOO', 'BAR');
    expect(mock.mock.calls.length).toBe(1);
    expect(mock.mock.results[0].value).toBe('docker pull FOO:BAR');

    Docker.ready = false;
    let error;
    try {
        await Docker.pull('FOO', 'BAR');
    }catch (err){
        error = err;
    }
    expect(error).toBe(1);
    done();
});


test('docker resolve return', () => {
    Docker.getDocker = jest.fn().mockResolvedValue('1.0.0.1');
    Docker.readyUp = jest.fn(()=>{});
    Docker.getReady();
});

test('docker reject return', () => {
    Docker.getDocker = jest.fn().mockRejectedValue('1.0.0.1');
    Docker.getElectron = jest.fn(() => {
        {
            jest.fn(()=>{});
        }
    });
    Docker.getReady();
});

test('docker sets vscrim url on function call', () => {
    Docker.vscrimUrl('localhost', '8080', 'notmypassword');
    let url = Docker.vscrimUrl();
    let urlCheck = 'http://localhost:8080/?password=notmypassword';
    expect(url).toStrictEqual(urlCheck);
});

test('docker sets jupyter url on function call', () => {
    Docker.jupyterUrl('8888');
    let url = Docker.jupyterUrl('8888');
    let urlCheck = 'http://localhost:8888';
    expect(url).toStrictEqual(urlCheck);
});

test('docker getImages throws error', () => {
    let mockReturn = jest.fn((first1, second)=> {
        second();
    });

    let mockExec = jest.fn((first1, second)=> {
        second(1, 'hello');
        return {on: mockReturn};
    });
    const mockShowMessageBox = jest.fn(()=>({
        then: (first)=>{
            first();
        }
    }));
    const mockQuit = jest.fn();
    const mockGetElectron = jest.fn(()=>({
        dialog: {showMessageBox: mockShowMessageBox},
        app: {quit: mockQuit}
    }));
    Docker.getElectron = mockGetElectron;
    Docker._childProcess = {exec: mockExec};
    Docker.getImages();
    expect(Docker.getImages).toThrow();
    expect(mockExec).toBeCalled();
    expect(mockReturn).toBeCalled();
});

test('docker getImages correctly extracts image names', () => {
    let mockReturn = jest.fn(() => {
        new Promise((second) => second());
    });
    let mockExec = jest.fn((first1, second) => {
        second(0, 'PARAM1\n VAL1');
        return {on: mockReturn};
    });
    Docker._childProcess = {exec: mockExec};
    Docker.getImages().then((objs)=>{
        expect('PARAM1' in objs[0]).toBe(true);
        expect(objs[0]['PARAM1']).toBe('VAL1');
    });
    expect(mockExec).toBeCalled();
    expect(mockReturn).toBeCalled();
});

describe('imageUpToDate', ()=>{
    let mockResponse;
    beforeAll(()=>{
        mockResponse = {
            statusCode:200,
            on: (first, second)=>{
                second('{"token":"FOOBAR","config":{"digest":"BARFOO"}}');
            },
            setEncoding:()=>{}
        };
        Docker.getElectron = ()=>({
            require: ()=>({
                get: (first, second)=>{
                    second(mockResponse);
                }
            })
        });

        Docker._childProcess = {
            exec:(first, second)=>{
                second(1);
                second(0, 'BARFOO');
                return {on:(first, second)=>second()};
            }
        };
    });
    test('self', ()=>{
        expect(Docker.imageUpToDate()).toBeTruthy();
    });

    test('_getTokenObj', async(done)=>{
        let response = await Docker._getTokenObject();
        expect(response.token).toBe('FOOBAR');
        done();
    });
    test('_getRepoID', async(done)=>{
        let response = await Docker._getRepoID();
        expect(response.config.digest).toBe('BARFOO');
        done();
    });
    test('_getLocalID', async(done)=>{
        let response = await Docker._getLocalID();
        expect(response).toBe('BARFOO');
        done();
    });

    test('self error', ()=>{
        mockResponse.statusCode = 404;
        expect(Docker.imageUpToDate()).toBeTruthy();
    });

    test('_getTokenObj error', async(done)=>{
        let error;
        let response;
        try {
            response = await Docker._getTokenObject();
        }catch (err){
            error = err;
        }
        expect(error).toBe(404);
        expect(response).toBe(undefined);
        done();
    });
    test('_getRepoID error', async(done)=>{
        let error;
        let response;
        try {
            response = await Docker._getRepoID();
        }catch (err){
            error = err;
        }
        expect(error).toBe(404);
        expect(response).toBe(undefined);
        done();
    });
});
