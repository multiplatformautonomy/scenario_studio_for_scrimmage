const parseXml = require('./parseXML');

describe('parsing and saving xml files', () => {
    test('no file argument should equal false', () => expect(parseXml.readParseFile()).rejects.toBe(false));

    test('bad file argument should equal false', () => expect(parseXml.readParseFile('out.txt')).rejects.toBe(false));

    test('valid xml file argument should equal true', () => {
        const fileIn = 'src/screens/LiveMission.test.xml';
        return expect(parseXml.readParseFile(fileIn)
            .then((data) => parseXml.saveJSToXml(data.fileName, data.data)))
            .resolves.toBe(true);
    });
    
    test('validate file in', () => {
        process.argv = ['hello', 'world', 'foo', 'bar'];
        expect(parseXml.validateFileIn()).toStrictEqual([false, null]);
        expect(parseXml.validateFileIn('/hello.xml')).toStrictEqual([true, '/hello.xml']);
    });
});
