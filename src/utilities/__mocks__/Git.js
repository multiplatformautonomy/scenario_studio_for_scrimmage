export class MockGit{
    getGitSettingsFilePath(){
        return 'Foo Bar';
    }
    loadGitSettings(){
        return {gitName:'Foo', gitEmail:'Bar'};
    }
    saveGitSettings(){}
    async gitClone(){
        return [0, 'Foo Bar'];
    }
    gitHostCommit(){}
    gitContainerCommit(){}
}

const instance = new MockGit;
export default instance;
