class Docker{
    async exec(){
        let missionResolve = {
            stdout: {
                setEncoding: ()=>{},
                on: (topic_, fun) => {
                    fun('Params:\nhello=world');
                    fun('DUMMY_TEXT.xml');
                }
            },
            stdin:{
                setEncoding: ()=>{},
                write:()=>{}
            },
            stderr: {
                setEncoding: ()=>{},
                on: (topic_, fun) => {
                    fun(6);
                }
            },
            on: (topic_, fun) => {
                fun();
            }
        };
        return missionResolve;
    }
    async start(){
        try {
            return require('process').exec('echo', ['test' ]);
        }catch (err){
            null;
        }
    }
    copyToContainer(){}
    stopAndRemove(){}
    bashExec(something){
        try {
            something();
        }catch (exception){
            console.log(something, exception);
        }
    }
    
    getImages(){
        return {
            then: (callback) => {
                callback([{REPOSITORY: 'repo', TAG: 'tag'}]);
            }
        };
    }

    async run(image) {
        let subProc = {
            stdout: {
                setEncoding: ()=>{},
                on: (first, second) =>{
                    second(first);
                }
            },
            stdin: {
                setEncoding: ()=>{},
                write: ()=>{}
            },
            stderr: {
                setEncoding: ()=>{},
                on: (first, second) =>{
                    second(first);
                }
            },
            on: (first, second) => {
                second(image === 'error' ? 1 : 0);
            }
        };
        return subProc;
    }
    async imageUpToDate(){
        return true;
    }
}
const instance = new Docker();
export default instance;
