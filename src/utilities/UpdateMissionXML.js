import {readParseFile, saveJSToXml }from './parseXML';

class EntityProperties {
    _updatePluginXml(entityTypeStr, dataObj, entityProperties, variableDefaults){
        entityProperties[entityTypeStr].enabledPlugins.forEach((plugin, index)=>{
            // Add attributes (args) to property element.
            let pluginName = plugin.pluginType;

            let argList = this._updateArgValues(plugin, variableDefaults); // pass in plugin
            let pluginObj = {$: argList };
            let pluginValue = plugin.name;
            if(pluginValue !== null){
                pluginObj['_'] = pluginValue;
            }

            if(dataObj[pluginName]) {
                dataObj[pluginName][index] = pluginObj;
            }else {
                dataObj[pluginName] = [];
                dataObj[pluginName][index] = pluginObj;
            }
        });
    }

    _updateMissionSettingsXml(data, entityProperties, variableDefaults){
        entityProperties['mission-settings'].properties.forEach((property)=>{
            // Add attributes (args) to property element.
            let propertyName = property.name;

            let argList = this._updateArgValues(property, variableDefaults);
            // Attributes are stored as '$' and inner text is stored as '_'
            data.data.runscript[propertyName] = {$: argList };

            let propertyValue = this._updatePropertyValue(property, variableDefaults);
            
            if(propertyValue !== null){
                data.data.runscript[propertyName]['_'] = propertyValue;
            }
        });

        this._updatePluginXml('mission-settings', data.data.runscript, entityProperties, variableDefaults);
        return data;
    }

    // TODO: explore debouncing and refactor XML logic
    // debouncedMissionXmlFile : _.debounce(() => {put everything in here}, 500)
    // Save the current state of data to the mission XML file
    updateMissionXmlFile(xmlFilePath, missionName, entityList, entityProperties, variableDefaults = {}){
        // this.debouncedMissionXmlFile
        this.updatePromise = readParseFile(xmlFilePath).then((data) =>{
            // parse-stringify used to create a new copy of the template. This prevents bindings that edit the template.
            data = JSON.parse(JSON.stringify(require('../assets/MissionXmlTemplate.js').default));
            data.data.runscript.$.name = missionName;
            data = this._updateMissionSettingsXml(data, entityProperties, variableDefaults);

            entityList.forEach((entity, index)=>{
                let entityId = entity.props.id;
                let entityObj = {name:[entityProperties[entityId].name ] };
                entityObj['icon'] = entity.props.faIcon;

                entityProperties[entityId].properties.forEach((property)=>{
                    let propertyName = property.name;
                    propertyName = propertyName.replace(/ /g, '_');
                    let propertyValue = this._updatePropertyValue(property, variableDefaults);
                    entityObj[propertyName] = propertyValue;
                });

                this._updatePluginXml(entityId, entityObj, entityProperties, variableDefaults);
                data.data.runscript.entity[index] = entityObj;
            });
            return data;
        }).then((data) => {
            saveJSToXml(xmlFilePath, data.data);
        });
    }
    
    _updateArgValues(property, variableDefaults) {
        let argList = {};
        if(property.args){
            property.args.forEach((arg)=>{
                let match = arg.value.toString().trim().match(/\${\s*(\w+)\s*=?[\w\s]*}/);
                argList[arg.name] = match && variableDefaults[ match[1] ] ? 
                    `\${${match[1]}=${variableDefaults[ match[1] ]}}` : 
                    arg.value;
            });
        }
        return argList;
    }

    _updatePropertyValue(property, variableDefaults) {
        let propertyValue = property.value;
        if(propertyValue) {
            let match = property.value.toString().trim().match(/\${\s*(\w+)\s*=?[\w\s]*}/);
            propertyValue = match && variableDefaults[match[1]] ?
                `\${${match[1]}=${variableDefaults[match[1]]}}` :
                property.value;
        }
        return propertyValue;
    }
}
const instance = new EntityProperties();

export default instance;
