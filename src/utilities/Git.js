import Docker from './Docker';
class Git{
    getElectron(){
        return window.require('electron').remote;
    }

    getGitSettingsFilePath() {
        const electron = this.getElectron();
        const settingsFilename = 'settings.json';
        const path = electron.require('path');
        const userDataPath = (electron.app || electron.remote.app).getPath('userData');
        const filepath = path.join(userDataPath, settingsFilename);
        return filepath;
    }

    loadGitSettings() {
        const fs = this.getElectron().require('fs');
        const filepath = this.getGitSettingsFilePath();
        let data = {};
        try {
            data = JSON.parse(fs.readFileSync(filepath));
        }catch (error){
            data = {gitSettings : {username:'', email:''}};
            fs.writeFileSync(filepath, JSON.stringify(data));
        }
        return data.gitSettings;
    }

    saveGitSettings(gitObject) {
        if(!gitObject.gitSettings){
            throw Error('Invalid Object');
        }
        const fs = this.getElectron().require('fs');
        const filepath = this.getGitSettingsFilePath();
        try {
            fs.writeFileSync(filepath, JSON.stringify(gitObject));
            return 'Git settings saved successfully';
        }catch (error){
            return 'Could not save because of the following error:\n' + error;
        }
    }

    gitClone(url, targetDirectory) {
        const electron = this.getElectron();
        const child_process = electron.require('child_process');
        const proc = child_process.exec(`git clone ${url}`, {cwd:targetDirectory});
        let error = '';
        proc.stderr.on('data', (data)=>{
            error += data;
        });

        return new Promise((resolve)=>{
            proc.on('close', (code)=> {
                resolve([code, error]);
            });
        });   
    }

    gitHostCommit(){/*NOT Implemented Yet*/}

    gitContainerCommit(container, message){
        const bashCommand = `cd /missionMount ; git add . ; git commit -m "${message}"\n`;     
        Docker.exec(container, '/bin/bash').then((output) => {
            output.stderr.setEncoding('utf8');
            output.stderr.on('data', console.error);
            output.stdin.setEncoding('utf8');
            output.stdin.write(bashCommand);
        });
    }
}

const instance = new Git();
export default instance;
