import React from 'react';
import DialogBox from './DialogBox';
import { shallow, mount }from 'enzyme';


/** 
 * Test Case that I should cover:
 * + DialogBox initializes with no errors
 * + DialogBox renders nothing on initialization
 * + DialogBox renders only when show is true
 * + DialogBox stops rendering after show becomes false
 * + DialogBox stops rendering after a click outside of the contentRef
 * + DialogBox keeps rendering after a click within the contentRef
 * + DialogBox can take in children
 * - DialogBox doesn't break when contentRef or wrapperRef is Undefinded or null
 */

test('DialogBox initializes', ()=>{
    const dBox = shallow(<DialogBox className=''/>);
    expect(dBox.instance().className).toBe('component-dialogbox');
});

test('DialogBox reneders nothing on initialization', ()=>{
    const dBox = shallow(<DialogBox/>);
    expect(dBox.state('show')).toBeFalsy();
    expect(dBox.render().length).toBe(0);
});


test('DialogBox render only when show is true', ()=>{
    const dBox = shallow(<DialogBox/>);
    expect(dBox.state('show')).toBeFalsy();
    dBox.setState({show:true });
    expect(dBox.state('show')).toBeTruthy();
    expect(dBox.render().length).toBe(1);
});

test('DialogBox stops rendering when show becomes false', ()=>{
    const dBox = shallow(<DialogBox/>);
    dBox.setState({show:true });
    expect(dBox.state('show')).toBeTruthy();
    expect(dBox.render().length).toBe(1);
    dBox.setState({show:false });
    expect(dBox.render().length).toBe(0);
});

test('DialogBox stops rendering after a click outside of the contentRef', () =>{
    const dBox = mount(<DialogBox/>);
    dBox.instance().setState({show:true });
    expect(dBox.state('show')).toBeTruthy();
    window.map.click({target: dBox.instance().wrapperRef });
    expect(document.addEventListener).toBeCalled();
    expect(dBox.state('show')).toBeFalsy();
    dBox.unmount();
    expect(document.removeEventListener).toBeCalled();
});

test('DialogBox keeps rendering after a click within the contentRef', () =>{
    const dBox = mount(<DialogBox/>);
    dBox.instance().setState({show:true });
    expect(dBox.state('show')).toBeTruthy();
    window.map.click({target: dBox.instance().contentRef });
    expect(document.addEventListener).toBeCalled();
    expect(dBox.state('show')).toBeTruthy();
    dBox.unmount();
    expect(document.removeEventListener).toBeCalled();
});

test('DialogBox contentRef or wrapperRef is undef or null unhandled', () =>{
    const dBox = mount(<DialogBox/>);
    dBox.instance().setState({show:true });
    expect(dBox.state('show')).toBeTruthy();
    dBox.instance().wrapperRef = null;
    dBox.instance().contentRef = undefined;
    window.map.click({target: dBox.instance().contentRef });
    expect(document.addEventListener).toBeCalled();
    expect(dBox.state('show')).toBeTruthy(); 
    dBox.instance().wrapperRef = undefined;
    dBox.instance().contentRef = null;
    window.map.click({target: dBox.instance().contentRef });
    expect(document.addEventListener).toBeCalled();
    expect(dBox.state('show')).toBeTruthy();
    dBox.unmount();
    expect(document.removeEventListener).toBeCalled();
});



test('DialogBox can take in children', () =>{
    let dBox = shallow(<DialogBox>DialogBox</DialogBox>);
    expect(dBox.instance().props.children).toBe('DialogBox');
    dBox = shallow(<DialogBox><div/></DialogBox>);
    expect(dBox.instance().props.children.type).toBe('div');
    dBox = shallow(<DialogBox>DialogBox<div/></DialogBox>);
    expect(dBox.instance().props.children[0]).toBe('DialogBox');
    expect(dBox.instance().props.children[1].type).toBe('div');
});
