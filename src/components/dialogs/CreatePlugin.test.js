

import React from 'react';
import CreatePlugin from './CreatePlugin';
import { mount, shallow }from 'enzyme';

test('Plugin', () => {
    const wrap = shallow(<CreatePlugin onCreate={() => { }} onClose={() => { }} />);

    expect(wrap.length).toEqual(1);
});

test('Button renders', () => {
    const wrap = mount(<CreatePlugin onCreate={() => { }} onClose={() => { }} />);

    //expect(wrap.find('.buttons')).to.have.lengthOf(2);
    expect(wrap.find('SSSButton').length).toBe(2);
});

test('cancel button renders', () => {
    const wrap = mount(<CreatePlugin onCreate={() => { }} onClose={() => { }} />);

    expect(wrap.text()).toMatch('Cancel');
});

test('On change function executes', () => {
    const mockFunc = jest.fn(() => { });
    let wrap = mount(<CreatePlugin />);
    wrap.setState({ pluginName: 'PluginName', errorMessage: null });

    wrap.find('button').at(0).simulate('click');

    wrap.unmount();

    wrap = mount(<CreatePlugin onCreate={mockFunc} onClose={mockFunc} />);
    wrap.setState({ pluginName: 'PluginName', errorMessage:null });

    wrap.find('button').at(0).simulate('click');

    expect(mockFunc).toHaveBeenCalledTimes(2);
});

test('input box on change changes state.', () => {
    function mockFunc(value) {
        wrap.setState({ pluginName: value });
    }
    const wrap = mount(<CreatePlugin onCreate={mockFunc} />);
    wrap.setState({ pluginName: 'PluginName' });

    wrap.find('input').simulate('change', { target: { value: 'foo' } });
    wrap.find('button').at(0).simulate('click');

    expect(wrap.state('pluginName')).toBe('foo');
});
test('Plugin name cannot be empty.', () => {
    function mockFunc(value) {
        wrap.setState({ pluginName: value });
    }
    const wrap = mount(<CreatePlugin onCreate={mockFunc} />);
    wrap.setState({ pluginName: 'PluginName' });

    wrap.find('input').simulate('change', { target: { value: '' } });
    wrap.find('button').at(0).simulate('click');

    expect(wrap.state('errorMessage')).toBe('Plugin name cannot be empty');
});
test('First letter of plugin name must be uppercase.', () => {
    function mockFunc(value) {
        wrap.setState({ pluginName: value });
    }
    const wrap = mount(<CreatePlugin onCreate={mockFunc} />);
    wrap.setState({ pluginName: 'PluginName' });

    wrap.find('input').simulate('change', { target: { value: 'lowercase' } });
    wrap.find('button').at(0).simulate('click');

    expect(wrap.state('errorMessage')).toBe('Plugin name may not begin with lowercase');
});

test('No spaces allowed in pugin name.', () => {
    function mockFunc(value) {
        wrap.setState({ pluginName: value });
    }
    const wrap = mount(<CreatePlugin onCreate={mockFunc} />);
    wrap.setState({ pluginName: 'PluginName' });

    wrap.find('input').simulate('change', { target: { value: 'With spaces' } });
    wrap.find('button').at(0).simulate('click');

    expect(wrap.state('errorMessage')).toBe('Plugin name cannot have any spaces');
});


test('Valid plugin name.', () => {
    function mockFunc(value) {
        wrap.setState({ pluginName: value });
    }
    
    const wrap = mount(<CreatePlugin onCreate={mockFunc} pluginList={['notValid.xml']}/>);
    wrap.setState({ pluginName: 'PluginName' });

    wrap.find('input').simulate('change', { target: { value: 'Valid' } });
    wrap.find('button').at(0).simulate('click');

    expect(wrap.state('errorMessage')).toBe(null);
});
test('Plugin already exists  test.', () => {
    function mockFunc(value) {
        wrap.setState({ pluginName: value });
    }
    
    const wrap = mount(<CreatePlugin onCreate={mockFunc} pluginList={['Valid.xml']}/>);
    wrap.setState({ pluginName: 'PluginName' });

    wrap.find('input').simulate('change', { target: { value: 'Valid' } });
    wrap.find('button').at(0).simulate('click');

    expect(wrap.state('errorMessage')).toBe('Plugin name already exists');
});
