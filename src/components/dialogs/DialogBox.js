import React from 'react';
import './DialogBox.css';
import propTypes from 'prop-types';

export default class SSSDialogBox extends React.Component {
    constructor(props){
        super(props);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.setContentRef = this.setContentRef.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.className = ('component-dialogbox ' + props.className).trim();
        this.state = {show:false };
    }

    setShow(show){
        this.setState({show:show});
    }

    componentDidMount(){
        if(this.props.closeable){
            document.addEventListener('click', this.handleOutsideClick);
        }
    }

    componentWillUnmount() {
        if(this.props.closeable){
            document.removeEventListener('click', this.handleOutsideClick);
        }
    }

    setWrapperRef(node){
        this.wrapperRef = node;
    }
    setContentRef(node){
        this.contentRef = node;
    }
    handleOutsideClick(event){
        if(!(this.wrapperRef && this.contentRef)) {
            return;
        }//Return if wrapper or content refs equals nothing
        if(!this.contentRef.contains(event.target) && this.wrapperRef.contains(event.target)) {
            this.setState({show:false });
        }
    }

    render(){
        if(this.state.show){
            return (
                <div ref={this.setWrapperRef} className='container-dialogbox'>
                    <div ref={this.setContentRef} className={this.className}>
                        <div className='dialogbox-title'>
                            {this.props.title}
                        </div>
                        <div className='content'>{this.props.children}</div>
                    </div>
                </div>
            );
        }
        return null;
    }
}

SSSDialogBox.defaultProps = {closeable:true};

SSSDialogBox.propTypes = {
    className: propTypes.string,
    closeable: propTypes.bool,
    children: propTypes.oneOfType([propTypes.element, propTypes.string, propTypes.array ]),
    title:propTypes.oneOfType([propTypes.string, propTypes.element, propTypes.array])
};
