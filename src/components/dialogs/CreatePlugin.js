import React from 'react';
import propTypes from 'prop-types';
import './CreatePlugin.css';
import Button from '../Button';
export default class CreatePlugin extends React.Component {
    constructor(props){
        super(props);
        this.state = {pluginName: '', errorMessage : ' '};
        this.handleOnCreate = this.handleOnCreate.bind(this);
        this.inputCheck = this.inputCheck.bind(this);
    }

    inputCheck(){
        if(this.state.pluginName === ''){
            this.setState({errorMessage:'Plugin name cannot be empty'});
        }else if(this.state.pluginName.includes(' ')){
            this.setState({errorMessage:'Plugin name cannot have any spaces'});
        }else if(this.state.pluginName[0] !== this.state.pluginName[0].toUpperCase()){
            this.setState({errorMessage:'Plugin name may not begin with lowercase'});
        }else if(this.props.pluginList.includes(this.state.pluginName + '.xml')){
            this.setState({errorMessage:'Plugin name already exists'});
        }else {
            this.setState({errorMessage:null});
        }
    }

    handleOnCreate () {
        if(!this.state.errorMessage){
            this.props.onCreate(this.state.pluginName, this.props.pluginType);
            this.props.onClose();
        }
    }
    render (){
        return (
            
            <div className='pluginDiv'>
                <label>
                    <div className='dialog-label-text'>
                    Plugin Name: 
                    </div>
                    <input className='dialog-text-input' type='text' name='Plugin Name' onChange={(event)=>{
                        this.setState({pluginName:event.target.value}, this.inputCheck);
                    }}/>
                    <span style={{color:'red'}}>{this.state.errorMessage}</span>
                </label>
                <div className='dialog-button-container'>
                
                    <Button className='dialog-button-blue' clickHandler={this.handleOnCreate}>Create Plugin</Button>
                    <Button className='dialog-button-red' clickHandler={this.props.onClose}>Cancel</Button>
                </div>
            </div>
        );
    }
}
CreatePlugin.defaultProps = { onClose: () => { }, onCreate: () => { }, pluginType:'', pluginList:{} };
CreatePlugin.propTypes = {
    onClose: propTypes.func,
    onCreate: propTypes.func,
    pluginType: propTypes.string,
    pluginList: propTypes.array
};


