import React from 'react';
import './Button.css';
import propTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';

export default class SSSButton extends React.Component {
    constructor(props){
        super(props);
        //this.clickHandler = this.props.clickHandler.bind(this);
        this.className = ('component-button ' + this.props.className).trim();
    }

    button() {
        return (
            <div className= {this.className}>
                <button onClick={this.props.clickHandler}
                    type="button"  
                    disabled={this.props.isDisabled || false}
                >
                    <i  className={this.props.faIcon}/>
                    {this.props.children}
                </button>
            </div>
        );
    }
    
    render() {
        if(this.props.tag && this.props.message) {
            return (
                [
                    <a key={this.props.tag} data-tip data-for={this.props.tag}>
                        {this.button()}
                    </a>,
                    <ReactTooltip id={this.props.tag}
                        key={this.props.message}
                        place="right" 
                        type='dark' 
                        effect='solid' 
                        delayHide={200} 
                        delayShow={200}>
                        <span>{this.props.message}</span>
                    </ReactTooltip>
                ]
            );
        }else {
            return (
                this.button()
            );
        }
    }
}

SSSButton.propTypes = {
    children: propTypes.oneOfType([propTypes.element, propTypes.string, propTypes.array ]),
    clickHandler: propTypes.func,
    className: propTypes.string,
    faIcon: propTypes.string,
    tag: propTypes.string,
    message: propTypes.string,
    isDisabled: propTypes.bool
};
