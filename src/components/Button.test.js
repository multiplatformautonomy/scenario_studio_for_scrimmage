import React from 'react';
import { shallow }from 'enzyme';
import SSSButton from './Button';

test('button with icon should contain one icon', () => {
    const button = shallow(<SSSButton clickHandler={()=>{}} faIcon="fa-cogs"/>);
    expect(button.find('i')).toHaveLength(1);
});

test('value should render text', () => {
    const button = shallow(<SSSButton clickHandler={()=>{}}>RenderThisText</SSSButton>);
    expect(button.find('button').text()).toBe('RenderThisText');
});

test('no value should not render text', () => {
    const button = shallow(<SSSButton clickHandler={()=>{}} />);
    expect(button.find('button').text()).toHaveLength(0);
});

test('onClick should call clickhandler', () => {
    const chobj = {
        clickh() {
            return true;
        }
    };
    const clickSpy = jest.spyOn(chobj, 'clickh');
    const button = shallow(<SSSButton clickHandler={chobj.clickh}/>);
    button.find('button').simulate('click');
    expect(clickSpy).toBeCalled();
    expect(clickSpy).toBeCalledTimes(1);
    button.find('button').simulate('click');
    expect(clickSpy).toBeCalledTimes(2);
});
