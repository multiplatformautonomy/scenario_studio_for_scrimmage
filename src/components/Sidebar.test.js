import React from 'react';
import { shallow, mount }from 'enzyme';
import SSSSidebar from './Sidebar';

test('Sidebar just has text passed in', () => {
    const Sidebar = shallow(<SSSSidebar show>SIDEBAR</SSSSidebar>);
    expect(Sidebar.instance().props.children).toBe('SIDEBAR');
});


test('Click off sidebar changes show state', () =>{
    const div = mount(<SSSSidebar show closeable/>);
    const sidebar = div.find(SSSSidebar).first();
    expect(sidebar.state('show')).toBeTruthy();
    window.map.mouseup({target: null });
    expect(document.addEventListener).toBeCalled();
    expect(sidebar.state('show')).toBeFalsy();
    div.unmount();
    expect(document.removeEventListener).toBeCalled();
});
