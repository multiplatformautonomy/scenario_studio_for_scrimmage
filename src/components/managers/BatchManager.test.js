import React from 'react';
import {mount}from 'enzyme';
import Batch from './BatchManager';

test('batch renders all of its children', () => {
    const batch = mount(<Batch />);
    expect(batch.find('button')).toHaveLength(2);
    expect(batch.find('input')).toHaveLength(3);
});

test('simulate command creation', () => {
    const batch = mount(<Batch />);
    batch.instance().setState({numRuns: 1, numProcessors: 1, experimentName:'Foo'});
    const output = batch.instance().createCommand();
    let expected = 'python3 /home/scrimmage/scrimmage/scripts/run_experiments.py ' +
        '-l /experiments -t 1 -m /home/scrimmage/scrimmage/missions/MASS_mission.xml ' +
        '-p 1 -r /home/scrimmage/scrimmage/config/ranges/batch-ranges.xml -n Foo';
    expect(output).toBe(expected);
});

test('simulate input blur in textboxes', ()=>{
    const batch = mount(<Batch/>);
    const textCollection = batch.find('input');
    textCollection.forEach((box)=>{
        box.simulate('change', {target:{value:'Foo'}});
    });
    for(const [, value]of Object.entries(batch.state())){
        expect(value).toBe('Foo');
    }
});

test('simulate button presses', ()=>{
    const MOCKonRun = jest.fn(Batch.defaultProps.onRun);
    const MOCKonCancel = jest.fn(Batch.defaultProps.onCancel);
    const batch = mount(<Batch onRun={MOCKonRun} onCancel={MOCKonCancel}/>);
    const buttonCollection = batch.find('button');
    buttonCollection.forEach((button)=>{
        button.simulate('click');
    });
    expect(MOCKonRun.mock.calls.length).toBe(1);
    expect(MOCKonCancel.mock.calls.length).toBe(1);
});
