import React from 'react';
import { mount }from 'enzyme';
import VariableManager from './VariableManager';
import {RangeVariable }from './VariableManager';

test('n buttons found in Variable Manager', ()=>{
    let varMan = mount(<VariableManager/>);
    let buttonCollection = varMan.find('button');
    expect(buttonCollection.length).toBe(4);
});

test('add a new variable', ()=>{
    let varMan = mount(<VariableManager/>);
    let inputCollection = varMan.find('input');
    expect(inputCollection.length).toBe(0);

    let addVarButton = varMan.find('button').first();
    addVarButton.simulate('click');

    inputCollection = varMan.find('input');
    expect(inputCollection.length).toBe(1);
});

test('deleteVariableDialogBox can render', ()=>{
    let varMan = mount(<VariableManager/>);
    // new variable is selected to enable delete variable button
    expect(varMan.instance().DialogBox.state.show).toBe(false);
    varMan.setState({selectedVariable: 'foo'});

    // delete variable button to open dialog box
    let deleteVarButton = varMan.find('button').at(1);
    deleteVarButton.simulate('click');

    // check that it renders the dialog box (2) and varMan (4) buttons
    let newButtons = varMan.find('button');
    expect(newButtons.length).toBe(6);
    
    expect(varMan.instance().DialogBox.state.show).toBe(true);
});

describe('test replaceVariableProperties', ()=>{
    let propertyArr;
    beforeEach(()=>{
        propertyArr = [
            {
                name: 'foo',
                defaultValue: '3',
                value: '${fooVar}'
            },
            {
                name: 'bar',
                defaultValue: '2',
                value: '${fooVar}'
            }
        ];
    });

    test('replace all instances of variable with new value', ()=>{
        let varMan = mount(<VariableManager/>);
        varMan.setState({
            replaceVarValue: 10, 
            selectedVariable: 'fooVar'
        });

        varMan.instance().replaceVariableProperties(propertyArr);
        propertyArr.forEach((property)=>{
            expect(property.value).toBe(10);
        });
    });

    test('replace all instances with default values', ()=>{
        let varMan = mount(<VariableManager/>);
        varMan.setState({
            replaceVarValue: null, 
            selectedVariable: 'fooVar'
        });

        varMan.instance().replaceVariableProperties(propertyArr);
        propertyArr.forEach((property)=>{
            expect(property.value).toBe(property.defaultValue);
        });
    });
});

const VarData = {$:{low:'', high:'', type:''}};

test('RangeVariable can handle change', ()=>{
    let returnValue = null;
    const mockUpdateDefault = jest.fn();
    const MOCK = jest.fn((data)=>{
        returnValue = data;
    });
    const vari = mount(<RangeVariable data = {VarData} returnValue={MOCK}
        updateDefault={mockUpdateDefault}/>);
    const inputCollection = vari.find('input');
    inputCollection.forEach((input)=>{
        input.simulate('change', {target:{value:'FooBar'}});
    });

    const selectInput = vari.find('select');
    selectInput.at(0).simulate('change', {target:{value:'FooBar3'}});

    expect(returnValue).toEqual({$:{low:'FooBar', high:'FooBar', type:'FooBar3'}});
});
