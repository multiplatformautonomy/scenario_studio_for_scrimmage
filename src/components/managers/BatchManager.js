import React from 'react';
import propTypes from 'prop-types';
import './BatchManager.css';
import Button from '../Button';

export default class Batch extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            numRuns:'',
            numProcessors:'',
            experimentName:''
        };
    }

    createCommand(){
        let command = 'python3 /home/scrimmage/scrimmage/scripts/run_experiments.py -l /experiments';
        if(this.state.numRuns){
            command += ' -t ' + this.state.numRuns;
        }
        command += ' -m /home/scrimmage/scrimmage/missions/MASS_mission.xml';
        if(this.state.numProcessors){
            command += ' -p ' + this.state.numProcessors;
        }
        command += ' -r /home/scrimmage/scrimmage/config/ranges/batch-ranges.xml';
        if(this.state.experimentName){
            command += ' -n ' + this.state.experimentName;
        }
        return command;
    }

    render(){
        return (<div className='batch_manager_container'>
            <label className='dialog-label-inline'>
                <div className='dialog-label-text'>
                    {'Number of Runs:'}
                </div>
                <input className='dialog-text-input'
                    type='text'
                    onChange={(event)=>this.setState({numRuns:event.target.value})}/>
            </label>
            <label className='dialog-label-inline'>
                <div className='dialog-label-text'>
                    {'Number of Processors:'}
                </div>
                <input className='dialog-text-input'
                    type='text'
                    onChange={(event)=>this.setState({numProcessors:event.target.value})}/>
            </label>
            <label className='dialog-label-inline'>
                <div className='dialog-label-text'>
                    {'Experiment\'s Name:'}
                </div>
                <input className='dialog-text-input'
                    type='text'
                    onChange={(event)=>this.setState({experimentName:event.target.value})}/>
            </label>
            <div className='dialog-button-container'>
                <Button className='dialog-button-blue' clickHandler={()=>this.props.onRun(this.createCommand())}
                >Run</Button>
                <Button className='dialog-button-red' clickHandler={this.props.onCancel}
                >Cancel</Button>
            </div>
        </div>);
    }
}

Batch.defaultProps = {
    onRun:()=>{},
    onCancel:()=>{}
};

Batch.propTypes = {
    onRun: propTypes.func,
    onCancel: propTypes.func
};
