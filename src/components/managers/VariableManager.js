import React, {useState, useEffect}from 'react';
import propTypes from 'prop-types';
import './VariableManager.css';
import Button from '../Button.js';
import VariableTracker from './VariableTracker';
import DialogBox from '../dialogs/DialogBox';

function StateInput (props){
    const [obj] = useState(props.obj);
    return <input 
        className = 'stateInput'
        readOnly
        key={props.name}
        value={props.name}
        obj = {obj}
        onFocus = {()=>props.onFocus()/* props.updateObject(obj) */ }
        style={props.selected ? {backgroundColor:'lightblue'} : {}}
    />;
}
StateInput.defaultProps = {onFocus:console.log};
StateInput.propTypes = {
    name: propTypes.string,
    obj: propTypes.object,
    onFocus: propTypes.func,
    selected: propTypes.bool
};

export function RangeVariable(props) {
    let [data, setData] = useState(Object.assign({$:{}}, props.data));
    let [def, setDef] = useState(props.default);
    useEffect(()=>{
        setDef(props.default);
    }, [props.default]);
    useEffect(() => {
        // works like getDerivedStateFromProps.
        setData({$:Object.assign({}, props.data.$)});
    }, [props.data]); // this function only executes on creation. Alternative to componentDidMount. 

    // early return logic
    let i;
    for(i in props.data.$){
        break;
    }
    if(!i){
        return null;
    }
    // displayForm will display the variables  with textboxes and change the vars state everytime user changes text fields.
    let displayForm = Object.entries(data.$).map(([key, value]) => {   
        function eventHandler(event){
            let temporary = Object.assign({}, data);
            temporary.$[key] = event.target.value;
            setData(temporary);
            props.returnValue(data);
        }
        return <label className='rangeLabel' key={key}>
            {key} 
            {key === 'type' && 
                <select
                    key = {key} 
                    value = {value}
                    onChange={eventHandler} 
                > 
                    <option value='int'>int</option>
                    <option value='float'>float</option>
                </select>
            }

            {key !== 'type' &&
                <span><input
                    key = {key} 
                    type='number'
                    value = {value}
                    onChange={eventHandler} 
                /></span>
            }
        </label>;
    });
    displayForm = [
        <label className='rangeLabel' key='def'>
            Default
            <span><input
                key = 'def' 
                type='number'
                value = {def}
                onChange={(event) => {
                    setDef(event.target.value);
                    props.updateDefault(event.target.value);
                }} 
            /></span>
        </label>, 
        ...displayForm
    ];
    return (<div className='rangeContainer' >
        <form>
            {displayForm}
        </form>
    </div>);
}

RangeVariable.defaultProps = {data: {}};

RangeVariable.propTypes = {
    data: propTypes.object,
    default:propTypes.oneOfType([propTypes.number, propTypes.string]),
    returnValue: propTypes.func,
    updateDefault: propTypes.func
};

export default class VariableManager extends React.Component{
    constructor(props){
        super(props);
        this.deleteVariableDialogBox = this.deleteVariableDialogBox.bind(this);
        this.DialogBox = React.createRef();
        this.varTrack = VariableTracker.getInstance();
        this.state = {
            selectedVariable: this.props.focus || null,
            replaceVarValue: null
        };
    }

    variableRender(){
        let variableRanges = this.varTrack.missionVariables.data.ranges;
        let temporary = Object.assign({}, variableRanges);
        let variableList = Object.keys(temporary);
        variableList.splice(variableList.indexOf('$'), 1);
        //Create list of selectable items
        variableList.forEach((item, index, array)=>{
            array[index] = <StateInput
                key={item}
                name={item} 
                obj={temporary[item]}
                selected = {this.state.selectedVariable === item}
                onFocus={()=>{
                    this.setState({selectedVariable:item});
                }}
            />;
        });
        return variableList;
    }

    deleteVariableDialogBox(){
        return <>
            <div style={{paddingBottom:'5px', textAlign:'center'}}>
                <p>You are about to delete the variable <b>{this.state.selectedVariable}</b></p>
                
                <p>Please set a value to replace all instances of this variable.</p>

                <p>If no value is set, then the variable will be set with its original default value.</p>
            </div>

            <div className='deleteVariableVertical'>
                <div className='deleteVariableText'>
                    <input
                        className='deleteInputText'
                        type='text'
                        onChange={(event) => this.setState({replaceVarValue:event.target.value})}
                    />
                </div>
                <div className='deleteVariableButtons'>
                    <Button className='dialog-button-red'
                        clickHandler={()=>{
                            this.deleteVariableInstances();
                        }}>
                        Delete Variable
                    </Button>
                    <Button className='dialog-button-blue'
                        clickHandler={()=> {
                            this.DialogBox.setShow(false);
                            this.setState({replaceVarValue:null});
                        }}>
                        Cancel
                    </Button>
                </div>
            </div>
        </>;
    }

    deleteVariableInstances() {
        this.varTrack.removeVariable(this.state.selectedVariable);
        
        for(const [, entity]of Object.entries(this.props.entityProperties)) {
            if(entity.enabledPlugins.length > 0) {
                this.replaceVariableProperties(entity.enabledPlugins);
            }
            if(entity.properties.length > 0) {
                this.replaceVariableProperties(entity.properties);
            }
        }

        this.props.updateAllMissionProperties(); // write to file and refresh
        
        this.setState({
            selectedVariable: null,
            replaceVarValue: null
        });
        this.DialogBox.setShow(false);
    }

    replaceVariableProperties(propertyArr){
        propertyArr.forEach((property)=>{
            if(property.value !== undefined && property.value !== null){
                if(property.value.toString().match(this.state.selectedVariable)){
                    (this.state.replaceVarValue) 
                        ? property.value = this.state.replaceVarValue 
                        : property.value = property.defaultValue;
                }
            }

            if(property.args){
                property.args.forEach((arg)=>{
                    if(arg.value.toString().match(this.state.selectedVariable)){
                        (this.state.replaceVarValue) 
                            ? arg.value = this.state.replaceVarValue 
                            : arg.value = arg.defaultValue;
                    }
                });
            }
        });
    }

    render(){
        return (
            <div className='managerContainer'>
                <div className='variableRootContainer'>
                    <div className='variableManagerContainer'>
                        <div className='variableContainer'>
                            {this.variableRender()}
                            {this.new && <input 
                                autoFocus
                                onChange={(event)=>{
                                    this.setState({newName:event.target.value});
                                }}
                                onBlur={(event)=>{
                                    if(event.target.value){
                                        this.varTrack.createAndAddVariable(event.target.value);
                                    }
                                    this.new = false;
                                    this.setState({selectedVariable:event.target.value});
                                }}
                                onKeyPress={(event)=>{
                                    if(event.key === 'Enter'){
                                        this.varTrack.createAndAddVariable(event.target.value);
                                        this.new = false;
                                        this.setState({selectedVariable:event.target.value});
                                    }
                                }}
                            />}
                        </div>
                        <div className='variableButtonContainer'>
                            <Button key='variableAddButton'
                                className='dialogButtonGreen'
                                clickHandler={()=>{
                                    this.new = true;
                                    this.forceUpdate();
                                }}
                            >+</Button>

                            {/** Remove var button that opens up dialogbox. Grayed out if no var selected */}
                            {!this.state.selectedVariable && 
                                <Button className='dialogButtonGray'
                                    isDisabled={!this.state.selectedVariable}
                                    clickHandler={()=>{
                                        this.DialogBox.setShow(true);
                                    }}
                                >-</Button>
                            }
                            {this.state.selectedVariable && 
                                <Button className='dialogButtonRed'
                                    clickHandler={()=>{
                                        this.DialogBox.setShow(true);
                                    }}
                                >-</Button>
                            }
                            
                        </div>
                    </div>

                    {this.state.selectedVariable && <RangeVariable
                        default = {this.varTrack.missionVariableDefaults[this.state.selectedVariable] || ''}
                        updateDefault = {(val)=>this.varTrack.saveVariableDefaults({[this.state.selectedVariable]:val})}
                        data = {this.varTrack.missionVariables.data.ranges[this.state.selectedVariable]}
                        returnValue={(data)=>this.varTrack.updateVariable(this.state.selectedVariable, data)}
                    />}

                    {/**Rendering the deleteVariableDialogBox */}
                    <DialogBox ref={(node)=>{
                        this.DialogBox = node;
                    }}
                    key={this.state.dialogCloseable}
                    closeable = {this.state.dialogCloseable}
                    title={'Delete Variable'}>  
                        {this.deleteVariableDialogBox()}
                    </DialogBox>
                </div>

                <div className='dialogButtonContainer'>
                    {!this.state.selectedVariable && 
                        <Button isDisabled={!this.state.selectedVariable}
                            className='dialogButtonGray'
                        >
                            Select Variable {this.state.selectedVariable}
                        </Button>
                    }
                    {this.state.selectedVariable &&
                        <Button
                            className='dialogButtonGreen' 
                            clickHandler={()=>{
                                this.props.elevateVariable(this.state.selectedVariable);
                                this.props.onclose(`\${${this.state.selectedVariable}}`);
                            }}
                        >
                            Select Variable {this.state.selectedVariable}
                        </Button>
                    }   

                    <Button className='dialogButtonRed' clickHandler={this.props.closeManager}>
                        Cancel
                    </Button>
                </div>
            </div>
        );
    }
}

VariableManager.defaultProps = {
    elevateVariable: ()=>{},
    onclose: ()=>{}
};

VariableManager.propTypes = {
    focus: propTypes.string,
    elevateVariable: propTypes.func,
    onclose: propTypes.func,
    closeManager: propTypes.func,
    updateAllMissionProperties: propTypes.func,
    entityProperties: propTypes.object
};
