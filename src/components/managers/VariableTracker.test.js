import *as readModule from '../../utilities/parseXML';
import VariableTracker from './VariableTracker';

jest.mock('../../utilities/parseXML');

test('initialize variable tracker from file path', ()=>{
    let varTrack = VariableTracker.getInstance();
    const mockInitializeDefaults = jest.fn(()=>{});
    const mockInitializeRanges = jest.fn(()=>{});

    function readParse(obj) {
        obj.readParseFile = jest.fn().mockResolvedValue();
    }
    readParse(readModule);
    jest.mock('./VariableTracker', () => {
        jest.fn().mockImplementation(() => ({initializeVariableDefaults: mockInitializeDefaults}));
    });
    jest.mock('./VariableTracker', () => {
        jest.fn().mockImplementation(() => ({initializeVariableRanges: mockInitializeRanges}));
    });
    expect(varTrack.initializeVariables('fakePath')).toBe(undefined);
});

test('get electron does not return undefined', ()=>{
    let varTrack = VariableTracker.getInstance();
    expect(varTrack.getElectron()).not.toBe(undefined);
});

test('saveVariableDefaults calls getElectron to writeFile', ()=>{
    let varTrack = VariableTracker.getInstance();
    const writeFileSpy = jest.spyOn(varTrack, 'getElectron');
    varTrack.saveVariableDefaults({foo:'bar'});
    expect(writeFileSpy).toHaveBeenCalledTimes(1);
});

test('updateVariable', ()=>{
    let varTrack = VariableTracker.getInstance();
    varTrack.updateVariable('foo', {low:1, high:5});
    expect(varTrack.missionVariables.data.ranges['foo']).toStrictEqual({low:1, high:5});
});

test('removeVariable', ()=>{
    let varTrack = VariableTracker.getInstance();
    
    const writeFileSpy = jest.spyOn(varTrack, 'getElectron');

    varTrack.updateVariable('foo', {low:1, high:5});
    varTrack.removeVariable('foo');
    expect(varTrack.missionVariables.data.ranges.length).toBe(undefined);
    // called in update and remove
    expect(writeFileSpy).toHaveBeenCalledTimes(2);
});

test('createAndAddVariable', ()=>{
    let varTrack = VariableTracker.getInstance();
    expect(varTrack.missionVariables.data.ranges.length).toBe(undefined);

    varTrack.createAndAddVariable('foo');
    varTrack.createAndAddVariable('bar');
    expect(varTrack.missionVariables.data.ranges['foo']).toStrictEqual({$:{low:'', high:'', type:'int'}});
});
