import {readParseFile, saveJSToXml}from '../../utilities/parseXML';

export default class VariableTracker {
    constructor(){
        this.missionVariables = JSON.parse(JSON.stringify(require('../../assets/RangesXmlTemplate').default));
        this.missionVariableDefaults = {};
        this.xmlFilePath = undefined;
    }

    saveVariableDefaults(obj){
        Object.assign(this.missionVariableDefaults, obj);
        
        this.getElectron().require('fs').writeFile(
            this.xmlFilePath + '/defaults.json',
            JSON.stringify(this.missionVariableDefaults),
            console.error
        );
    }

    updateVariable(name, obj){
        this.missionVariables.data.ranges[name] = obj;
        saveJSToXml(this.missionVariables.fileName, this.missionVariables.data);
    }

    removeVariable(name){
        if(name === null){
            return;
        }
        // remove and update default
        delete this.missionVariableDefaults[name];
        this.getElectron().require('fs').writeFile(
            this.xmlFilePath + '/defaults.json',
            JSON.stringify(this.missionVariableDefaults),
            console.error
        );

        // remove and update ranges
        delete this.missionVariables.data.ranges[name];
        saveJSToXml(this.missionVariables.fileName, this.missionVariables.data);
    }

    createAndAddVariable(name){
        if(!name || this.missionVariables.data.ranges[name]){
            return;
        }
        let rangeDefaults = {$:{low:'', high:'', type:'int'}};
        this.missionVariables.data.ranges[name] = rangeDefaults;
        saveJSToXml(this.missionVariables.fileName, this.missionVariables.data);
    }

    getElectron() {
        return window.require('electron').remote;
    }
    
    initializeVariableRanges(){
        let xmlFilePath = this.xmlFilePath.concat('/ranges.xml');
        // parse-stringify used to create a new copy of the template. This prevents bindings that edit the template.
        this.missionVariables = JSON.parse(JSON.stringify(require('../../assets/RangesXmlTemplate').default));
        this.missionVariables.fileName = xmlFilePath;

        readParseFile(xmlFilePath).then((xml) =>{
            for(let key of Object.keys(xml.data.ranges)){
                // This loop works off the assumption that all entries are unique and there will only ever be one instance
                // of each xml element. This will stream line the object and make it eaiser to access the code in 
                // MissionEntity.js, which works off of this assumption. This new data structure can still be converted 
                // to XML with no further intervention
                if(Array.isArray(xml.data.ranges[key])){
                    xml.data.ranges[key] = xml.data.ranges[key][0];
                }
            }
            this.missionVariables.data = xml.data;
        }).catch((bad)=>{
            console.warn(bad, 'Unable to open ' + xmlFilePath + '\nCreating new file from default');
        });
    }

    initializeVariableDefaults(){
        let fs = this.getElectron().require('fs');
        let path = this.xmlFilePath + '/defaults.json';
        let varDef;
        if(fs.existsSync(path)){
            varDef = JSON.parse(fs.readFileSync(path).toString());
        }else {
            varDef = {};
        }
        this.missionVariableDefaults = varDef;
    }

    initializeVariables(xmlFilePath){
        this.xmlFilePath = xmlFilePath;
        this.initializeVariableRanges();
        this.initializeVariableDefaults();
    }

    static getInstance(){
        // eslint-disable-next-line eqeqeq
        if(this.instance == null){
            this.instance = new VariableTracker();
            return this.instance;
        }else { 
            return this.instance; 
        }
    }

    static setInstance(newTracker){
        this.instance = newTracker;
    }
}
