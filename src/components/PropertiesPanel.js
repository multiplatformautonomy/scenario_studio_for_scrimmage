import React, {createRef}from 'react';
import propTypes from 'prop-types';
import './PropertiesPanel.css';
import Button from './Button';
import VariableManager from './managers/VariableManager';
import DialogBox from './dialogs/DialogBox';
import TextBox from './properties/TextBox';

//FontAwesome Dependencies and Setup
import {dom, library }from '@fortawesome/fontawesome-svg-core';
import {faPaperPlane, faMicrochip }from '@fortawesome/free-solid-svg-icons';
library.add(faPaperPlane, faMicrochip);
dom.watch();
//----------------------------------

export default class PropertiesPanel extends React.Component {
    constructor(props){
        super(props);
        this.state = { varManagerKey:'' };
        this.varManager = createRef();
    }

    // renders data and inputs in the properties panel
    renderPropertiesData(){
        let data = this.props.card.state.propertiesData;
        let renderList = [];

        let cardProperties = data.properties.concat(data.enabledPlugins);
        cardProperties.forEach((prompt) =>{
            renderList.push(
                // generalize the input type (text box, select, etc.) that is rendered
                <prompt.type
                    key={prompt.key}
                    argKey={prompt.key}
                    className={'propertyContainer'}
                    name={prompt.name} 
                    pluginType={prompt.pluginType}
                    value={prompt.value}
                    options={prompt.options}
                    args={prompt.args}
                    updatePropertiesData={this.props.updatePropertiesData}
                    displayVariables={(updateFunc, focus)=>{
                        this.setState({varManagerKey:focus});
                        this.varManager.current.setShow(true);
                        this.setState({updateProperty:updateFunc});
                    }}
                    updateDefaults={this.props.updateDefaults}
                    openCodeEditor={this.props.openCodeEditor}
                    xmlFilePath={this.props.xmlFilePath}
                />);
        });
        return renderList;
    }

    renderPluginButtons(){
        if(this.props.card.type() === 'Entity') {
            return [
                <Button key='autonomy' className='plugin_bar' 
                    clickHandler={()=>this.props.openPluginMenu('autonomy')} faIcon={'fas fa-microchip fa-2x'}>
                    <div className='bar_text'>Select Autonomy</div>
                </Button>,
                <Button key='controller' className='plugin_bar' 
                    clickHandler={()=>this.props.openPluginMenu('controller')} faIcon={'fas fa-microchip fa-2x'}>
                    <div className='bar_text'>Select Controller</div>
                </Button>,
                <Button key='motion' className='plugin_bar' 
                    clickHandler={()=>this.props.openPluginMenu('motion')} faIcon={'fas fa-microchip fa-2x'}>
                    <div className='bar_text'>Select Motion</div>
                </Button>,
                <Button key='sensor' className='plugin_bar' 
                    clickHandler={()=>this.props.openPluginMenu('sensor')} faIcon={'fas fa-microchip fa-2x'}>
                    <div className='bar_text'>Select Sensor</div>
                </Button> 
            ];
        }else if(this.props.card.type() === 'MissionEntity') {
            return [
                <Button key='interaction' className='plugin_bar' 
                    clickHandler={()=>this.props.openPluginMenu('interaction')} faIcon={'fas fa-microchip fa-2x'}>
                    <div className='bar_text'>Select Interaction</div>
                </Button>,
                <Button key='metrics' className='plugin_bar' 
                    clickHandler={()=>this.props.openPluginMenu('metrics')} faIcon={'fas fa-microchip fa-2x'}>
                    <div className='bar_text'>Select Metric</div>
                </Button>,
                <Button key='network' className='plugin_bar' 
                    clickHandler={()=>this.props.openPluginMenu('network')} faIcon={'fas fa-microchip fa-2x'}>
                    <div className='bar_text'>Select Network</div>
                </Button> 
            ];
        }
    }

    render(){
        return (
            <div className='panel_container'>
                <DialogBox
                    ref={this.varManager}
                    title="Mission Variables"
                >
                    <VariableManager
                        key={this.state.varManagerKey}
                        entityProperties={this.props.entityProperties}
                        updateAllMissionProperties={()=>this.props.updateAllMissionProperties()}
                        focus={this.state.varManagerKey}
                        closeManager={()=>this.varManager.current.setShow(false)}
                        elevateVariable = {()=>{
                            this.forceUpdate();
                        }}
                        onclose = {(value)=>{
                            this.varManager.current.setShow(false);
                            this.state.updateProperty(value);
                        }}
                    />
                </DialogBox>

                <div className='entity_name_bar'>
                    <i className='fas fa-paper-plane fa-3x' />
                    <TextBox
                        className={'missionContainer'}
                        key={this.props.card.state.name}
                        name={null}
                        value={this.props.card.state.name}
                        updatePropertiesData={this.props.updateName}
                    />
                </div>
                
                <div className='properties_bar'>
                    {this.renderPropertiesData()}
                </div>
                {this.renderPluginButtons()}
            </div>
        );
    }
}

PropertiesPanel.propTypes = {
    'card': propTypes.object,
    'updatePropertiesData': propTypes.func.isRequired,
    'updateName': propTypes.func.isRequired,
    'openPluginMenu':propTypes.func.isRequired,
    'openCodeEditor' :propTypes.func,
    'xmlFilePath': propTypes.string,
    'entityProperties': propTypes.object,
    'updateAllMissionProperties': propTypes.func,
    'updateDefaults': propTypes.func
};
