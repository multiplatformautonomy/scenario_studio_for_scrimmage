import React from 'react';
import { shallow, mount }from 'enzyme';
import PropertiesPanel from './PropertiesPanel';
import EntityProperties from '../assets/EntityTemplate';
import Entity from './card/Entity';
import MissionEntity from './card/MissionEntity';
import {VariableInputBox}from './properties/TextBox';
import BaseProperty from './properties/BaseProperty';
// jest.mock('./properties/TextBox.js');
let defaultProperties = new EntityProperties();
let simpleCard = new Entity({name:'simpleCard', clickHandler:()=>{}});

function openMenu(data) {
    // console.log(data); // Uncomment this line for debugging
    data;
}

test('number of <TextBox /> components', ()=>{
    // should equal 1 for MissionEntity name. All other text components are VariableInput.
    const panel = shallow(<PropertiesPanel clickHandler={()=>{}} card={simpleCard} 
        propertiesData={defaultProperties} updatePropertiesData={()=>{}}/>);

    expect(panel.find('TextBox')).toHaveLength(1);
});

test('number of <SelectBox /> components', ()=>{
    const panel = shallow(<PropertiesPanel clickHandler={()=>{}} card={simpleCard} 
        propertiesData={defaultProperties} updatePropertiesData={()=>{}}/>);

    expect(panel.find('SelectBox')).toHaveLength(2);
});

// Simulating clicks for rendered buttons
test('click handler', () => {
    let Card = new MissionEntity({name:'simpleCard', clickHandler:()=>{}});
    Card.state.propertiesData = {
        properties:[],
        enabledPlugins:[]
    };
    const mock = jest.fn(()=>{});
    let panel = mount(<PropertiesPanel card={simpleCard} openPluginMenu={mock} 
        propertiesData={defaultProperties} updatePropertiesData={()=>{}}/>);

    panel.instance().renderPluginButtons();
    panel.find('button').forEach((button)=>{
        button.simulate('click');
    });
    expect(mock.mock.calls.length).toBe(4);
    panel.unmount();
    
    panel = mount(<PropertiesPanel card={Card} openPluginMenu={mock} 
        propertiesData={defaultProperties} updatePropertiesData={()=>{}}/>);
    panel.instance().renderPluginButtons();
    panel.find('button').forEach((button)=>{
        button.simulate('click');
    });
    expect(mock.mock.calls.length).toBe(7);
    panel.unmount();
});

test('mission entity without name prop', () => {
    let Card = new MissionEntity({name:'simpleCard', clickHandler:()=>{}});
    
    const panel = mount(<PropertiesPanel card={Card} openPluginMenu={openMenu} 
        propertiesData={defaultProperties} xmlFilePath={'screens/LiveMission.test.xml'} updatePropertiesData={()=>{}}/>);
    panel.props = {card: {constructor: {name: 'hello'}}};

    panel.props.card.constructor.name = 'MissionEntity';
    panel.instance().renderPluginButtons();
    expect(panel.find('button')).toHaveLength(19); // All VariableInput components have buttons.
});

describe('Mission Variables', ()=>{
    const entity = {state:{}, type:()=>'Entity'};
    entity.state.propertiesData = {
        enabledPlugins:[],
        properties:[
            {
                name: 'FOOBAR',
                type: BaseProperty,
                pluginType: 'autonomy', 
                args: [{name:'Foo', type: VariableInputBox, value:'Bar'}] 
            }
        ]
    };
    const panel =  mount(<PropertiesPanel card={entity} xmlFilePath={''} openPluginMenu={openMenu}
        propertiesData={defaultProperties} updatePropertiesData={()=>{}} updateName={()=>{}}
        missionVariables={{data:{ranges:{}}}}/>);
    
    test('a button is present to edit FooBar\'s variable', ()=>{
        expect(panel.find('VariableInputBox').find('button').length).toBe(1);
    });

    test('open Variable menu', ()=>{
        const button = panel.find('VariableInputBox').find('button');
        expect(panel.find('VariableManager').length).toBe(0);
        button.simulate('click');
        expect(panel.find('VariableManager').length).toBe(1);
    });
});
