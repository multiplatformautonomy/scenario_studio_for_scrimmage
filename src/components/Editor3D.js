import React from 'react';
import PropTypes from 'prop-types';
import { Viewer, Entity, Label, LabelCollection, CameraFlyTo, ScreenSpaceEventHandler, ScreenSpaceEvent }from 'resium';
import {
    Cartographic, Cartesian3, Color, ScreenSpaceEventType, HeadingPitchRoll, 
    Transforms, Math, HorizontalOrigin, NearFarScalar, Matrix4, Ellipsoid, Ion
}from 'cesium';
import 'cesium/Build/Cesium/Widgets/widgets.css';
import bluePlane from '../assets/wing-blue.glb';
import redPlane from '../assets/wing-red.glb';
import car from '../assets/volkswagen.glb';
import angler from '../assets/angler.glb';
window.CESIUM_BASE_URL = 'Cesium';
// eslint-disable-next-line max-len
Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmOTJmZjdmZS03MWI4LTQwYzEtOTVmOS01MDY3OWQ5MzdhZjYiLCJpZCI6Mzg4MzIsImlhdCI6MTYwNjg3MzU1MH0.dNwJ1hmwmwUyurjaA4pc3chpk9xf-08UksHoJh5BDbU';

class Editor3D extends React.Component {
    constructor(props) {
        super(props);
    }

    parseCoordinates(coordinate){
        if(Array.isArray(coordinate)) {
            if(coordinate[0][0] === '-' && coordinate[0].length > 1) {
                return coordinate[0];
            }else if(coordinate[0].length > 0 && /^\d+$/.test(coordinate[0])) {
                return coordinate[0];
            }else {
                return 0;
            }
        }else {
            if(coordinate[0] === '-' && coordinate.length > 1) {
                return coordinate;
            }else if(coordinate.length > 0 && /^\d+$/.test(coordinate)) {
                return coordinate;
            }else {
                return 0;
            }
        }
    }

    setOrigin() {
        let longitude = Number(this.props.missionSettings.props.loadedProperties.longitude_origin);
        let latitude = Number(this.props.missionSettings.props.loadedProperties.latitude_origin);
        let altitude = Number(this.props.missionSettings.props.loadedProperties.altitude_origin);
        altitude = altitude ? altitude : 300;
        longitude = longitude ? longitude : -120.767925;
        latitude = latitude ? latitude : 35.721025;
        let position = new Cartographic(longitude * (Math.PI / 180), latitude * (Math.PI / 180), altitude);
        let cartesianPosition = Cartographic.toCartesian(position);
        return cartesianPosition;
    }

    entityPosition() {
        let long = Number(this.props.missionSettings.props.loadedProperties.longitude_origin);
        let lat = Number(this.props.missionSettings.props.loadedProperties.latitude_origin);
        long = long ? long : -120.767925;
        lat = lat ? lat : 35.721025;
        let entityElements = [];
        let entityProperties = this.props.entityList;
        Object.keys(entityProperties).forEach((key)=>{
            if(key !== 'mission-settings'){                
                let model, maxScale;
                let entity = entityProperties[key].properties[10].value;
                if(entity[0] === 'zephyr-blue' || entity === 'zephyr-blue') {
                    model = bluePlane;
                    maxScale = 0.002;
                }else if(entity[0] === 'zephyr-red' || entity === 'zephyr-red'){
                    model = redPlane;
                    maxScale = 0.002;
                }else if(entity[0] === 'volkswagen' || entity === 'volkswagen'){
                    model = car;
                    maxScale = 0.5;
                }else {
                    model = angler;
                    maxScale = 0.5;
                }
                var headingDegrees = Number(this.parseCoordinates(entityProperties[key].properties[8].value));

                var ENU = new Matrix4();
                var position = Cartesian3.fromDegrees(long, lat, 100);
                Transforms.eastNorthUpToFixedFrame(position, Ellipsoid.WGS84, ENU);

                let xDeg = Number(this.parseCoordinates(entityProperties[key].properties[5].value));
                let yDeg = Number(this.parseCoordinates(entityProperties[key].properties[6].value));
                let zDeg = Number(this.parseCoordinates(entityProperties[key].properties[7].value));
                var offset = new Cartesian3(xDeg, yDeg, zDeg);
                var finalPos = Matrix4.multiplyByPoint(ENU, offset, new Cartesian3());

                let labelOffset = Cartesian3.fromArray([0, 2, 5]);
                let nearFarScale = new NearFarScalar(0, .9, 300, 0);
                if(model === angler) {
                    headingDegrees -= 90;
                }else {
                    headingDegrees += 90;
                }
                var heading = Math.toRadians(headingDegrees);
                var hpr = new HeadingPitchRoll(heading, 0, 0);
                var orientation = Transforms.headingPitchRollQuaternion(
                    finalPos,
                    hpr
                );
                entityElements.push(
                    <>
                        <Entity
                            model={{
                                uri: model,
                                minimumPixelSize: 1,
                                maximumScale: maxScale
                            }}
                            key={key}
                            id={key}
                            name={entityProperties[key].name}
                            position={finalPos}
                            orientation={orientation}
                        />
                        <Label
                            fillColor={Color.ORANGE}
                            scaleByDistance={nearFarScale}
                            position={finalPos}
                            eyeOffset={labelOffset}
                            horizontalOrigin={HorizontalOrigin.CENTER}
                            text={entityProperties[key].name}
                        />
                    </>
                );
            }
        });
        return entityElements;
    }

    openPropertiesPanel() {
        if(this.viewer.cesiumElement.selectedEntity) {
            let id = this.viewer.cesiumElement.selectedEntity.id;
            let entityList = this.props.entityComponents;
            for(let i = 0; i < entityList.length ; i++){
                if(entityList[i].key === id) {
                    let element = document.getElementsByClassName('card_component')[i + 1];
                    element.click();
                    break;
                }
            }
        }else {
            console.log(this.props.missionSettings);
            let element = document.getElementsByClassName('cesium-viewer')[0];
            if(element) {
                let mission = document.getElementsByClassName('card_component')[0];
                mission.click();
            } 
            this.props.initializeMission();
            return;
        }     
    }
    render() {
        if(this.props.missionSettings !== null) {
            return (
                <Viewer ref={(err)=>{
                    this.viewer = err;
                }}
                className={'editor_viewer'}
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 270,
                    right: 350,
                    bottom: 0
                }}
                animation={false}
                timeline={false}
                >   
                    <ScreenSpaceEventHandler>
                        <ScreenSpaceEvent action={()=>{
                            this.openPropertiesPanel();
                        }} type={ScreenSpaceEventType.LEFT_CLICK} />
                    </ScreenSpaceEventHandler>
                    <CameraFlyTo duration={1} destination={this.setOrigin()} />
                    <LabelCollection>
                        {this.entityPosition()}
                    </LabelCollection>
                </Viewer>
            );
        }
        return null;        
    }
}

Editor3D.propTypes = {
    'entityList' : PropTypes.object,
    'entityComponents' : PropTypes.array,
    'missionSettings' : PropTypes.object,
    'card' : PropTypes.object,
    'initializeMission' : PropTypes.func
};
export default Editor3D;
