import React from 'react';
import './Button.css';
import './Mask.css';
import propTypes from 'prop-types';

export default class Mask extends React.Component{
    constructor(props){
        super(props);
        this.state = {isOpen:false};

        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mouseup', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mouseup', this.handleOutsideClick);
    }

    toggleOpen(isOpen){
        this.setState({isOpen:isOpen});
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleOutsideClick(event) {
        if(this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({isOpen:false });
        }
    }

    render(){
        if(this.state.isOpen){
            return (
                <div>
                    <div className='horizontalMask'>
                        {this.props.default}
                        <div className='maskDiv' ref={this.setWrapperRef}>
                            <div className='maskItems'> {this.props.children} </div> 
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div onClick={()=>this.toggleOpen(!this.state.isOpen)}>
                {this.props.default}
            </div>
        );
    }
}

Mask.propTypes = {
    default: propTypes.element,
    children: propTypes.oneOfType([propTypes.element, propTypes.string, propTypes.array ]),
    onHover: propTypes.oneOfType([propTypes.element, propTypes.string, propTypes.array ])
};
