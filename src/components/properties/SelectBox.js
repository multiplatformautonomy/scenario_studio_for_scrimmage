import React from 'react';
import PropTypes from 'prop-types';
import BaseProperty from './BaseProperty';

export class SelectBox extends BaseProperty {
    renderOptions(){
        let optionList = [];
        this.props.options.forEach((choice, i) => {
            optionList.push(<option key={i}>{choice}</option>);
        });
        return optionList;
    }

    inputElement(){
        let valueProp = null;
        if(this.props.value === undefined){
            return;
        }
        if(this.props.value.length > 1) {
            valueProp = this.props.value;
        }else {
            valueProp = this.props.value[0];
        }
        return (
            <select 
                className='selectBox' 
                value={valueProp} 
                onChange={(event) => this.changeHandler(event)}
                multiple={false}
            >
                {this.renderOptions()}
            </select>
        );
    }

    render() {
        return super.render();
    }
}

SelectBox.propTypes = {
    'updatePropertiesData': PropTypes.func,
    'value': PropTypes.oneOfType([PropTypes.string, PropTypes.array ]),
    'name': PropTypes.string,
    'options': PropTypes.array
};

export default SelectBox;
