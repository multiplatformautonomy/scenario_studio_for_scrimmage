import React from 'react';
import BaseProperty from './BaseProperty';
import {ArgumentList}from './BaseProperty';
import { shallow, mount }from 'enzyme';
import {TextBox, VariableInputBox }from './TextBox';
import CheckBox from './CheckBox';
import SelectBox from './SelectBox';

test('base property renders a div with prop name', ()=>{
    const baseProp = shallow(<BaseProperty changeHandler={()=>{}} name="TestingName"/>);
    expect(baseProp.text()).toContain('TestingName');
});

test('base property renders one propertyContainer class', () => {
    const baseProp = shallow(<BaseProperty changeHandler={()=>{}} name="TestingName" className={'propertyContainer'}/>);
    expect(baseProp.findWhere((ent) => ent.hasClass('propertyContainer'))).toHaveLength(1);
});


test('check number of properties inside of a property group', ()=>{ 
    const args = [
        {
            name:'start',
            type:TextBox,
            value:0.0
        },
        {
            name:'end',
            type:TextBox,
            value:100
        }
    ];
    const baseProp = mount(
        <BaseProperty changeHandler={()=>{}}
            name="TestingName"
            args={args}
            className={'propertyContainer'}
            xmlFilePath=''
            pluginType='motion'
        />
    );
    expect(baseProp.findWhere((node) => node.hasClass('propertyContainer'))).toHaveLength(5);
});

test('check TextBoxes have default values', ()=>{
    const panel = shallow(<BaseProperty changeHandler={()=>{}} name="TestingName" className={'propertyContainer'}/>);
    const collection = panel.findWhere((node) => node.name() === 'TextBox');
    collection.forEach((node)=>{
        expect(node.name()).toBe('TextBox');
        expect(node.prop('value')).toBeDefined();
    });
});

test('baseProperty renders inputElement', ()=> {
    class PropertyTest extends BaseProperty {
        inputElement() {
            return <span className="asdf">Hello World!</span>;
        }
    }
    const baseProp = shallow(<PropertyTest changeHandler={()=>{}} name="TestingName"/>);
    expect(baseProp.text()).toContain('TestingName');
    expect(baseProp.find('span.asdf')).toHaveLength(1);
    expect(baseProp.text()).toContain('Hello World!');
});

test('change handler', () => {
    function updateData(data) {
        console.log(data);
    }

    const property = shallow(<BaseProperty value='hello' updatePropertiesData={updateData} />);

    const ent = {target: {value: 'hello'}};
    property.instance().changeHandler(ent);

    expect(property.state('value')).toBe('hello');
});

test('check box handler', () => {
    const check = mount(<CheckBox updatePropertiesData = {() => {}}/>);
    check.setState({value: ['hello', 'world']});
    check.instance().inputElement();
    check.find('input').simulate('change');
    expect(check.find('input')).toHaveLength(1);
    check.setState({value: ['foo']});
    check.instance().inputElement();
});

test('text box handler', () => {
    const check = mount(<TextBox updatePropertiesData = {() => {}}/>);
    check.instance().inputElement();
    check.find('input').simulate('focus');
    check.find('input').simulate('change', {target:{value:'${asdf=2}'}});
    check.find('input').simulate('blur');
    expect(check.find('input')).toHaveLength(1);
});

test('Select box handler', () => {
    let obj = {value: {length: 5}};
    const check = mount(<SelectBox updatePropertiesData = {() => {}} options={[]} value={obj}/>);
    check.instance().inputElement();
    check.find('select').simulate('change');
    expect(check.find('select')).toHaveLength(1);
});

describe('test VariableInputBox', ()=>{
    const BUTTONMOCK = jest.fn(()=>{});
    const CHANGEMOCK = jest.fn(()=>{});
    const varInput = mount(
        <VariableInputBox
            updatePropertiesData={CHANGEMOCK} 
            value='Foo'
            displayVariables={BUTTONMOCK}
        />
    );

    test('display variable button with no variable set', ()=>{
        expect(BUTTONMOCK.mock.calls.length).toBe(0);
        varInput.find('button').simulate('click');
        expect(BUTTONMOCK.mock.calls.length).toBe(1);
        BUTTONMOCK.mockClear();
    });
    
    test('changeHandler called when updating value', ()=>{
        expect(varInput.instance().props.value).toBe('Foo');
        varInput.instance().changeHandler('Bar');
        // value is passed in but not kept in VarInput component
        expect(CHANGEMOCK.mock.calls.length).toBe(1);
        CHANGEMOCK.mockClear();
    });

    test('display variables buttons with variable set', ()=>{
        expect(BUTTONMOCK.mock.calls.length).toBe(0);
        varInput.find('button').first().simulate('click');
        expect(BUTTONMOCK.mock.calls.length).toBe(1);
        BUTTONMOCK.mockClear();
    });

    test('clear button with variables set calls changeHandler', ()=>{
        const CHANGEMOCK2 = jest.fn(()=>{});
        const varInput2 = mount(
            <VariableInputBox
                updatePropertiesData={CHANGEMOCK2} 
                value='${FOO = 123}'
                displayVariables={()=>{}}
            />
        );

        expect(varInput.find('button')).toHaveLength(1);
        expect(varInput2.find('button')).toHaveLength(2);
        varInput2.find('button').last().simulate('click');
        // deleting a variable instance calls changeHandler w/ value = ''
        expect(CHANGEMOCK2.mock.calls.length).toBe(1); 
        CHANGEMOCK2.mockClear();
    });
});

describe('Argument list test', () => {
    const args = [
        {
            name:'start',
            type:TextBox,
            value:0.0
        },
        {
            name:'end',
            type:TextBox,
            value:100
        }
    ];

    test('component renders', () => {
        const list  = shallow(<ArgumentList properties={{args:args, name: 'plugname', pluginType: 'motion', xmlFilePath:''}}
        />);
        expect(list.findWhere((node) => node.hasClass('headerBar'))).toHaveLength(1);
        expect(list.state('collapse')).toBe(true);

        
        list.unmount();
    });

    test('isCustomPlugins  test', () => {
        const list  = mount(<ArgumentList properties={{args:args, name: 'plugname', pluginType: 'motion', xmlFilePath:''}} />); 

        const mockExistsSync = jest.fn(()=>{
            true;
        });
        const mockGetPath = {normalize:()=>{}};

        const mockGetElectron = jest.fn(()=>({
            app: {getAppPath: ()=>{}},
            require: ()=>({existsSync: mockExistsSync, getPath: mockGetPath})
        }));
        

        list.instance().getElectron = mockGetElectron;
        list.instance().getPath = mockGetPath;



        
        try {
            expect(list.instance().isCustomPlugin('name', 'motion_model')).toBe(true);
            expect(mockExistsSync).toBeCalled();
        }catch (except) {
            console.error(except);
        }
        

        list.unmount();
    });
    test('editPlugin  test', () => {
        const mockOpenCodeEditor = (jest.fn(()=>{}));
    
        const list  = mount(<ArgumentList properties={{
            args:args,
            name: 'plugname',
            openCodeEditor:{mockOpenCodeEditor},
            pluginType: 'motion',
            xmlFilePath:''
        }}
        />); 




        try {
            list.instance().editPlugin();
        }catch (except){
            expect(except.message).toBe('Plugin name and type cannot be undefined');
        }
            

        try {
            expect(list.instance().editPlugin('name', 'motion_model')).toBe(true);
            expect(mockOpenCodeEditor).toBeCalled();
        }catch (err){
            console.error(err);
        }
    });
    test('Argument collapse state check', () =>{
        let mockOpenCodeEditor = jest.fn(()=>{});
        let mockGetPath = {normalize:()=>{}};
        const list  = mount(<ArgumentList properties={{
            args:args,
            name: 'plugname',
            openCodeEditor:{mockOpenCodeEditor},
            pluginType: 'motion',
            xmlFilePath:''
        }}/>); 
        list.instance().getPath = ()=>mockGetPath;

        list.find('button').at(0).simulate('click');

        expect(list.state('collapse')).toBe(false);
    });
});
