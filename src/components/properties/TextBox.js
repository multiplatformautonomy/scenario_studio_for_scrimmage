import React from 'react';
import PropTypes from 'prop-types';
import './Properties.css';
import BaseProperty from './BaseProperty';
export class TextBox extends BaseProperty {
    constructor(props){
        super(props);
        this.state.value = this.props.value;
        this.state.keyData = this.props.keyData || this.props.argKey; 
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(value){
        this.props.updatePropertiesData(this.state.keyData, value);
    }

    inputElement(){
        return (
            <input
                className='textBox'
                type='text'
                value={this.state.value}
                onChange={(event) => this.setState({value:event.target.value})}
                onFocus={(event)=>this.changeHandler(event.target.value)}
                onBlur={(event)=>this.changeHandler(event.target.value)}
                style={{backgroundColor:this.state.color}}
            />
        );
    }

    render() {
        return super.render();
    }
}

TextBox.propTypes = {
    'updatePropertiesData': PropTypes.func,
    'value': PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array ]),
    'argKey': PropTypes.string,
    'name': PropTypes.string,
    'keyData': PropTypes.string
};

export class VariableInputBox extends TextBox{
    inputElement(){
        let reg = this.props.value.toString().trim().match(/\${\s*(\w+)s*=?[^]*}/);
        if(reg){
            return <div className='variableTextBox variable'>
                <button
                    onClick={()=>this.props.displayVariables(this.changeHandler, reg[1])}
                >{reg[1]}</button>
                <button 
                    onClick={()=>this.changeHandler('')}
                >X</button>
            </div>;
        }else {
            // No Variable set
            return <div className='variableTextBox'>
                <button
                    onClick={()=>this.props.displayVariables(this.changeHandler, '')}
                >V</button>
                <input
                    type="text" 
                    value={this.props.value}
                    onChange={(event)=>this.changeHandler(event.target.value)}
                />
            </div>;
        }
    }
}
VariableInputBox.propTypes = {
    'displayVariables': PropTypes.func,
    'onVariableSelect': PropTypes.func
};

export default TextBox;
