import React from 'react';
import PropTypes from 'prop-types';
import './Properties.css';
import Button from '../Button';
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';
export class BaseProperty extends React.Component{
    constructor(props) {
        super(props);
        this.className = ('input_component ' + this.props.className).trim();
        this.state = { value: this.props.value };
    }

    changeHandler(event) {
        this.props.updatePropertiesData(this.props.argKey, event.target.value);
        this.setState({ value: event.target.value });
    }

    inputElement() {
        return undefined;
    }

    // The following function is meant to replace componentWillReceiveProps(nextProps) 
    static getDerivedStateFromProps(nextProps, previousState) {
        if(previousState.value !== nextProps.value) {
            return nextProps.value;
        }
        return null;
    }

    // If the prompt contains args, it will render additional prompt boxes for each arg.

    render() {
        // Checks if it is a header or the header's properties (args).
        if(!this.props.args) {
            return (
                <div className={this.className}>
                    <span className='propertyText' title={this.props.name}>{this.props.name}</span>
                    {this.inputElement()}
                </div>
            );
        }else {
            return (
                <div className='verticalContainer'>

                    {<ArgumentList properties={this.props} inputElement={this.inputElement()} value={this.state.value} />}
                </div>
            );
        }
    }
}
export class  ArgumentList extends React.Component{
    constructor(props) {
        super(props);
        this.state = { collapse: true };
    }


    DisplayArgs(){
        return (
            this.props.properties.args.map((arg) => (<>
                {this.state.collapse ? <><arg.type
                    key={arg.key}
                    argKey={arg.key}
                    className={'propertyContainer'}
                    name={arg.name}
                    pluginType={arg.pluginType}
                    value={arg.value}
                    options={arg.options}
                    updatePropertiesData={this.props.properties.updatePropertiesData}
                    displayVariables={this.props.properties.displayVariables}
                /> </> : null}

            </>
            )));
    }
    getPath(){
        const electron = this.getElectron();
        let path = electron.require('path');
        return path.normalize(this.props.properties.xmlFilePath);
    }
    
    getElectron() {
        return window.require('electron').remote;
    }

    isCustomPlugin(pluginName, pluginType){
        const remote = this.getElectron();
        const fs = remote.require('fs');
        const path = remote.require('path');
        let typeOfPlugin = pluginType; 
        if(pluginType === 'motion_model'){
            typeOfPlugin = 'motion';
        }
        let dirPath = this.getPath();
        if(fs.existsSync(path.normalize(dirPath + `/ProjectPlugins/include/` +
        `ProjectPlugins/plugins/${typeOfPlugin}/${pluginName}/${pluginName}.xml`))){
            return true;
        }
        return false;
    }

    editPlugin(name, pluginType) {
        if(!name || !pluginType){
            throw Error('Plugin name and type cannot be undefined');
        }

        let typeOfPlugin = pluginType; 
        if(pluginType === 'motion_model'){
            typeOfPlugin = 'motion';
        }
        const remote = this.getElectron();
        const path = remote.require('path');


        let dirPath = this.getPath();
        let pluginFiles = [];
        pluginFiles.push(path.normalize(dirPath + `/ProjectPlugins/include/` +
           `ProjectPlugins/plugins/${typeOfPlugin}/${name}/${name}.xml`));

        pluginFiles.push(path.normalize(dirPath + `/ProjectPlugins/include/` +
           `ProjectPlugins/plugins/${typeOfPlugin}/${name}/${name}.h`));

        pluginFiles.push(path.normalize(dirPath + `/ProjectPlugins/src` +
           `/plugins/${typeOfPlugin}/${name}/${name}.cpp`));
        this.props.properties.openCodeEditor(pluginFiles);
        return true;
    }
    render(){
        return (
            <>
                <div className='headerBar' >
                    <Button  clickHandler={() => {
                        this.setState({collapse: !this.state.collapse});
                    }}>{this.state.collapse ? <>▼</> : <>►</>}</Button>
                    <b style={{float: 'right'}}>{this.props.properties.name}</b>

                    { this.isCustomPlugin(this.props.properties.name, this.props.properties.pluginType) ?
                        <button style={{
                            backgroundColor: 'transparent', 
                            outline: 'none',
                            'borderStyle': 'solid',
                            cursor:'pointer'
                        }}
                        onClick={() => this.editPlugin(this.props.properties.name, this.props.properties.pluginType)}
                        >
                            {<EditTwoToneIcon />}
                        </button> : null }

               
                    {this.props.inputElement}

                </div>

                {this.DisplayArgs()}

            </>);
    }
}

ArgumentList.defaultProps = {'xmlFilePath': ''};

BaseProperty.propTypes = {
    'updatePropertiesData': PropTypes.func,
    'className': PropTypes.string,
    'value': PropTypes.string,
    'argKey': PropTypes.string,
    'name': PropTypes.string,
    'args': PropTypes.array,
    'displayVariables': PropTypes.func,
    'openCodeEditor': PropTypes.func,
    'xmlFilePath': PropTypes.string
};
ArgumentList.propTypes = {
    'properties.name': PropTypes.string,
    'properties.args': PropTypes.array,
    'inputElement': PropTypes.func,
    'updatePropertiesData': PropTypes.func,
    'displayVariables': PropTypes.func,
    'properties': PropTypes.object
};
export default BaseProperty;
