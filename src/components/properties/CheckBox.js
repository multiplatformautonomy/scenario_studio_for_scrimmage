import React from 'react';
import PropTypes from 'prop-types';
import './Properties.css';
import BaseProperty from './BaseProperty';

export class CheckBox extends BaseProperty {
    inputElement(){
        return (
            <input 
                className="checkBox"
                type="checkbox" 
                style={{width:16, height:16 }}
                value={this.state.value} 
                onChange={(event) => this.changeHandler(event)}
            />
        );
    }

    render() {
        return (<div className='propertyContainer'>
            <div style={{fontSize: 18 }}>{this.props.name}</div>
            {this.inputElement()}
        </div>);
    }
}

CheckBox.propTypes = {
    'updatePropertiesData': PropTypes.func,
    'value': PropTypes.bool,
    'name': PropTypes.string
};

export default CheckBox;
