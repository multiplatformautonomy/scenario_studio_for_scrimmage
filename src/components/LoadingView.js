import React from 'react';
import './LoadingView.css';
import propTypes from 'prop-types';
import {dom, library}from '@fortawesome/fontawesome-svg-core';
import {faSpinner}from '@fortawesome/free-solid-svg-icons';
library.add(faSpinner);
dom.watch();

export default class LoadingView extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            show:this.props.show,
            size: this.props.size
        };
        this.getNewSize = this.getNewSize.bind(this);
    }

    getNewSize() {
        const height = this.loaderSizer.clientHeight;
        const width = this.loaderSizer.clientWidth;
        const dimension = Math.min(height, width);
        return dimension;
    }
    componentDidMount() {
        this.setState({size: this.getNewSize()});
    }

    componentDidUpdate(){
        if(this.state.size !== this.getNewSize()) {
            this.setState({size: this.getNewSize()});
        }
    }

    render(){
        return (
            <div 
                style={this.props.style}
                className={'loader-container '
                    + (!this.state.show ? 'none ' : '')
                    + (this.props.fullScreen ? 'fullScreen ' : '')
                    + (this.props.static ? 'static ' : '')
                    + this.props.className}>
                <div ref={(node)=>{
                    this.loaderSizer = node;
                }}
                className='loader-sizer'
                style={{
                    width: this.props.size ? this.props.size : '100%',
                    height: this.props.size ? this.props.size : '100%'
                }}>
                    <div
                        style={{fontSize:this.state.size}}
                        className={'loader'}
                    >
                        <i className={'fas fa-spinner'} />
                    </div>
                </div>
            </div>
        );
    }
}
LoadingView.defaultProps = {
    fullScreen: false,
    show: false,
    static:false,
    className: ''
};
LoadingView.propTypes = {
    fullScreen: propTypes.bool,
    size: propTypes.string,
    show: propTypes.bool,
    static: propTypes.bool,
    style: propTypes.object,
    className: propTypes.string
};

