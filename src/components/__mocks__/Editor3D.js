import React from 'react';
import PropTypes from 'prop-types';

class Editor3D extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true,
            right: 0
        };
    }

    parseCoordinates(){
        return 0;
    }

    entityPosition() {
        let entityElements = [];
        return entityElements;
    }

    openPropertiesPanel() {
        return;
    }
    
    render() {
        return null;
    }
}

Editor3D.propTypes = {
    'entityList' : PropTypes.object,
    'show': PropTypes.bool,
    'entityComponents' : PropTypes.array,
    'closePanel' : PropTypes.func
};
export default Editor3D;
