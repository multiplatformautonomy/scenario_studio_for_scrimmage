import React from 'react';
import {mount}from 'enzyme';
import Mask from './Mask';

test('setHover to true', ()=>{
    const mask = mount(<Mask >
        <div className='Hovered'/>
    </Mask>);
    expect(mask.state('isOpen')).toBe(false);
});

/*
test('mouseLeave event', ()=>{
    const mask = mount(<Mask default={<div/>}/>);
    const mock = jest.fn(()=>{});
    mask.instance().setHover = mock;
    mask.find('.Mask').simulate('mouseLeave');
    expect(mock.mock.calls.length).toBe(1);
});
*/
