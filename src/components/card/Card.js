import React from 'react';
import propTypes from 'prop-types';
import './Card.css';

export class Card extends React.Component {
    constructor(props){
        super(props);
        this.className = ('card_component ' + this.props.className).trim();
        this.clickHandler = this.props.clickHandler ? this.props.clickHandler.bind(this) : undefined;
        this.state = {
            name: this.props.name || 'New Entity',
            color: this.props.color
        };
    }

    // Changes color of card on click if it is a property card. Performs clickHandler passed from props.
    handleOnClick(){
        if(this.props.isDisabled !== true){
            if(this.props.isPropertyCard){
                this.changeColor();
            }
            let propertiesData = [];
            if(this.state.propertiesData) {
                propertiesData = this.state.propertiesData;
            }
            this.props.clickHandler(this, propertiesData);
        }
    }

    changeColor(){
        if(this.state.color === 'white'){
            this.setState({color:'paleGreen' });
        }else { 
            this.setState({color:'white' });
        }
    }

    render(){
        return (
            <div className={'flex-row'}>
                <li className={this.className}
                    onClick={()=>this.handleOnClick()}
                    style={{width:'100%', 'color':this.state.color, 'userSelect':'none' }}>
                    <i className= {this.props.faIcon}/>
                    {this.state.name}
                    {this.props.children}
                </li>
                {this.props.isRemovable &&
                <li className={'trash'} onClick={()=>this.props.clickCopyHandler(this)}>
                    <div style={{margin:'auto' }}>
                        <i className='fas fa-copy white' />
                    </div>
                </li>
                }
                {this.props.isRemovable &&
                <li className={'trash'} onClick={()=>this.props.clickRemoveHandler(this)}>
                    <div style={{margin:'auto', position: 'relative'}}>
                        <i className='fas fa-trash red' />
                    </div>
                </li>
                }
            </div>
        );
    }
}

Card.propTypes = {
    'id': propTypes.string,
    'name': propTypes.string,
    'color': propTypes.string,
    'isRemovable': propTypes.bool,
    'clickRemoveHandler': propTypes.func,
    'clickCopyHandler' : propTypes.func,
    'clickHandler': propTypes.func,
    'isDisabled': propTypes.bool,
    'className': propTypes.string,
    'children': propTypes.element,
    'isPropertyCard': propTypes.bool,
    'faIcon': propTypes.string
};

export default Card;
