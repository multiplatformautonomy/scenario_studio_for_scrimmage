import React from 'react';
import Card from './Card';
import { shallow }from 'enzyme';

test('card initializes', ()=>{
    const card = shallow(<Card className='' isPropertyCard color='white' clickHandler={()=>{}}/>);
    card.simulate('click');
    expect(card.prop('className')).toBe('flex-row');
});

test('card color switch', ()=>{
    const card = shallow(<Card className='' isPropertyCard color='white' clickHandler={()=>{}}/>);
    expect(card.state('color')).toBe('white');
    card.instance().changeColor();
    expect(card.state('color')).toBe('paleGreen');
    card.instance().changeColor();
    expect(card.state('color')).toBe('white');
});

test('handle click', () => {
    const card = shallow(<Card className='' isPropertyCard color='white' isDisabled={false} clickHandler={()=>{}}/>);
    card.setState({propertiesData: ''});
    card.instance().handleOnClick();
});
