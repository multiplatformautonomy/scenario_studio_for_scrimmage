import Card from './Card';
import propTypes from 'prop-types';
import Docker from '../../utilities/Docker';
import { VariableInputBox }from '../properties/TextBox';
import BaseProperty from '../properties/BaseProperty';

// Contains logic for updating the property values of an Entity or MissionEntity.
export class PropertiesCard extends Card {
    type() {
        return 'PropertiesCard';
    }
    constructor(props) {
        super(props);
        this.updatePropertiesData = this.updatePropertiesData.bind(this);
        this.getPropertiesData = this.props.getPropertiesData ? this.props.getPropertiesData.bind(this) : undefined;
        this.getPropertiesData = () => this.state.propertiesData;
        this.pluginTypes = ['entity_interaction', 'metrics', 'network', 'autonomy', 'controller', 'motion_model', 'sensor'];
    }

    _getPluginElementType(params) {
        // plugins in mission xml have specific names for their elements. This will be used for 
        // saving the elements correctly in xml.
        let paramList = params['XML_DIR'].split('/');
        let type;
        switch (paramList[paramList.length - 3]) {
        case 'interaction': type = 'entity_interaction'; break;
        case 'metrics': type = 'metrics'; break;
        case 'network': type = 'network'; break;
        case 'autonomy': type = 'autonomy'; break;
        case 'controller': type = 'controller'; break;
        case 'motion': type = 'motion_model'; break;
        case 'sensor': type = 'sensor'; break;
        default: console.warn('Unknown plugin type: ', paramList[paramList.length - 3]); return;
        }
        return type;
    }

    _addPluginToProperties(pluginName, params, pluginElementType, pluginDefaults) {
        delete params['XML_DIR'];
        delete params['XML_FILENAME'];
        let newPropertiesData = this.state.propertiesData;

        const uuidv4 = require('uuid/v4');
        let pluginParamList = [];
        for(let [paramName, paramValue]of Object.entries(params)) {
            if(!paramName.includes(' ') || !paramName.includes('[')) {
                pluginParamList.push({ 
                    key: uuidv4(), 
                    name: paramName, 
                    type: VariableInputBox, 
                    value: paramValue, 
                    defaultValue: pluginDefaults[paramName] 
                });
            }
        }

        newPropertiesData.enabledPlugins.push({
            key: uuidv4(),
            name: pluginName,
            type: BaseProperty,
            pluginType: pluginElementType,
            args: pluginParamList
        });

        this.setState({ propertiesData: newPropertiesData });
    }

    _removePluginFromProperties(pluginName) {
        let newPropertiesData = this.state.propertiesData;
        let pluginList = newPropertiesData.enabledPlugins;
        let newPluginList = [];

        pluginList.forEach((plugin) => {
            if(plugin.name !== pluginName) {
                newPluginList.push(plugin);
            }
        });
        newPropertiesData.enabledPlugins = newPluginList;

        this.setState({ propertiesData: newPropertiesData });
    }

    async _getPluginParams(container, str) {
        let output = Docker.bashExec(container, `"scrimmage-plugin ${str}"`);
        let obj;
        try {
            this._parsePlugin(output, (resolve) => {
                obj = resolve;
            });
        }catch (error) {
            console.error(error);
        }
        return obj;
    }

    _parsePlugin(string, resolve) {
        let params = {};
        string = string.match(/(?:Params: )([^]*)(?:\n[-]+)/)[1];
        string = string.split(/\n(?=\S)/);
        string.forEach((param)=>{
            try {
                let [ , key, value] = param.match(/^(\S+)=([^]*)/);
                params[key] = value.replace(/[ \t]+/g, ' ').replace(/\n$/, '');
            }catch (err){
                console.error(err);
            }
        });
        resolve(params);
    }

    _toggleButtonDisability(pluginName) {
        //Disables buttons from being clicked by the user for the timeframe that the plugin is being added/removed 
        // from the entity.
        let newDisabledPlugins = this.state.disabledPluginButtons;

        let ind = newDisabledPlugins.indexOf(pluginName);
        if(ind === -1) {
            newDisabledPlugins.push(pluginName);
        }else {
            newDisabledPlugins.splice(ind, 1);
        }

        this.setState({ disabledPluginButtons: newDisabledPlugins });
        this.props.updateData(this.props.id, this.state.propertiesData); // refresh the button in order to toggle disability.
    }

    // Updates a list of enabled plugins that is tracked by this entity.
    updateEnabledPlugins(pluginName) {
        let isEnabled = false;
        this.state.propertiesData.enabledPlugins.forEach((plugin) => {
            if(plugin.name === pluginName) {
                isEnabled = true;
            }
        });

        this._toggleButtonDisability(pluginName);

        this._getPluginParams('sss_scrimmage', pluginName).then((params) => {
            if(isEnabled) {
                // TODO: could remove this from the promise. Do not need to know params to remove enabled plugin.
                this._removePluginFromProperties(pluginName, params);
            }else {
                let pluginElementType = this._getPluginElementType(params);
                this._addPluginToProperties(pluginName, params, pluginElementType, params);
            }

            this.props.updateData(this.props.id, this.state.propertiesData);
        }).then(() => {
            this._toggleButtonDisability(pluginName);
        });
    }

    _getPluginParamsCallback(params, pluginName, isEnabled) {
        if(isEnabled) {
            // TODO: could remove this from the promise. Do not need to know params to remove enabled plugin.
            this._removePluginFromProperties(pluginName, params);
        }else {
            let pluginElementType = this._getPluginElementType(params);
            delete params['XML_DIR'];
            delete params['XML_FILENAME'];
            this._addPluginToProperties(pluginName, params, pluginElementType, params);
        }

        this.props.updateData(this.props.id, this.state.propertiesData);
    }

    updateName(name, value) {
        this.props.updateName(this.props.id, value);
        this.setState({ name: value });
    }

    _searchPropertiesAndUpdate(propertiesData, key, value) {
        // Find the property in the js object and update its value.
        propertiesData.forEach((identifier) => {
            if(identifier.key === key) {
                identifier.value = value;
            }
            if(identifier.args) {
                identifier.args.forEach((arg) => {
                    if(arg.key === key) {
                        arg.value = value;
                    }
                });
            }
        });
    }

    updatePropertiesData(key, value) {
        let newPropertiesData = this.state.propertiesData;

        this._searchPropertiesAndUpdate(newPropertiesData.properties, key, value);
        this._searchPropertiesAndUpdate(newPropertiesData.enabledPlugins, key, value);

        this.props.updateData(this.props.id, newPropertiesData);
        this.setState({ propertiesData: newPropertiesData });
    }

    loadPropertiesData(propertyList) {
        // takes in a list of property names and values. Updates properties data.
        let newPropertiesData = this.state.propertiesData;

        newPropertiesData.properties.forEach((property) => {
            try {
                let propertyKey = property.name;

                // Load in XML attributes into the properties state. Attributes are stored in '$'
                switch (typeof (propertyList[propertyKey][0])) {
                case 'string': property.value = propertyList[propertyKey]; break;

                case 'object': property.args && property.args.forEach((arg, index) => {
                    if(propertyList[propertyKey][0]['$'][arg.name] !== undefined) {
                        property.args[index].value = propertyList[propertyKey][0]['$'][arg.name];
                    }
                }); break;

                case 'undefined': break;

                default: console.log(propertyKey, propertyList[propertyKey]);
                }
            }catch (err) {
                console.error(err);
            }
        });

        this.setState({ propertiesData: newPropertiesData });
    }

    loadPluginData(propertyList) {
        this.pluginTypes.forEach((type) => {
            if(propertyList[type]) {
                propertyList[type].forEach((plugin) => {
                    /* 
                    This loads in each plugin from docker in order to retrieve all default params. Then overwrites
                    params from the loaded-in plugin. This allows all plugin args to be editable by the user.
                    */
                    let pluginName = null;
                    if(plugin._) {
                        pluginName = plugin._;
                    }else {
                        pluginName = plugin;
                    }

                    this._toggleButtonDisability(pluginName);
                    
                    let pluginDefaults = {};
                    this._getPluginParams('sss_scrimmage', pluginName).then((params) => {
                        let pluginElementType = this._getPluginElementType(params);
                        let loadParams = params;

                        // replace default values with loaded-in values.
                        if(plugin['$']) { // $ are attribute args.
                            for(let arg in params) {
                                if(plugin['$'][arg] !== undefined) {
                                    pluginDefaults[arg] = loadParams[arg];
                                    loadParams[arg] = plugin['$'][arg];
                                }
                            }
                        }

                        this._addPluginToProperties(pluginName, loadParams, pluginElementType, pluginDefaults);
                        this.props.updateData(this.props.id, this.state.propertiesData);
                    }).then(() => {
                        this._toggleButtonDisability(pluginName);
                    });
                });
            }
        });
    }

    componentDidMount() {
        if(this.props.loadedProperties) {
            this.loadPluginData(this.props.loadedProperties);
            this.loadPropertiesData(this.props.loadedProperties);
        }
        this.props.updateData(this.props.id, this.state.propertiesData);
        this.props.updateName(this.props.id, this.state.name);
        this.props.select && this.props.clickHandler(this, this.propertiesData);
    }
}

PropertiesCard.propTypes = {
    'select': propTypes.bool,
    'loadedProperties': propTypes.object,
    'updateData': propTypes.func,
    'updateName': propTypes.func
};

export default PropertiesCard;
