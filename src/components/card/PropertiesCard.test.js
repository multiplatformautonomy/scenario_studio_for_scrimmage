import React from 'react';
import PropertiesCard from './PropertiesCard';
import BaseProperty from '../properties/BaseProperty';
import { shallow, mount }from 'enzyme';
import {VariableInputBox}from '../properties/TextBox';

test('get plugin type', ()=>{
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    let params = {XML_DIR: 'interaction/hello/hello'};
    expect(card.instance()._getPluginElementType(params)).toBe('entity_interaction');

    params.XML_DIR = 'metrics/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe('metrics');

    params.XML_DIR = 'network/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe('network');

    params.XML_DIR = 'autonomy/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe('autonomy');

    params.XML_DIR = 'controller/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe('controller');

    params.XML_DIR = 'motion/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe('motion_model');

    params.XML_DIR = 'sensor/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe('sensor');

    params.XML_DIR = 'metrics/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe('metrics');

    params.XML_DIR = 's/hello/hello';
    expect(card.instance()._getPluginElementType(params)).toBe(undefined);
});

test('add plugin', ()=>{
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);

    card.setState({propertiesData: {enabledPlugins: []}});

    let pluginName = 'hello';
    let params = {key: 'value'};
    let pluginElementType = 'sensor';

    card.instance()._addPluginToProperties(pluginName, params, pluginElementType, params);

    let expected = {
        enabledPlugins: [
            {
                name: 'hello',
                type: BaseProperty,
                pluginType: 'sensor',
                args: [
                    {
                        name: 'key',
                        type: VariableInputBox,
                        value: 'value',
                        defaultValue: 'value'
                    }
                ]
            }
        ]
    };
    
    // Add randomly generated key values to expected so that it will match the added plugin.
    expected.enabledPlugins[0].key = card.state('propertiesData').enabledPlugins[0].key;
    expected.enabledPlugins[0].args[0].key = card.state('propertiesData').enabledPlugins[0].args[0].key;

    expect(card.state('propertiesData')).toStrictEqual(expected);
});

test('remove plugin', ()=>{
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);

    card.setState({propertiesData: {enabledPlugins: [{name: 'hello'}, {name: 'world'}]}});

    card.instance()._removePluginFromProperties('hello');
    let expected = [{name: 'world'}];
    expect(card.state('propertiesData').enabledPlugins).toStrictEqual(expected);
});

test('toggle button', ()=>{
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    
    card.instance().setState({disabledPluginButtons: ['hello', 'world', 'foo', 'bar']});
    card.instance()._toggleButtonDisability('hello');

    let spliced = ['world', 'foo', 'bar'];
    expect(card.state('disabledPluginButtons')).toStrictEqual(spliced);

    card.instance()._toggleButtonDisability('hello');
    let arr = [ 'world', 'foo', 'bar', 'hello'];
    expect(card.state('disabledPluginButtons')).toStrictEqual(arr);
});

test('load properties', () => {
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    
    card.setState({
        propertiesData: {
            properties: [
                {
                    name: 'hello',
                    args: [{name: 'foo'}]
                },
                {
                    name: 'world',
                    args: [{name: 'bar'}]
                }
            ]
        }
    });

    let propertyList = {
        hello: [{$: {foo: 5}}],
        world: [{$: {bar: 6}}]
    };

    let expected = {
        properties: [
            {
                name: 'hello',
                args: [
                    {
                        name: 'foo',
                        value: 5
                    }
                ]
            },
            {
                name: 'world',
                args: [
                    {
                        name: 'bar',
                        value: 6
                    }
                ]
            }
        ]
    };

    card.instance().loadPropertiesData(propertyList);

    expect(card.state('propertiesData')).toStrictEqual(expected);
});

test('update plugin', () => {
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);

    card.setState({
        propertiesData: {enabledPlugins: [{name: 'hello'}, {name: 'world'}]},
        disabledPluginButtons: ['hello', 'world']
    });

    card.instance().updateEnabledPlugins('hello');

    let expectedProperties = {enabledPlugins: [{name: 'hello'}, {name: 'world'}]}; 

    let expectedDisabled = ['world'];

    expect(card.state('propertiesData')).toStrictEqual(expectedProperties);
    expect(card.state('disabledPluginButtons')).toStrictEqual(expectedDisabled);
});

test('update name', () => {
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);

    let value = 'hello';

    let name = 'foo';
    card.instance().updateName(name, value);

    expect(card.state('name')).toBe('hello');
});

test('search properties and update', () => {
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    let key = 'hello';
    let value = 'foo';
    let propertiesData = [
        {
            key: 'hello',
            args: [{key: 'hello'}]
        }
    ];
    card.instance()._searchPropertiesAndUpdate(propertiesData, key, value);
    expect(propertiesData[0].value).toBe('foo');
    expect(propertiesData[0].args[0].value).toBe('foo');
});

test('load plugin', () =>{
    const card = shallow(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    
    card.setState({
        propertiesData: {enabledPlugins: []},
        disabledPluginButtons: []
    });
    
    let propertyList = {
        metrics: [
            {
                $: {
                    key: 'value',
                    arg: 'world'
                }
            }
        ]
    };
    propertyList.metrics[0]['_'] = 'foo';

    let obj = {arg: 'hello'};
    card.instance()._getPluginParams = jest.fn((first1) => first1).mockResolvedValue(obj);
    card.instance()._getPluginElementType = jest.fn((first) => first);

    card.instance().loadPluginData(propertyList);

    propertyList = {
        metrics: [
            {
                $: {
                    key: 'value',
                    arg: 'world'
                }
            }
        ]
    };
    card.instance().loadPluginData(propertyList);
});

// test('scrimmage plugin callback processes docker output data', () => {
//     const card = mount(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}} 
//         getPropertiesData={()=>true}/>);
//     const cardInst = card.instance();
//     let resolution = null;
//     let rejection = null;
//     const resolve = jest.fn((val) => {
//         resolution = val;
//     });
//     const reject = jest.fn((val) => {
//         rejection = val;
//     });
//     let topic = null;
//     let topic2 = null;
//     let topic3 = null;
//     const proc = {
//         stdout: {
//             setEncoding: jest.fn(),
//             on: jest.fn((topic_, fun) => {
//                 topic = topic_;
//                 fun('Params:\nhello=world');
//             })
//         },
//         stderr: {
//             setEncoding: jest.fn(),
//             on: jest.fn((topic_, fun) => {
//                 topic2 = topic_;
//                 fun(6);
//             })
//         },
//         on: jest.fn((topic_, fun) => {
//             topic3 = topic_;
//             fun();
//         })
//     };
//     cardInst.scrimmagePluginCallbackProcClose = (_s_, resolve_) => {
//         resolve_(7);
//     };
//     cardInst.scrimmagePluginCallback(proc, resolve, reject);
//     expect(proc.stdout.on).toHaveBeenCalled();
//     expect(topic).toBe('data');
//     expect(topic2).toBe('data');
//     expect(topic3).toBe('close');
//     expect(resolve).toHaveBeenCalled();
//     expect(reject).toHaveBeenCalled();
//     expect(rejection).toBe(6);
//     expect(resolution).toBe(7);
// });

test('scrimmage plugin proc close callback generates appropriate output for parameters', () => {
    const card = mount(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    const cardInst = card.instance();
    let resolution = null;
    const resolve = jest.fn((val) => {
        resolution = val;
    });
    let string = 'ConfigParse: found /home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/Straight.xml\n';
    string += '==========================================\n';
    string += 'Plugin found.\n';
    string += 'Name: Straight.xml\n';
    string += 'File: /home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/Straight.xml\n';
    string += 'Library: Straight_plugin\n';
    string += '-------------------------\n';
    string += 'Params: XML_DIR=/home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/\n';
    string += 'XML_FILENAME=/home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/Straight.xml\n';
    string += 'enable_boundary_control=false\n';
    string += 'generate_entities=false\n';
    string += 'library=Straight_plugin\n';
    string += 'save_camera_images=false\n';
    string += 'show_camera_images=false\n';
    string += 'show_text_label=false\n';
    string += 'speed=21\n';
    string += '---------------------------\n';
    string += 'Matching plugin libraries found: \n';
    string += '/home/scrimmage/scrimmage/build/plugin_libs/libStraight_plugin.so\n';
    cardInst._parsePlugin(string, resolve);
    expect(resolve).toHaveBeenCalled();
    expect(resolution.speed).toBe('21');
});

test('getting plugin parameters should not return xml file paths', () => {
    const updateData = jest.fn(()=>{});
    const card = mount(<PropertiesCard className='' updateData={updateData} updateName={()=>{}}/>);
    const cardInst = card.instance();
    const params = {
        'XML_DIR': '/home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/',
        'XML_FILENAME': '/home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/Straight.xml',
        'speed': '21'
    };
    cardInst._addPluginToProperties = jest.fn((_pluginName, _params) => {
        expect(_params).toEqual(expect.not.objectContaining({'XML_DIR': expect.any(String)}));
        expect(_params).toEqual(expect.not.objectContaining({'XML_FILENAME': expect.any(String)}));
        expect(_params).toEqual(expect.objectContaining({'speed': '21'}));
    });
    cardInst._removePluginFromProperties = jest.fn((pluginName) => {
        expect(pluginName).toBe('Straight');
    });
    cardInst._getPluginParamsCallback(params, 'Straight', false);
    expect(cardInst._addPluginToProperties).toHaveBeenCalled();
    expect(cardInst._removePluginFromProperties).not.toHaveBeenCalled();
    expect(updateData).toHaveBeenCalled();
});

test('getting plugin parameters should not return xml file paths different', () => {
    const updateData = jest.fn(()=>{});
    const card = mount(<PropertiesCard className='' updateData={updateData} updateName={()=>{}}/>);
    const cardInst = card.instance();
    const params = {
        'XML_DIR': '/home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/',
        'XML_FILENAME': '/home/scrimmage/scrimmage/include/scrimmage/plugins/autonomy/Straight/Straight.xml',
        'speed': '21'
    };
    cardInst._addPluginToProperties = jest.fn((_pluginName, _params) => {
        expect(_params).toEqual(expect.not.objectContaining({'XML_DIR': expect.any(String)}));
        expect(_params).toEqual(expect.not.objectContaining({'XML_FILENAME': expect.any(String)}));
        expect(_params).toEqual(expect.objectContaining({'speed': '21'}));
    });
    cardInst._removePluginFromProperties = jest.fn((pluginName) => {
        expect(pluginName).toBe('Straight');
    });
    cardInst._getPluginParamsCallback(params, 'Straight', true);
    expect(cardInst._addPluginToProperties).not.toHaveBeenCalled();
    expect(cardInst._removePluginFromProperties).toHaveBeenCalled();
    expect(updateData).toHaveBeenCalled();
});

test('update enabled plugins calls appropriate callbacks', () => {
    const card = mount(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    const cardInst = card.instance();
    const pluginName = 'Straight';
    cardInst._getPluginParamsCallback = jest.fn((_params, _pluginName) => {
        expect(_pluginName).toBe(pluginName);
    });
    cardInst._toggleButtonDisability = jest.fn((_pluginName) => {
        expect(_pluginName).toBe(pluginName);
    });
    cardInst._getPluginElementType = jest.fn((first)=>first);
    cardInst._getPluginParams = jest.fn((scrim, _pluginName) => {
        expect(_pluginName).toBe(pluginName);
        return ({
            then(f1_) {
                f1_({});
                return {
                    then(f2_) {
                        f2_();
                    }
                };
            }
        });
    });
    cardInst.setState({
        propertiesData: {enabledPlugins: []},
        disabledPluginButtons: [pluginName]
    });
    cardInst.updateEnabledPlugins(pluginName);
    expect(cardInst._toggleButtonDisability).toHaveBeenCalled();
});

test('add plugin to properties branch coverage', () => {
    const card = mount(<PropertiesCard className='' updateData={()=>{}} updateName={()=>{}}/>);
    card.setState({propertiesData: {enabledPlugins: []}});

    let params = {'hello ': 'world'};

    let expected = [
        {
            name: 'foo',
            type: BaseProperty,
            pluginType: 'bar',
            args: [
                {
                    name: 'hello ',
                    type: VariableInputBox,
                    value: 'world',
                    defaultValue:'world'
                }
            ]
        }
    ];

    card.instance()._addPluginToProperties('foo', params, 'bar', params);

    // Add randomly generated key values to expected so that it will match the added plugin.
    expected[0].key = card.state('propertiesData').enabledPlugins[0].key;
    expected[0].args[0].key = card.state('propertiesData').enabledPlugins[0].args[0].key;

    expect(card.state('propertiesData').enabledPlugins).toStrictEqual(expected);
});
