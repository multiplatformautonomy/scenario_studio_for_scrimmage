import MissionSettingProperties from '../../assets/MissionSettingsTemplate';
import PropertiesCard from './PropertiesCard';

export class MissionEntity extends PropertiesCard {
    type() {
        return 'MissionEntity';
    }
    constructor(props){
        super(props);

        this.state = {
            name: this.props.name || 'New Mission',
            propertiesData: this.props.propertiesData || new MissionSettingProperties(),
            disabledPluginButtons: []
        };
    }
}

export default MissionEntity;
