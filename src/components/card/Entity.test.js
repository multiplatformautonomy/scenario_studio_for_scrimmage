import React from 'react';
import Entity from './Entity';
import { shallow }from 'enzyme';


/**
 * Tests
 * + Entity initializes
 * + add an autonomy to enabled plugin list
 * + remove an autonomy from the enabled plugin list
 * Functions for testing
 * + updateEnabledPlugins
 * - updatePropertiesData
 * - loadPropertiesData
 */

test('card initializes', ()=>{
    const card = shallow(
        <Entity className=''
            updateData={()=>{}}
            updateName={()=>{}}/>);
    expect(card.prop('className')).toBe('flex-row');
});

// test('Entity can hold a plugin', ()=>{
//     const card = shallow(<Entity className='' updateData={()=>{}} updateName={()=>{}}/>);
//     card.instance().updateEnabledPlugins('test_plugin');
//     expect(card.state('enabledPluginList').indexOf('test_plugin')).toBeGreaterThanOrEqual(0);
// });

// test('Entity can remove a plugin',()=>{
//     const card = shallow(<Entity className='' updateData={()=>{}} updateName={()=>{}}/>);
//     card.setState({enabledPluginsList:['test_plugin']});
//     card.instance().updateEnabledPlugins('test_plugin');
//     expect(card.state('enabledPluginsList').indexOf('test_plugin')).toBeLessThan(0);
// });

test('Entity can update properties data', ()=>{
    const card = shallow(<Entity className='' updateData={()=>{}} updateName={()=>{}}/>);
    expect(card.instance().getPropertiesData()['properties'][0].value).toStrictEqual(['1']);
    let teamKey = card.instance().getPropertiesData()['properties'][0].key;
    card.instance().updatePropertiesData(teamKey, 2);
    expect(card.instance().getPropertiesData()['properties'][0].value).toBe(2);
});

test('Entity can load properties data', ()=>{
    const card = shallow(<Entity className='' updateData={()=>{}} updateName={()=>{}}/>);
    expect(card.instance().getPropertiesData()['properties']).toHaveLength(11);
    expect(card.instance().getPropertiesData()['properties'][0].value).toStrictEqual(['1']);
    card.instance().loadPropertiesData({'team_id':'3'});
    expect(card.instance().getPropertiesData()['properties'][0].value).toBe('3');
});
