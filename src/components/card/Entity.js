import PropertiesCard from './PropertiesCard';
import EntityProperties from '../../assets/EntityTemplate';

export class Entity extends PropertiesCard {
    type() {
        return 'Entity';
    }
    constructor(props){
        super(props);
        this.state = {
            name: this.props.name || 'New Entity',
            propertiesData: this.props.propertiesData || new EntityProperties(),
            disabledPluginButtons: []
        };
    }
}

export default Entity;
