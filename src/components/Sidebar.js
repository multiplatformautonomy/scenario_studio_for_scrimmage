import React from 'react';
import './Sidebar.css';
import propTypes from 'prop-types';

export default class SSSSidebar extends React.Component {
    constructor(props){
        super(props);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.className = ('component-sidebar ' + this.props.className).trim();
        this.state = {show: this.props.show === undefined ? true : this.props.show };
    }

    setShow(show){
        this.setState({show:show});
    }

    componentDidMount(){
        if(this.props.closeable) { //Gets rid of unneccessary event listeners
            document.addEventListener('mouseup', this.handleOutsideClick);
        }
    }

    componentWillUnmount() {
        document.removeEventListener('mouseup', this.handleOutsideClick);
    }

    setWrapperRef(node){
        this.wrapperRef = node;
    }
    handleOutsideClick(event){
        if(this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({show:false });
        }
    }

    render(){
        if(this.state.show){
            return (
                <div ref={this.setWrapperRef} className={this.className}>
                    {this.props.children}
                </div>


            );
        }
        return null;
    }
}

SSSSidebar.propTypes = {
    className: propTypes.string,
    show: propTypes.bool,
    closeable: propTypes.bool,
    children: propTypes.oneOfType([propTypes.element, propTypes.string, propTypes.array ])
};
