import React from 'react';
import LoadingView from './LoadingView';
import {mount}from 'enzyme';


/** 
 */

test('LoadingView initializes', ()=>{
    const loadingView = mount(<LoadingView/>);
    expect(loadingView.find('div').length).toBe(3);
});

test('LoadingView renders nothing on initialization', ()=>{
    const loadingView = mount(<LoadingView/>);
    expect(loadingView.instance().state.show).toBeFalsy();
    expect(loadingView.render().length).toBe(1);
});


test('Loading View render only when show is true', ()=>{
    const loadingView = mount(<LoadingView/>);
    expect(loadingView.state('show')).toBeFalsy();
    loadingView.setState({show:true });
    expect(loadingView.state('show')).toBeTruthy();
});

test('LoadingView stops rendering when show becomes false', ()=>{
    const loadingView = mount(<LoadingView/>);
    loadingView.setState({show:true });
    expect(loadingView.state('show')).toBeTruthy();
    expect(loadingView.render().length).toBe(1);
    loadingView.setState({show:false });
    expect(loadingView.render().length).toBe(1);
});

test('LoadingComponent changes height to match container when no size defined', () =>{
    const mockGetNewSize = jest.fn();
    LoadingView.prototype.getNewSize =  mockGetNewSize;
    const loadingView = mount(<LoadingView/>);
    loadingView.update();
    expect(mockGetNewSize).toBeCalled();
});


