// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import { configure }from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

window.testing = true;

window.alert = (msg) => {
    console.log(`ALERT: ${msg}`);
};
window.require = ()=> ({
    remote:{
        require:require,
        app:{getAppPath:()=>'src/screens/LiveMission.test.test.xml' }
    }
});

window.testing = true;

window.map = {};
document.addEventListener = jest.fn((event, cb) => {
    window.map[event] = cb;
});

document.removeEventListener = jest.fn((event) => {
    window.map[event] = undefined;
});

window.require = ()=> ({remote:{require:require }, app:{quit:()=>{} } });

if(!process.env.LISTENING_TO_UNHANDLED_REJECTION) {
    process.on('unhandledRejection', (reason) => {
        console.error(reason);
    });
    // Avoid memory leak by adding too many listeners
    process.env.LISTENING_TO_UNHANDLED_REJECTION = true;
}
